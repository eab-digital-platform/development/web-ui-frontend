'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var core = require('@material-ui/core');
var axios = _interopDefault(require('axios'));
var _ = _interopDefault(require('lodash'));
var _slicedToArray = _interopDefault(require('@babel/runtime/helpers/slicedToArray'));
var React = require('react');
var React__default = _interopDefault(React);
var PropTypes = _interopDefault(require('prop-types'));
var _defineProperty = _interopDefault(require('@babel/runtime/helpers/defineProperty'));
var _regeneratorRuntime = _interopDefault(require('@babel/runtime/regenerator'));
var _asyncToGenerator = _interopDefault(require('@babel/runtime/helpers/asyncToGenerator'));
var _extends = _interopDefault(require('@babel/runtime/helpers/extends'));
var AppBar = _interopDefault(require('@material-ui/core/AppBar'));
var Toolbar = _interopDefault(require('@material-ui/core/Toolbar'));
var Folder = _interopDefault(require('@material-ui/icons/Folder'));
var People = _interopDefault(require('@material-ui/icons/People'));
var Star = _interopDefault(require('@material-ui/icons/Star'));
var Schedule = _interopDefault(require('@material-ui/icons/Schedule'));
var OfflinePin = _interopDefault(require('@material-ui/icons/OfflinePin'));
var Publish = _interopDefault(require('@material-ui/icons/Publish'));
var Backup = _interopDefault(require('@material-ui/icons/Backup'));
var Delete = _interopDefault(require('@material-ui/icons/Delete'));
var Settings = _interopDefault(require('@material-ui/icons/Settings'));
var Menu = _interopDefault(require('@material-ui/icons/Menu'));
var Search = _interopDefault(require('@material-ui/icons/Search'));
var Close = _interopDefault(require('@material-ui/icons/Close'));
var Add = _interopDefault(require('@material-ui/icons/Add'));
var Create = _interopDefault(require('@material-ui/icons/Create'));
var ArrowBack = _interopDefault(require('@material-ui/icons/ArrowBack'));
var Button = _interopDefault(require('@material-ui/core/Button'));
var IconBtn = _interopDefault(require('@material-ui/core/IconButton'));
var styles = require('@material-ui/core/styles');
var Drawer = _interopDefault(require('@material-ui/core/Drawer'));
var Avatar = _interopDefault(require('@material-ui/core/Avatar'));
var Typography = _interopDefault(require('@material-ui/core/Typography'));
var ListItem = _interopDefault(require('@material-ui/core/ListItem'));
var ListItemIcon = _interopDefault(require('@material-ui/core/ListItemIcon'));
var ListItemText = _interopDefault(require('@material-ui/core/ListItemText'));
var AwesomeSlider = _interopDefault(require('react-awesome-slider'));
require('react-awesome-slider/dist/styles.css');
var Dialog = _interopDefault(require('@material-ui/core/Dialog'));
var CafeDisplayEngine = _interopDefault(require('cafe-interface-engine'));
var TextField = _interopDefault(require('@material-ui/core/TextField'));
var Card = _interopDefault(require('@material-ui/core/Card'));
var CardContent = _interopDefault(require('@material-ui/core/CardContent'));
var _objectWithoutProperties = _interopDefault(require('@babel/runtime/helpers/objectWithoutProperties'));
var Box = _interopDefault(require('@material-ui/core/Box'));
var Tabs = _interopDefault(require('@material-ui/core/Tabs'));
var Tab = _interopDefault(require('@material-ui/core/Tab'));
var Divider = _interopDefault(require('@material-ui/core/Divider'));
var reactRouterDom = require('react-router-dom');
var history = require('history');
var CssBaseline = _interopDefault(require('@material-ui/core/CssBaseline'));
var styles$1 = require('@material-ui/styles');

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = "body {\n  /* Display Form */\n  --display-header-height: 180px;\n  --display-sideBar-width: 200px;\n  --display-form-width: 768px;\n\n  --displayCard-height: 40px;\n\n  /* Navigation Stepper */\n  --stepper-height: 40px;\n  --stepper-width: 130px;\n\n  /* Form Width */\n  --myForm-width: 800px;\n  --myFormColumn3-width: 1200px;\n  --form-width: 800px;\n\n  /* SideBar Width */\n  --sideBar-width: 72px;\n  --subSideBar-width: 216px;\n\n  /* Preview width */\n  --preview-normal-width: 400px;\n  --preview-hidden-width: 50px;\n\n  /* Design Panel width */\n  --design-panel-normal-width: 300px;\n  --design-panel-hidden-width: 0px;\n\n  /* Hidden Question Btn width */\n  --hidden-question-btn-width: 160px;\n\n  /* Question Title Index */\n  --question-title-index: 90px;\n\n  /* Text Field */\n  --input-line-height: 40px;\n  --input-field-height: 120px;\n\n  /* add question bar */\n  --add-question-bar-width: 177px;\n  --add-question-bar-height: 24px;\n  --add-question-bar-font-size: 15px;\n  --add-question-bar-padding-top: 0px;\n  --add-question-bar-left-margin: 65px;\n\n  /* Color */\n  --main-background: #ffffff;\n  --primary-background: #efefef;\n  --sideBar-background: #000000;\n  --card-background: #f4f2f2;\n  --line-color: #e5e5e5;\n  --primary-color: #a70632;\n\n  --color-lightRed: #be8393;\n  --color-lightPink: #eee6e8;\n  --color-white: #ffffff;\n\n  --color-error: #d72825;\n\n  --color-lightGrey: #efefef;\n  --color-semiGrey: #e5e5e5;\n  --color-grey: #a7a7a7;\n  --color-darkGrey: #707070;\n  --color-black: #000000;\n  --color-placeholder: #cccccc;\n\n  --color-purple: #6435c9;\n  --color-success-green: #3addc4;\n\n  --font-black: var(--color-black);\n  --font-lightRed: var(--color-lightRed);\n  --font-grey: #a7a7a7;\n  --font-darkGrey: #707070;\n  --font-white: #ffffff;\n  --font-sideBar: #ffffff;\n\n  --paper-border: #d0d0d0;\n\n  --loading-color: var(--primary-color);\n\n  /* transform time */\n  --short-transform: 0.2s;\n  --long-transform: 0.5s;\n\n  /* Border Radius */\n  --borderRadius-Large: 8px;\n  --borderRadius-Medium: 4px;\n  --borderRadius-Small: 2px;\n\n  /* Snackbar */\n  --snackbar-info-background: #333333;\n  --snackbar-text: var(--color-white);\n\n  /* Button */\n  --btn-primary-background: #1d7ded;\n  --btn-secondary-background: var(--color-lightRed);\n  --btn-red-background: var(--primary-color);\n  --btn-primary-color: var(--color-lightGrey);\n\n  --btn-default-2-background: var(--color-lightGrey);\n  --btn-default-2-color: var(--color-grey);\n\n  --btn-edit-color: var(--color-lightRed);\n  /* Button End */\n\n  /* alignment */\n  --alignmentSSL: 108px;\n  --alignmentSL: 72px;\n  --alignmentXXXXL: 48px;\n  --alignmentXXXL: 36px;\n  --alignmentXXL: 32px;\n  --alignmentXL: 28px;\n  --alignmentL: 24px;\n  --alignmentXM: 20px;\n  --alignmentM: 16px;\n  --alignmentMS: 12px;\n  --alignmentXLS: 10px; /* Due to the repeat of 10px, adding a new for it */\n  --alignmentXS: 10px;\n  --alignmentS: 8px;\n  --alignmentXMS: 6px;\n  --alignmentXS: 4px;\n  --alignmentXXS: 2px;\n  --alignmentNth: 0px;\n\n  /* Fonts */\n  --font-family: \"Lato\";\n\n  --fontSizeBase: 14px;\n\n  --fontSizeXXXXL: 40px;\n  --fontSizeXXXL: 36px;\n  --fontSizeXXL: 30px;\n  --fontSizeXL: 26px;\n  --fontSizeL: 24px;\n  --fontSizeXXXM: 22px;\n  --fontSizeXXM: 20px;\n  --fontSizeXM: 18px;\n  --fontSizeM: 16px;\n  --fontSizeS: 14px;\n  --fontSizeXS: 12px;\n  --fontSizeXXS: 10px;\n  --fontSizeXXXS: 8px;\n\n  /* Font weight */\n  --fontLight: 200;\n  --fontNormal: 400;\n  --fontLightBold: 700;\n  --fontBold: 900;\n\n  /* border width */\n  --borderWidthS: 1px;\n  --borderWidthM: 2px;\n  --borderWidthL: 3px;\n\n  /* border-radius */\n  --borderRadiusXXS: 2px;\n  --borderRadiusXS: 4px;\n\n  /* Question dropdown list */\n  --menuWidthM: 500px;\n  --menuHeightM: 460px;\n\n  /* Shadow */\n  --shadowLight: 0 2px 4px 0 rgba(0, 0, 0, 0.2);\n  --shadowMedium: 0 6px 6px 0 rgba(0, 0, 0, 0.2);\n  --shadowMediumPlus: 0 12px 20px 0 rgba(0, 0, 0, 0.2);\n  --shadowLarge: 0 24px 34px 0 rgba(0, 0, 0, 0.2);\n\n  /* Left Ribccon  */\n  --ribbonWidthS: 5px;\n\n  /* TextField input */\n  --textField-input-width: 182px;\n\n  /* z-index Layer */\n  --overlay-depth-XS: 1;\n  --overlay-depth-S: 10;\n  --overlay-depth-M: 100;\n  --overlay-depth-XM: 200;\n  --overlay-depth-XXM: 300;\n  --overlay-depth-L: 1000;\n  --overlay-depth-XL: 10000;\n}\n\n/* OVERRIDE */\nh2 {\n  font-size: var(--fontSizeXXM);\n}\n\n\n\n/* Chrome scrollbar || START */\n.scrollWrapper::-webkit-scrollbar,\n.scrollWrapper::-webkit-scrollbar-thumb {\n  width: 10px;\n  height: 5px;\n}\n\n.scrollWrapper::-webkit-scrollbar-thumb {\n  /* box-shadow: inset 0 0 0 5px var(--color-grey); */\n  box-shadow: inset 0 0 0 10px transparent;\n  /* transition: box-shadow var(--long-transform); */\n}\n\n.scrollWrapper:hover::-webkit-scrollbar-thumb {\n  box-shadow: inset 0 0 0 10px var(--color-grey);\n}\n/* Chrome scrollbar || END */\n/* Firefox scrollbar || START */\n.scrollWrapper {\n  scrollbar-color: transparent transparent;\n  scrollbar-width: thin;\n  overflow: auto;\n  transition: all var(--long-transform);\n}\n\n.scrollWrapper:hover {\n  scrollbar-color: var(--color-grey) transparent ;\n}\n/* Firefox scrollbar || END */\n\n.scrollContainer {\n  width: 100%;\n  /* color: var(--color-grey); */\n}";
styleInject(css);

// This function take the name of the variable in CSS.

function getDefaultTheme(property) {
  return getComputedStyle(document.body).getPropertyValue(property).trim();
}

function customTheme() {
  return core.createMuiTheme({
    typography: {
      fontFamily: getDefaultTheme("--font-family"),
      useNextVariants: true
    },
    palette: {
      primary: {
        main: getDefaultTheme("--btn-primary-background")
      },
      secondary: {
        main: getDefaultTheme("--btn-secondary-background")
      },
      text: {
        white: getDefaultTheme("--font-white"),
        black: getDefaultTheme("--font-black"),
        lightRed: getDefaultTheme("--font-lightRed"),
        grey: getDefaultTheme("--font-grey"),
        darkGrey: getDefaultTheme("--font-darkGrey")
      }
    },
    overrides: {
      MuiButton: {
        contained: {
          "box-shadow": "none",
          background: getDefaultTheme("--btn-default-background")
        }
      },
      MuiExpansionPanelSummary: {
        root: {
          "min-height": 0,
          "&$expanded": {
            "min-height": 0
          }
        }
      },
      MuiFab: {
        root: {
          "box-shadow": "none"
        }
      },
      MuiPaper: {
        elevation2: {
          "box-shadow": "none",
          border: "".concat(getDefaultTheme("--borderWidthS"), " solid ").concat(getDefaultTheme("--paper-border"))
        }
      },
      MuiTooltip: {
        tooltip: {
          backgroundColor: "transparent",
          maxWidth: "none",
          lineHeight: 0
        },
        popper: {
          opacity: 1
        }
      },
      MuiInput: {
        underline: {
          /** strange MUI Behaviour */
          "&:hover:not($disabled):not($focused):not($error):before": {
            borderBottom: "".concat(getDefaultTheme("--borderWidthM"), " solid ").concat(getDefaultTheme("--color-grey"))
          },
          "&:after": {
            borderBottom: "".concat(getDefaultTheme("--borderWidthM"), " solid ").concat(getDefaultTheme("--color-grey"))
          },
          "&.Mui-disabled:before": {
            borderBottomStyle: "solid"
          }
        }
      },
      MuiInputBase: {
        input: {
          "&.Mui-disabled": {
            cursor: "not-allowed"
          }
        }
      },
      MuiSnackbarContent: {
        root: {
          width: "calc(100vw *(900/1440))",
          justifyContent: "space-between"
        },
        message: {
          display: "inherit",
          alignItems: "center",
          flex: "1",
          justifyContent: "center",
          color: getDefaultTheme("--snackbar-text")
        }
      },
      MuiFormControlLabel: {
        root: {
          "&.Mui-disabled": {
            color: getDefaultTheme("--font-black"),
            cursor: "not-allowed"
          }
        }
      }
    }
  });
}

var axiosTimeOut = 5000; // 这里应该是每次请求拦截器

axios.interceptors.request.use(function (config) {
  //config包含每次请求的内容
  if (localStorage.getItem('api_token')) {
    config.headers.apiToken = "".concat(localStorage.getItem('api_token'));
  }

  return config;
}, function (err) {
  return Promise.reject(err);
}); // 这里应该是每次响应(返回数据)拦截器

axios.interceptors.response.use(function (response) {
  // 这里可以写对于返回数据的拦截处理, 这里可以仅仅是返回response.data数据
  return response;
}, function (error) {
  console.log("=====数据返回错误=====", error);
  return Promise.resolve(error.response);
}); // 检查状态码的方法

function checkStatus(response) {
  if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
    // 检查返回的status, 如果没问题, 仅仅返回response.data数据
    console.log("=====response.status没有问题, 将直接返回response.data=====");
    return response.data;
  } else {
    console.log("=====response.status出现问题, response > =====", response);
    return {
      status: -404,
      msg: '网络异常'
    };
  }
} // 如果失败，弹出错误信息


function checkCode(response) {
  if (response.status === -404) {
    // alert(response.msg);
    console.log("======axios检查返回状态出错=====", response);
  }

  return response;
}

var webAjax = {
  post: function post(url, data) {
    return axios({
      method: 'post',
      // baseURL: axiosPreURl,
      url: url,
      data: data,
      timeout: axiosTimeOut,
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json;charset=UTF-8'
      }
    }).then(function (res) {
      return checkStatus(res);
    }).then(function (res) {
      return checkCode(res);
    });
  },
  get: function get(url, params) {
    return axios({
      method: 'get',
      // baseURL: axiosPreURl,
      url: url,
      params: params,
      timeout: axiosTimeOut,
      headers: {
        'x-Requested-With': 'XMLHttpRequest'
      }
    }).then(function (res) {
      return checkStatus(res);
    }).then(function (res) {
      return checkCode(res);
    });
  }
};

var configConstants = {
  // BACKEND_URL: "http://localhost:3001/api",
  // CAFE_API: "http://api-dev.eab-cafe.k8s-inno1",
  // CORE_API: "http://10.0.0.164:3500/core-api",
  // SEARCH_API: "http://10.0.0.164:3000/search-api",
  // BACKEND_URL: "http://web-ui-backend-dp.eab.openshift/api",
  BACKEND_URL_FOR_PROJECT: "http://cmsadminapi-dp.eab.openshift",
  // 下面这个是cms api
  BACKEND_URL_FOR_PAGE: "http://cmsadminapi-dp.eab.openshift/pageMaster/id",
  // 下面这个是旧的web-ui backend
  // BACKEND_URL_FOR_PAGE: "http://web-ui-backend-dp.eab.openshift/api/getJsonByDocId",
  CAFE_API: "http://api-dev.eab-cafe.k8s-inno1",
  CORE_API: "http://core-api-dp.eab.openshift/core-api",
  SEARCH_API: "http://search-api-dp.eab.openshift/search-api",
  POS_API: "http://core-api-dp.eab.openshift/pos-api",
  MQREADFROMMESSAGE: "Y",
  AXIOS_TIME_OUT: 5000,
  MQ: {
    CREATE: "LOL15",
    EDIT: "LOL16"
  }
};

var resolveParams = function resolveParams(data, dataUrl, params) {
  var result = {
    dataUrl: dataUrl,
    params: {}
  };
  params.map(function (param) {
    if (param.name && param.name !== "") {
      if (param.value.indexOf("#") === 0) {
        if (data && _.get(data, param.value.substr(1), "")) {
          result.params[param.name] = _.get(data, param.value.substr(1), "");
        }
      } else {
        result.params[param.name] = param.value;
      }
    } else if (param.value && param.value !== "") {
      if (param.value.indexOf("#") === 0) {
        if (data && _.get(data, param.value.substr(1), "")) {
          result.dataUrl = "".concat(result.dataUrl, "/").concat(_.get(data, param.value.substr(1), ""));
        }
      } else {
        result.dataUrl = "".concat(result.dataUrl, "/").concat(param.value);
      }
    }
  });
  return result;
};

var fetchData = function fetchData() {
  var dataUrl = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  console.log("run fetchData > ", dataUrl);
  return webAjax.get(dataUrl, params).then(function (res) {
    if (res.success && res.result) {
      return res.result;
    } else {
      console.error("没有获取到结果: ", res);
      return null;
    }
  });
};

var fetchPageJsonData = function fetchPageJsonData(pageId) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  console.log("===这里是得到page json数据的=== > ", pageId); // http://cmsadminapi-dp.eab.openshift/pageMaster/id/

  return webAjax.get("".concat(configConstants.BACKEND_URL_FOR_PAGE, "/").concat(pageId), params);
};

var fetchProjectJsonData = function fetchProjectJsonData(organizationId, projectId) {
  var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  // http://cmsadminapi-dp.eab.openshift/project/5e572b0450b21a0032f5c444
  console.log("===这里是得到project setting数据的=== > ", organizationId, projectId);
  return webAjax.get("".concat(configConstants.BACKEND_URL_FOR_PROJECT, "/").concat(organizationId, "/").concat(projectId), params);
}; // const fetchPageJsonDataForDev = (pageId, params={}) => {
//   console.log("===这里是本地测试用的, 得到page json数据的=== > ", pageId);
//   // http://cmsadminapi-dp.eab.openshift/pageMaster/id/
//   return webAjax.get(`${configConstants.BACKEND_URL_FOR_DEV}/getJsonByDocId/${pageId}`, params);
// }


var util = {
  resolveParams: resolveParams,
  fetchData: fetchData,
  fetchPageJsonData: fetchPageJsonData,
  fetchProjectJsonData: fetchProjectJsonData
};

var mapping = {
  "smokingMapping": [{
    "key": "N",
    "values": {
      "en": "Non-smoker",
      "zh-Hant": "非吸煙"
    }
  }, {
    "key": "Y",
    "values": {
      "en": "Smoker",
      "zh-Hant": "吸煙"
    }
  }, {
    "key": "U",
    "values": {
      "en": "Unknown",
      "zh-Hant": "不適用"
    }
  }],
  "countryMapping": [{
    "key": "C21",
    "values": {
      "en": "Hong Kong",
      "zh-Hant": "香港"
    }
  }, {
    "key": "C4",
    "values": {
      "en": "China",
      "zh-Hant": "中國"
    }
  }, {
    "key": "C150",
    "values": {
      "en": "Afghanistan",
      "zh-Hant": "阿富汗"
    }
  }, {
    "key": "C151",
    "values": {
      "en": "Åland Islands",
      "zh-Hant": "阿蘭群島"
    }
  }, {
    "key": "C40",
    "values": {
      "en": "Albania",
      "zh-Hant": "阿爾巴尼亞"
    }
  }, {
    "key": "C152",
    "values": {
      "en": "Algeria",
      "zh-Hant": "阿爾及利亞"
    }
  }, {
    "key": "C42",
    "values": {
      "en": "American Samoa",
      "zh-Hant": "美薩摩亞"
    }
  }, {
    "key": "C44",
    "values": {
      "en": "Andorra",
      "zh-Hant": "安道爾"
    }
  }, {
    "key": "C153",
    "values": {
      "en": "Angola",
      "zh-Hant": "安哥拉"
    }
  }, {
    "key": "C45",
    "values": {
      "en": "Anguilla",
      "zh-Hant": "安圭拉島"
    }
  }, {
    "key": "C253",
    "values": {
      "en": "Antarctica",
      "zh-Hant": "南極洲"
    }
  }, {
    "key": "C46",
    "values": {
      "en": "Antigua and Barbuda",
      "zh-Hant": "安提瓜 和 巴布達"
    }
  }, {
    "key": "C47",
    "values": {
      "en": "Argentina",
      "zh-Hant": "阿根廷"
    }
  }, {
    "key": "C48",
    "values": {
      "en": "Armenia",
      "zh-Hant": "亞美尼亞"
    }
  }, {
    "key": "C49",
    "values": {
      "en": "Aruba",
      "zh-Hant": "阿魯巴島"
    }
  }, {
    "key": "C9",
    "values": {
      "en": "Australia",
      "zh-Hant": "澳洲"
    }
  }, {
    "key": "C10",
    "values": {
      "en": "Austria",
      "zh-Hant": "奧地利"
    }
  }, {
    "key": "C50",
    "values": {
      "en": "Azerbaijan",
      "zh-Hant": "阿塞拜疆"
    }
  }, {
    "key": "C51",
    "values": {
      "en": "Bahamas",
      "zh-Hant": "巴哈馬"
    }
  }, {
    "key": "C154",
    "values": {
      "en": "Bahrain",
      "zh-Hant": "巴林"
    }
  }, {
    "key": "C155",
    "values": {
      "en": "Bangladesh",
      "zh-Hant": "孟加拉"
    }
  }, {
    "key": "C52",
    "values": {
      "en": "Barbados",
      "zh-Hant": "巴比多斯/巴巴多斯"
    }
  }, {
    "key": "C156",
    "values": {
      "en": "Belarus",
      "zh-Hant": "白俄羅斯"
    }
  }, {
    "key": "C11",
    "values": {
      "en": "Belgium",
      "zh-Hant": "比利時"
    }
  }, {
    "key": "C53",
    "values": {
      "en": "Belize",
      "zh-Hant": "百利茲"
    }
  }, {
    "key": "C157",
    "values": {
      "en": "Benin",
      "zh-Hant": "貝寧"
    }
  }, {
    "key": "C54",
    "values": {
      "en": "Bermuda",
      "zh-Hant": "百慕達"
    }
  }, {
    "key": "C158",
    "values": {
      "en": "Bhutan",
      "zh-Hant": "不丹"
    }
  }, {
    "key": "C159",
    "values": {
      "en": "Bolivia",
      "zh-Hant": "玻利維亞"
    }
  }, {
    "key": "C56",
    "values": {
      "en": "Bosnia and Herzegovina",
      "zh-Hant": "波斯尼亞及黑塞哥維那"
    }
  }, {
    "key": "C160",
    "values": {
      "en": "Botswana",
      "zh-Hant": "博茨瓦納"
    }
  }, {
    "key": "C57",
    "values": {
      "en": "Bouvet Island",
      "zh-Hant": "布維群島"
    }
  }, {
    "key": "C58",
    "values": {
      "en": "Brazil",
      "zh-Hant": "巴西"
    }
  }, {
    "key": "C59",
    "values": {
      "en": "British Indian Ocean Territory ",
      "zh-Hant": "英屬印度洋地區"
    }
  }, {
    "key": "C60",
    "values": {
      "en": "British Virgin Islands ",
      "zh-Hant": "英屬維京群島"
    }
  }, {
    "key": "C12",
    "values": {
      "en": "Brunei Darussalam",
      "zh-Hant": "文萊"
    }
  }, {
    "key": "C61",
    "values": {
      "en": "Bulgaria",
      "zh-Hant": "保加利亞"
    }
  }, {
    "key": "C161",
    "values": {
      "en": "Burkina Faso",
      "zh-Hant": "布基納法索"
    }
  }, {
    "key": "C162",
    "values": {
      "en": "Burundi",
      "zh-Hant": "布隆迪"
    }
  }, {
    "key": "C254",
    "values": {
      "en": "Cabo Verde",
      "zh-Hant": "佛得角"
    }
  }, {
    "key": "C163",
    "values": {
      "en": "Cambodia",
      "zh-Hant": "柬埔寨"
    }
  }, {
    "key": "C164",
    "values": {
      "en": "Cameroon",
      "zh-Hant": "喀麥隆"
    }
  }, {
    "key": "C13",
    "values": {
      "en": "Canada",
      "zh-Hant": "加拿大"
    }
  }, {
    "key": "C255",
    "values": {
      "en": "Caribbean Netherlands",
      "zh-Hant": "荷蘭加勒比區"
    }
  }, {
    "key": "C62",
    "values": {
      "en": "Cayman Islands",
      "zh-Hant": "開曼群島"
    }
  }, {
    "key": "C166",
    "values": {
      "en": "Central African Republic ",
      "zh-Hant": "中非共和國"
    }
  }, {
    "key": "C167",
    "values": {
      "en": "Chad",
      "zh-Hant": "乍得"
    }
  }, {
    "key": "C63",
    "values": {
      "en": "Chile",
      "zh-Hant": "智利"
    }
  }, {
    "key": "C64",
    "values": {
      "en": "Christmas Island",
      "zh-Hant": "聖誕島"
    }
  }, {
    "key": "C65",
    "values": {
      "en": "Cocos (Keeling) Islands ",
      "zh-Hant": "科科斯（基林）群島"
    }
  }, {
    "key": "C166",
    "values": {
      "en": "Colombia",
      "zh-Hant": "哥倫比亞"
    }
  }, {
    "key": "C169",
    "values": {
      "en": "Comoros ",
      "zh-Hant": "科摩羅"
    }
  }, {
    "key": "C171",
    "values": {
      "en": "Congo ",
      "zh-Hant": "剛果"
    }
  }, {
    "key": "C66",
    "values": {
      "en": "Cook Islands ",
      "zh-Hant": "庫克群島"
    }
  }, {
    "key": "C67",
    "values": {
      "en": "Costa Rica",
      "zh-Hant": "哥斯達黎加"
    }
  }, {
    "key": "C172",
    "values": {
      "en": "Cote d'Ivoire",
      "zh-Hant": "科特迪瓦"
    }
  }, {
    "key": "C14",
    "values": {
      "en": "Croatia",
      "zh-Hant": "克羅地亞"
    }
  }, {
    "key": "C173",
    "values": {
      "en": "Cuba",
      "zh-Hant": "古巴"
    }
  }, {
    "key": "C256",
    "values": {
      "en": "Curaçao",
      "zh-Hant": "庫拉索"
    }
  }, {
    "key": "C68",
    "values": {
      "en": "Cyprus",
      "zh-Hant": "塞浦路斯"
    }
  }, {
    "key": "C69",
    "values": {
      "en": "Czech Republic",
      "zh-Hant": "捷克"
    }
  }, {
    "key": "C171",
    "values": {
      "en": "Democratic Republic of the Congo",
      "zh-Hant": "剛果民主共和國"
    }
  }, {
    "key": "C15",
    "values": {
      "en": "Denmark",
      "zh-Hant": "丹麥"
    }
  }, {
    "key": "C174",
    "values": {
      "en": "Djibouti",
      "zh-Hant": "吉布提"
    }
  }, {
    "key": "C70",
    "values": {
      "en": "Dominica",
      "zh-Hant": "多米尼加"
    }
  }, {
    "key": "C71",
    "values": {
      "en": "Dominican Republic ",
      "zh-Hant": "多明尼加共和國"
    }
  }, {
    "key": "C72",
    "values": {
      "en": "Ecuador",
      "zh-Hant": "厄瓜多爾"
    }
  }, {
    "key": "C176",
    "values": {
      "en": "Egypt",
      "zh-Hant": "埃及"
    }
  }, {
    "key": "C73",
    "values": {
      "en": "El Salvador",
      "zh-Hant": "薩爾瓦多"
    }
  }, {
    "key": "C177",
    "values": {
      "en": "Equatorial Guinea",
      "zh-Hant": "赤道幾內亞"
    }
  }, {
    "key": "C178",
    "values": {
      "en": "Eritrea",
      "zh-Hant": "厄立特里亞"
    }
  }, {
    "key": "C16",
    "values": {
      "en": "Estonia",
      "zh-Hant": "愛沙尼亞"
    }
  }, {
    "key": "C179",
    "values": {
      "en": "Ethiopia",
      "zh-Hant": "埃塞俄比亞"
    }
  }, {
    "key": "C74",
    "values": {
      "en": "Falkland Islands",
      "zh-Hant": "福克蘭群島"
    }
  }, {
    "key": "C75",
    "values": {
      "en": "Faroe Islands ",
      "zh-Hant": "法羅群島"
    }
  }, {
    "key": "C103",
    "values": {
      "en": "Federated States of Micronesia",
      "zh-Hant": "密克羅尼西亞聯邦"
    }
  }, {
    "key": "C76",
    "values": {
      "en": "Fiji",
      "zh-Hant": "斐濟"
    }
  }, {
    "key": "C17",
    "values": {
      "en": "Finland",
      "zh-Hant": "芬蘭"
    }
  }, {
    "key": "C18",
    "values": {
      "en": "France",
      "zh-Hant": "法國"
    }
  }, {
    "key": "C77",
    "values": {
      "en": "French Guiana",
      "zh-Hant": "法屬圭亞那"
    }
  }, {
    "key": "C78",
    "values": {
      "en": "French Polynesia",
      "zh-Hant": "法屬波利尼西亞"
    }
  }, {
    "key": "C257",
    "values": {
      "en": "French Southern Territories ",
      "zh-Hant": "法屬南部地區"
    }
  }, {
    "key": "C180",
    "values": {
      "en": "Gabon",
      "zh-Hant": "加蓬"
    }
  }, {
    "key": "C181",
    "values": {
      "en": "Gambia ",
      "zh-Hant": "剛比亞"
    }
  }, {
    "key": "C79",
    "values": {
      "en": "Georgia",
      "zh-Hant": "格魯吉亞"
    }
  }, {
    "key": "C19",
    "values": {
      "en": "Germany",
      "zh-Hant": "德國"
    }
  }, {
    "key": "C182",
    "values": {
      "en": "Ghana",
      "zh-Hant": "加納"
    }
  }, {
    "key": "C180",
    "values": {
      "en": "Gibraltar",
      "zh-Hant": "直布羅陀"
    }
  }, {
    "key": "C20",
    "values": {
      "en": "Greece",
      "zh-Hant": "希臘"
    }
  }, {
    "key": "C81",
    "values": {
      "en": "Greenland",
      "zh-Hant": "格陵蘭島"
    }
  }, {
    "key": "C82",
    "values": {
      "en": "Grenada",
      "zh-Hant": "格林納達"
    }
  }, {
    "key": "C83",
    "values": {
      "en": "Guadeloupe",
      "zh-Hant": "瓜德羅普島"
    }
  }, {
    "key": "C84",
    "values": {
      "en": "Guam",
      "zh-Hant": "關島"
    }
  }, {
    "key": "C85",
    "values": {
      "en": "Guatemala",
      "zh-Hant": "危地馬拉"
    }
  }, {
    "key": "C258",
    "values": {
      "en": "Guernsey",
      "zh-Hant": "根西島"
    }
  }, {
    "key": "C183",
    "values": {
      "en": "Guinea",
      "zh-Hant": "幾內亞"
    }
  }, {
    "key": "C185",
    "values": {
      "en": "Guinea-Bissau",
      "zh-Hant": "幾內亞比紹"
    }
  }, {
    "key": "C87",
    "values": {
      "en": "Guyana",
      "zh-Hant": "圭亞那"
    }
  }, {
    "key": "C186",
    "values": {
      "en": "Haiti",
      "zh-Hant": "海地"
    }
  }, {
    "key": "C259",
    "values": {
      "en": "Heard Island and McDonald Islands",
      "zh-Hant": "赫爾特及麥克唐納群島"
    }
  }, {
    "key": "C90",
    "values": {
      "en": "Honduras",
      "zh-Hant": "洪都拉斯"
    }
  }, {
    "key": "C22",
    "values": {
      "en": "Hungary",
      "zh-Hant": "匈牙利"
    }
  }, {
    "key": "C23",
    "values": {
      "en": "Iceland",
      "zh-Hant": "冰島"
    }
  }, {
    "key": "C8",
    "values": {
      "en": "India",
      "zh-Hant": "印度"
    }
  }, {
    "key": "C5",
    "values": {
      "en": "Indonesia",
      "zh-Hant": "印尼"
    }
  }, {
    "key": "C187",
    "values": {
      "en": "Iran",
      "zh-Hant": "伊朗"
    }
  }, {
    "key": "C188",
    "values": {
      "en": "Iraq",
      "zh-Hant": "伊拉克"
    }
  }, {
    "key": "C24",
    "values": {
      "en": "Ireland",
      "zh-Hant": "愛爾蘭"
    }
  }, {
    "key": "C99",
    "values": {
      "en": "Isle of Man",
      "zh-Hant": "萌島"
    }
  }, {
    "key": "C189",
    "values": {
      "en": "Israel",
      "zh-Hant": "以色列"
    }
  }, {
    "key": "C25",
    "values": {
      "en": "Italy",
      "zh-Hant": "意大利"
    }
  }, {
    "key": "C91",
    "values": {
      "en": "Jamaica",
      "zh-Hant": "牙買加"
    }
  }, {
    "key": "C251",
    "values": {
      "en": "Japan",
      "zh-Hant": "日本"
    }
  }, {
    "key": "C92",
    "values": {
      "en": "Jersey",
      "zh-Hant": "澤西島"
    }
  }, {
    "key": "C190",
    "values": {
      "en": "Jordan",
      "zh-Hant": "約旦"
    }
  }, {
    "key": "C93",
    "values": {
      "en": "Kazakhstan",
      "zh-Hant": "哈薩克斯坦"
    }
  }, {
    "key": "C191",
    "values": {
      "en": "Kenya",
      "zh-Hant": "肯雅"
    }
  }, {
    "key": "C192",
    "values": {
      "en": "Kiribati",
      "zh-Hant": "基里巴斯"
    }
  }, {
    "key": "C194",
    "values": {
      "en": "Kuwait",
      "zh-Hant": "科威特"
    }
  }, {
    "key": "C94",
    "values": {
      "en": "Kyrgyzstan",
      "zh-Hant": "吉爾吉斯斯坦"
    }
  }, {
    "key": "C195",
    "values": {
      "en": "Laos",
      "zh-Hant": "老撾"
    }
  }, {
    "key": "C27",
    "values": {
      "en": "Latvia",
      "zh-Hant": "拉脫維亞"
    }
  }, {
    "key": "C196",
    "values": {
      "en": "Lebanon",
      "zh-Hant": "黎巴嫩"
    }
  }, {
    "key": "C197",
    "values": {
      "en": "Lesotho",
      "zh-Hant": "萊索托"
    }
  }, {
    "key": "C198",
    "values": {
      "en": "Liberia",
      "zh-Hant": "利比利亞"
    }
  }, {
    "key": "C199",
    "values": {
      "en": "Libya",
      "zh-Hant": "利比亞"
    }
  }, {
    "key": "C95",
    "values": {
      "en": "Liechtenstein",
      "zh-Hant": "列支敦士登"
    }
  }, {
    "key": "C28",
    "values": {
      "en": "Lithuania",
      "zh-Hant": "立陶宛"
    }
  }, {
    "key": "C96",
    "values": {
      "en": "Luxembourg",
      "zh-Hant": "盧森堡"
    }
  }, {
    "key": "C260",
    "values": {
      "en": "Macao",
      "zh-Hant": "澳門"
    }
  }, {
    "key": "C200",
    "values": {
      "en": "Macedonia",
      "zh-Hant": "馬其頓"
    }
  }, {
    "key": "C201",
    "values": {
      "en": "Madagascar",
      "zh-Hant": "馬達加斯加"
    }
  }, {
    "key": "C202",
    "values": {
      "en": "Malawi",
      "zh-Hant": "馬拉維"
    }
  }, {
    "key": "C3",
    "values": {
      "en": "Malaysia",
      "zh-Hant": "馬來西亞"
    }
  }, {
    "key": "C97",
    "values": {
      "en": "Maldives",
      "zh-Hant": "馬爾代夫"
    }
  }, {
    "key": "C203",
    "values": {
      "en": "Mali",
      "zh-Hant": "馬里"
    }
  }, {
    "key": "C98",
    "values": {
      "en": "Malta",
      "zh-Hant": "馬耳他"
    }
  }, {
    "key": "C100",
    "values": {
      "en": "Marshall Islands ",
      "zh-Hant": "馬紹爾群島"
    }
  }, {
    "key": "C101",
    "values": {
      "en": "Martinique",
      "zh-Hant": "馬提尼克島"
    }
  }, {
    "key": "C204",
    "values": {
      "en": "Mauritania",
      "zh-Hant": "毛裏塔尼亞"
    }
  }, {
    "key": "C205",
    "values": {
      "en": "Mauritius",
      "zh-Hant": "毛里求斯"
    }
  }, {
    "key": "C206",
    "values": {
      "en": "Mayotte",
      "zh-Hant": "馬約特島"
    }
  }, {
    "key": "C102",
    "values": {
      "en": "Mexico",
      "zh-Hant": "墨西哥"
    }
  }, {
    "key": "C207",
    "values": {
      "en": "Moldova ",
      "zh-Hant": "摩爾多瓦共和國"
    }
  }, {
    "key": "C104",
    "values": {
      "en": "Monaco",
      "zh-Hant": "摩納哥"
    }
  }, {
    "key": "C208",
    "values": {
      "en": "Mongolia",
      "zh-Hant": "蒙古"
    }
  }, {
    "key": "C015",
    "values": {
      "en": "Montenegro",
      "zh-Hant": "黑山"
    }
  }, {
    "key": "C106",
    "values": {
      "en": "Montserrat",
      "zh-Hant": "蒙特塞拉特島"
    }
  }, {
    "key": "C209",
    "values": {
      "en": "Morocco",
      "zh-Hant": "摩洛哥"
    }
  }, {
    "key": "C210",
    "values": {
      "en": "Mozambique",
      "zh-Hant": "莫桑比克"
    }
  }, {
    "key": "C41",
    "values": {
      "en": "Myanmar",
      "zh-Hant": "緬甸"
    }
  }, {
    "key": "C211",
    "values": {
      "en": "Namibia",
      "zh-Hant": "納米比亞"
    }
  }, {
    "key": "C108",
    "values": {
      "en": "Nauru",
      "zh-Hant": "瑙魯島"
    }
  }, {
    "key": "C212",
    "values": {
      "en": "Nepal",
      "zh-Hant": "尼泊爾"
    }
  }, {
    "key": "C30",
    "values": {
      "en": "Netherlands ",
      "zh-Hant": "荷蘭"
    }
  }, {
    "key": "C109",
    "values": {
      "en": "New Caledonia",
      "zh-Hant": "新喀里多尼亞"
    }
  }, {
    "key": "C32",
    "values": {
      "en": "New Zealand",
      "zh-Hant": "新西蘭"
    }
  }, {
    "key": "C110",
    "values": {
      "en": "Nicaragua",
      "zh-Hant": "尼加拉瓜"
    }
  }, {
    "key": "C213",
    "values": {
      "en": "Niger ",
      "zh-Hant": "尼日爾共和國"
    }
  }, {
    "key": "C214",
    "values": {
      "en": "Nigeria",
      "zh-Hant": "尼日里亞"
    }
  }, {
    "key": "C111",
    "values": {
      "en": "Niue",
      "zh-Hant": "紐埃島"
    }
  }, {
    "key": "C112",
    "values": {
      "en": "Norfolk Island",
      "zh-Hant": "諾福克群島"
    }
  }, {
    "key": "C215",
    "values": {
      "en": "North Korea",
      "zh-Hant": "北韓"
    }
  }, {
    "key": "C113",
    "values": {
      "en": "Northern Mariana Islands ",
      "zh-Hant": "北馬里亞納群島"
    }
  }, {
    "key": "C33",
    "values": {
      "en": "Norway",
      "zh-Hant": "挪威"
    }
  }, {
    "key": "C216",
    "values": {
      "en": "Oman",
      "zh-Hant": "阿曼"
    }
  }, {
    "key": "C217",
    "values": {
      "en": "Pakistan",
      "zh-Hant": "巴基斯坦"
    }
  }, {
    "key": "C115",
    "values": {
      "en": "Palau",
      "zh-Hant": "帕勞"
    }
  }, {
    "key": "C116",
    "values": {
      "en": "Panama",
      "zh-Hant": "巴拿馬"
    }
  }, {
    "key": "C117",
    "values": {
      "en": "Papua New Guinea",
      "zh-Hant": "巴布亞新幾內亞"
    }
  }, {
    "key": "C219",
    "values": {
      "en": "Paraguay",
      "zh-Hant": "巴拉圭"
    }
  }, {
    "key": "C118",
    "values": {
      "en": "Peru",
      "zh-Hant": "秘魯"
    }
  }, {
    "key": "C119",
    "values": {
      "en": "Philippines",
      "zh-Hant": "菲律賓"
    }
  }, {
    "key": "C120",
    "values": {
      "en": "Pitcairn",
      "zh-Hant": "皮特凱恩"
    }
  }, {
    "key": "C34",
    "values": {
      "en": "Poland",
      "zh-Hant": "波蘭"
    }
  }, {
    "key": "C121",
    "values": {
      "en": "Portugal",
      "zh-Hant": "葡萄牙"
    }
  }, {
    "key": "C122",
    "values": {
      "en": "Puerto Rico",
      "zh-Hant": "波多黎各島"
    }
  }, {
    "key": "C220",
    "values": {
      "en": "Qatar",
      "zh-Hant": "卡塔爾"
    }
  }, {
    "key": "C221",
    "values": {
      "en": "Reunion",
      "zh-Hant": "留尼汪島"
    }
  }, {
    "key": "C123",
    "values": {
      "en": "Romania",
      "zh-Hant": "羅馬尼亞"
    }
  }, {
    "key": "C149",
    "values": {
      "en": "Russia",
      "zh-Hant": "俄羅斯"
    }
  }, {
    "key": "C222",
    "values": {
      "en": "Rwanda",
      "zh-Hant": "盧旺達"
    }
  }, {
    "key": "C124",
    "values": {
      "en": "Saint Barthelemy",
      "zh-Hant": "聖巴托洛繆島"
    }
  }, {
    "key": "C125",
    "values": {
      "en": "Saint Helena, Ascension and Tristan da Cunha",
      "zh-Hant": "聖赫勒拿、阿森松與特里斯坦達庫尼亞"
    }
  }, {
    "key": "C126",
    "values": {
      "en": "Saint Kitts and Nevis",
      "zh-Hant": "聖克里斯托弗和尼維斯聯邦"
    }
  }, {
    "key": "C127",
    "values": {
      "en": "Saint Lucia",
      "zh-Hant": "聖盧西亞"
    }
  }, {
    "key": "C129",
    "values": {
      "en": "Saint Pierre and Miquelon",
      "zh-Hant": "聖皮埃爾島和密克隆島"
    }
  }, {
    "key": "C130",
    "values": {
      "en": "Saint Vincent and the Grenadines",
      "zh-Hant": "聖文森特和格林納丁斯"
    }
  }, {
    "key": "C128",
    "values": {
      "en": "Saint-Martin",
      "zh-Hant": "法屬聖馬丁島"
    }
  }, {
    "key": "C223",
    "values": {
      "en": "Samoa",
      "zh-Hant": "薩摩亞"
    }
  }, {
    "key": "C131",
    "values": {
      "en": "San Marino",
      "zh-Hant": "聖馬力諾"
    }
  }, {
    "key": "C224",
    "values": {
      "en": "São Tomé and Príncipe",
      "zh-Hant": "聖多美和普林西比"
    }
  }, {
    "key": "C225",
    "values": {
      "en": "Saudi Arabia",
      "zh-Hant": "沙地阿拉伯"
    }
  }, {
    "key": "C226",
    "values": {
      "en": "Senegal",
      "zh-Hant": "塞內加爾"
    }
  }, {
    "key": "C132",
    "values": {
      "en": "Serbia",
      "zh-Hant": "塞爾維亞"
    }
  }, {
    "key": "C227",
    "values": {
      "en": "Seychelles",
      "zh-Hant": "塞舌爾"
    }
  }, {
    "key": "C228",
    "values": {
      "en": "Sierra Leone",
      "zh-Hant": "塞拉里昂"
    }
  }, {
    "key": "C1",
    "values": {
      "en": "Singapore",
      "zh-Hant": "新加坡"
    }
  }, {
    "key": "C261",
    "values": {
      "en": "Sint Maarten",
      "zh-Hant": "荷屬聖馬丁島"
    }
  }, {
    "key": "C35",
    "values": {
      "en": "Slovakia",
      "zh-Hant": "斯洛伐克共和國"
    }
  }, {
    "key": "C133",
    "values": {
      "en": "Slovenia",
      "zh-Hant": "斯洛文尼亞"
    }
  }, {
    "key": "C229",
    "values": {
      "en": "Solomon Islands",
      "zh-Hant": "所羅門群島"
    }
  }, {
    "key": "C230",
    "values": {
      "en": "Somalia",
      "zh-Hant": "索馬利亞"
    }
  }, {
    "key": "C43",
    "values": {
      "en": "South Africa",
      "zh-Hant": "南非"
    }
  }, {
    "key": "C262",
    "values": {
      "en": "South Georgia and the South Sandwich Islands",
      "zh-Hant": "南喬治亞島及南桑威奇群島"
    }
  }, {
    "key": "C26",
    "values": {
      "en": "South Korea",
      "zh-Hant": "南韓"
    }
  }, {
    "key": "C231",
    "values": {
      "en": "South Sudan",
      "zh-Hant": "南蘇丹"
    }
  }, {
    "key": "C136",
    "values": {
      "en": "Spain",
      "zh-Hant": "西班牙"
    }
  }, {
    "key": "C232",
    "values": {
      "en": "Sri Lanka",
      "zh-Hant": "斯里蘭卡"
    }
  }, {
    "key": "C218",
    "values": {
      "en": "State of Palestine",
      "zh-Hant": "巴勒斯坦"
    }
  }, {
    "key": "C233",
    "values": {
      "en": "Sudan ",
      "zh-Hant": "蘇丹"
    }
  }, {
    "key": "C234",
    "values": {
      "en": "Suriname",
      "zh-Hant": "蘇利南"
    }
  }, {
    "key": "C137",
    "values": {
      "en": "Svalbard and Jan Mayen",
      "zh-Hant": "斯瓦爾巴群島及揚馬延島"
    }
  }, {
    "key": "C235",
    "values": {
      "en": "Swaziland",
      "zh-Hant": "斯威士蘭"
    }
  }, {
    "key": "C36",
    "values": {
      "en": "Sweden",
      "zh-Hant": "瑞典"
    }
  }, {
    "key": "C37",
    "values": {
      "en": "Switzerland",
      "zh-Hant": "瑞士"
    }
  }, {
    "key": "C236",
    "values": {
      "en": "Syrian Arab Republic",
      "zh-Hant": "敘利亞"
    }
  }, {
    "key": "C38",
    "values": {
      "en": "Taiwan ",
      "zh-Hant": "台灣"
    }
  }, {
    "key": "C138",
    "values": {
      "en": "Tajikistan",
      "zh-Hant": "塔吉克斯坦"
    }
  }, {
    "key": "C237",
    "values": {
      "en": "Tanzania",
      "zh-Hant": "坦桑尼亞"
    }
  }, {
    "key": "C6",
    "values": {
      "en": "Thailand",
      "zh-Hant": "泰國"
    }
  }, {
    "key": "C175",
    "values": {
      "en": "Timor-Leste",
      "zh-Hant": "東帝汶"
    }
  }, {
    "key": "C238",
    "values": {
      "en": "Togo",
      "zh-Hant": "多哥"
    }
  }, {
    "key": "C139",
    "values": {
      "en": "Tokelau",
      "zh-Hant": "托克勞群島"
    }
  }, {
    "key": "C140",
    "values": {
      "en": "Tonga",
      "zh-Hant": "湯加"
    }
  }, {
    "key": "C141",
    "values": {
      "en": "Trinidad and Tobago",
      "zh-Hant": "千里達和多巴哥"
    }
  }, {
    "key": "C239",
    "values": {
      "en": "Tunisia",
      "zh-Hant": "突尼西亞"
    }
  }, {
    "key": "C142",
    "values": {
      "en": "Turkey",
      "zh-Hant": "土耳其"
    }
  }, {
    "key": "C240",
    "values": {
      "en": "Turkmenistan",
      "zh-Hant": "土庫曼斯坦"
    }
  }, {
    "key": "C143",
    "values": {
      "en": "Turks and Caicos Islands ",
      "zh-Hant": "特克斯及凱科斯群島"
    }
  }, {
    "key": "C241",
    "values": {
      "en": "Tuvalu",
      "zh-Hant": "圖瓦盧"
    }
  }, {
    "key": "C242",
    "values": {
      "en": "Uganda",
      "zh-Hant": "烏干達"
    }
  }, {
    "key": "C243",
    "values": {
      "en": "Ukraine",
      "zh-Hant": "烏克蘭"
    }
  }, {
    "key": "C244",
    "values": {
      "en": "United Arab Emirates",
      "zh-Hant": "阿拉伯聯合酋長國"
    }
  }, {
    "key": "C39",
    "values": {
      "en": "United Kingdom",
      "zh-Hant": "英國"
    }
  }, {
    "key": "C263",
    "values": {
      "en": "United States Minor Outlying Islands",
      "zh-Hant": "美屬邊疆群島"
    }
  }, {
    "key": "C252",
    "values": {
      "en": "United States of America ",
      "zh-Hant": "美國"
    }
  }, {
    "key": "C144",
    "values": {
      "en": "Uruguay",
      "zh-Hant": "烏拉圭"
    }
  }, {
    "key": "C245",
    "values": {
      "en": "Uzbekistan",
      "zh-Hant": "烏茲別克斯坦"
    }
  }, {
    "key": "C246",
    "values": {
      "en": "Vanuatu",
      "zh-Hant": "瓦努阿圖"
    }
  }, {
    "key": "C264",
    "values": {
      "en": "Vatican City",
      "zh-Hant": "梵蒂岡"
    }
  }, {
    "key": "C145",
    "values": {
      "en": "Venezuela",
      "zh-Hant": "委內瑞拉"
    }
  }, {
    "key": "C7",
    "values": {
      "en": "Vietnam",
      "zh-Hant": "越南"
    }
  }, {
    "key": "C146",
    "values": {
      "en": "Virgin Islands of the United States",
      "zh-Hant": "美屬維京群島"
    }
  }, {
    "key": "C147",
    "values": {
      "en": "Wallis and Futuna",
      "zh-Hant": "瓦利斯群島及富圖納群島"
    }
  }, {
    "key": "C247",
    "values": {
      "en": "Western Sahara",
      "zh-Hant": "西撒哈拉"
    }
  }, {
    "key": "C248",
    "values": {
      "en": "Yemen",
      "zh-Hant": "也門"
    }
  }, {
    "key": "C249",
    "values": {
      "en": "Zambia",
      "zh-Hant": "贊比亞"
    }
  }, {
    "key": "C250",
    "values": {
      "en": "Zimbabwe",
      "zh-Hant": "津巴布韋"
    }
  }],
  "genderMapping": [{
    "key": "M",
    "values": {
      "en": "Male",
      "zh-Hant": "男"
    }
  }, {
    "key": "F",
    "values": {
      "en": "Female",
      "zh-Hant": "女"
    }
  }],
  "educationMapping": [{
    "key": "primary",
    "values": {
      "en": "Primary School or below",
      "zh-Hant": "小學或以下"
    }
  }, {
    "key": "secondary",
    "values": {
      "en": "Secondary School",
      "zh-Hant": "中學"
    }
  }, {
    "key": "diploma",
    "values": {
      "en": "Post Secondary/ Associate Degree/ Diploma",
      "zh-Hant": "大專/ 副學士/ 文憑"
    }
  }, {
    "key": "university",
    "values": {
      "en": "University or above ",
      "zh-Hant": "大學或以上"
    }
  }],
  "occupationMapping": [{
    "key": "024001",
    "values": {
      "en": "Abattoir Inspector",
      "zh-Hant": "屠場檢查員 "
    }
  }, {
    "key": "024002",
    "values": {
      "en": "Abattoir Manager",
      "zh-Hant": "屠場經理"
    }
  }, {
    "key": "024003",
    "values": {
      "en": "Abattoir Worker",
      "zh-Hant": "屠場工人"
    }
  }, {
    "key": "002001",
    "values": {
      "en": "Account Executive - Advertising ",
      "zh-Hant": "客戶主任 - 廣告業"
    }
  }, {
    "key": "060001",
    "values": {
      "en": "Account Executive - Financial Institution (Banking, Investment and Insurance, etc) ",
      "zh-Hant": "客戶主任 - 金融業 (銀行、投資及保險等)"
    }
  }, {
    "key": "072001",
    "values": {
      "en": "Accountant",
      "zh-Hant": "會計師"
    }
  }, {
    "key": "085001",
    "values": {
      "en": "Accreditor  - Jewellery",
      "zh-Hant": "鑑證人 - 珠寶業"
    }
  }, {
    "key": "030002",
    "values": {
      "en": "Acrobat - Circus ",
      "zh-Hant": "雜技演員 - 馬戲"
    }
  }, {
    "key": "059001",
    "values": {
      "en": "Actor or Actress - No kung fu fighting ",
      "zh-Hant": "男或女演員 - 不參予拍攝動作片"
    }
  }, {
    "key": "059002",
    "values": {
      "en": "Actor or Actress - with kung fu fighting ",
      "zh-Hant": "男或女演員 - 參予拍攝動作片"
    }
  }, {
    "key": "060002",
    "values": {
      "en": "Actuary",
      "zh-Hant": "精算師"
    }
  }, {
    "key": "081001",
    "values": {
      "en": "Acupuncturist",
      "zh-Hant": "針灸師"
    }
  }, {
    "key": "081002",
    "values": {
      "en": "Administrator - Hospital & Clinic",
      "zh-Hant": "行政人員 - 醫院及診所"
    }
  }, {
    "key": "060003",
    "values": {
      "en": "Agent  - Financial Institution (Banking, Investment and Insurance, etc) ",
      "zh-Hant": "代理 - 金融業 (銀行、投資及保險等)"
    }
  }, {
    "key": "012001",
    "values": {
      "en": "Air Traffic Controller (Indoor Duties)",
      "zh-Hant": "航空交通管制官（室內職務）"
    }
  }, {
    "key": "012002",
    "values": {
      "en": "Air Traffic Controller (Outdoor)",
      "zh-Hant": "航空交通管制官 (戶外工作)"
    }
  }, {
    "key": "012003",
    "values": {
      "en": "Aircraft Designer (Indoor Duties)",
      "zh-Hant": "飛機設計師（室內職務）"
    }
  }, {
    "key": "012004",
    "values": {
      "en": "Aircraft Instrument Maker (Indoor Duties)",
      "zh-Hant": "飛機儀器製造者（室內職務）"
    }
  }, {
    "key": "012005",
    "values": {
      "en": "Aircraft Refueller (Repair & Maintenance)",
      "zh-Hant": "飛機加油員 (維修及保養)"
    }
  }, {
    "key": "012006",
    "values": {
      "en": "Aircrew: Hostess, Steward (Airline)",
      "zh-Hant": "航空機組人員：空中服務員(航空公司)"
    }
  }, {
    "key": "012007",
    "values": {
      "en": "Aircrew: Hostess, Steward (Private Plane)",
      "zh-Hant": "機組人員：空中服務員 (私人飛機)"
    }
  }, {
    "key": "012008",
    "values": {
      "en": "Airforce Winchman (Private Plane)",
      "zh-Hant": "空軍絞車工 (私人飛機)"
    }
  }, {
    "key": "012009",
    "values": {
      "en": "Airport Manager (Indoor Duties)",
      "zh-Hant": "機場經理（室內職務）"
    }
  }, {
    "key": "081003",
    "values": {
      "en": "Ambulanceman",
      "zh-Hant": "救護員"
    }
  }, {
    "key": "061001",
    "values": {
      "en": "Ambulanceman - Fire Services Department",
      "zh-Hant": "救護員 - 消防處"
    }
  }, {
    "key": "058001",
    "values": {
      "en": "Animal breeder: Domestic - Farmland",
      "zh-Hant": "育養禽畜工人 - 農場"
    }
  }, {
    "key": "005001",
    "values": {
      "en": "Animal Trainer - Amusement Park",
      "zh-Hant": "馴獸師 - 遊樂園"
    }
  }, {
    "key": "030001",
    "values": {
      "en": "Animal Trainer - Circus",
      "zh-Hant": "馴獸師 - 馬戲"
    }
  }, {
    "key": "141001",
    "values": {
      "en": "Animal Welfare Inspector - Veterinary",
      "zh-Hant": "動物福利督察 - 獸醫業"
    }
  }, {
    "key": "038001",
    "values": {
      "en": "Animator",
      "zh-Hant": "動畫繪製員"
    }
  }, {
    "key": "074001",
    "values": {
      "en": "Apprentice - Goldsmith",
      "zh-Hant": "學徒 - 金匠"
    }
  }, {
    "key": "037001",
    "values": {
      "en": "Architect (Office work)",
      "zh-Hant": "建築師 (辦公室工作)"
    }
  }, {
    "key": "098003",
    "values": {
      "en": "Army Personnel (non-HK Regiment)",
      "zh-Hant": "軍隊人員(非香港)"
    }
  }, {
    "key": "098002",
    "values": {
      "en": "Army Personnel: Aviation - Military Force (Rank and File, HK Regiment)",
      "zh-Hant": "軍隊人員：空軍 - 軍隊 (員佐級，香港)"
    }
  }, {
    "key": "098001",
    "values": {
      "en": "Army Personnel: No aviation - Military Force (Rank and File, HK Regiment)",
      "zh-Hant": "軍隊人員：非空軍 - 軍隊 (員佐級，香港)"
    }
  }, {
    "key": "017001",
    "values": {
      "en": "Aromatherapist - Beauty Parlour ",
      "zh-Hant": "香薰師 - 美容院"
    }
  }, {
    "key": "017004",
    "values": {
      "en": "Aromatherapist - Parlour At home",
      "zh-Hant": "香薰師 - 家庭式美容院"
    }
  }, {
    "key": "007006",
    "values": {
      "en": "Art Dealer",
      "zh-Hant": "藝術品經銷商"
    }
  }, {
    "key": "007007",
    "values": {
      "en": "Art Gallery Attendant",
      "zh-Hant": "美術館服務員"
    }
  }, {
    "key": "007008",
    "values": {
      "en": "Art Gallery Curator",
      "zh-Hant": "美術館館長 "
    }
  }, {
    "key": "007009",
    "values": {
      "en": "Artist",
      "zh-Hant": "藝術家"
    }
  }, {
    "key": "007010",
    "values": {
      "en": "Artist (Freelance)",
      "zh-Hant": "藝術家 (自由職業)"
    }
  }, {
    "key": "079001",
    "values": {
      "en": "Asphalter - Highway, Road & Street Construction",
      "zh-Hant": "瀝青工 - 高速公路、道路及街道建築"
    }
  }, {
    "key": "053001",
    "values": {
      "en": "Assembler - Electronic Industry",
      "zh-Hant": "裝配員 - 電子業"
    }
  }, {
    "key": "018001",
    "values": {
      "en": "Assistant  - Bicycle Shop ",
      "zh-Hant": "店員 - 單車店"
    }
  }, {
    "key": "113005",
    "values": {
      "en": "Assistant - Printing & Publishing Industry ",
      "zh-Hant": "助理 - 印刷及出版業"
    }
  }, {
    "key": "101001",
    "values": {
      "en": "Assistant Editor - Newspaper Industry",
      "zh-Hant": "助理編輯 - 報業"
    }
  }, {
    "key": "128005",
    "values": {
      "en": "Athletes",
      "zh-Hant": "運動員"
    }
  }, {
    "key": "005002",
    "values": {
      "en": "Attendant - Amusement Park",
      "zh-Hant": "服務員 - 遊樂園 "
    }
  }, {
    "key": "015001",
    "values": {
      "en": "Attendant - Bath / Massage Parlours (licensed)",
      "zh-Hant": "服務員 - 浴室/按摩院 (持牌)"
    }
  }, {
    "key": "019001",
    "values": {
      "en": "Attendant - Billiard Room / Snooker Centre",
      "zh-Hant": "服務員 - 桌球室/桌球中心"
    }
  }, {
    "key": "089001",
    "values": {
      "en": "Attendant - Lift",
      "zh-Hant": "服務員 - 升降機"
    }
  }, {
    "key": "116001",
    "values": {
      "en": "Attendant - Public Transport (e.g. MTR, LTR, KCR, Tram)",
      "zh-Hant": "服務員 - 公共交通 (如地鐵，輕鐵，九鐵，電車)"
    }
  }, {
    "key": "005003",
    "values": {
      "en": "Attendant - Zoo / Aquarium",
      "zh-Hant": "服務員 - 動物園/水族館"
    }
  }, {
    "key": "092001",
    "values": {
      "en": "Attendant (non-cleaning ) - Mahjong School",
      "zh-Hant": "服務員（非清潔） - 麻雀館"
    }
  }, {
    "key": "029001",
    "values": {
      "en": "Attendant / Usher  - Cinema or Concert Hall",
      "zh-Hant": "服務員 / 帶位 - 電影院或戲院"
    }
  }, {
    "key": "009001",
    "values": {
      "en": "Auctioneer",
      "zh-Hant": "拍賣官"
    }
  }, {
    "key": "072002",
    "values": {
      "en": "Auditor",
      "zh-Hant": "核數師"
    }
  }, {
    "key": "007011",
    "values": {
      "en": "Author (Freelance)",
      "zh-Hant": "作家  (自由職業)"
    }
  }, {
    "key": "013001",
    "values": {
      "en": "Babysitter ",
      "zh-Hant": "保姆"
    }
  }, {
    "key": "014001",
    "values": {
      "en": "Baker - Bakery",
      "zh-Hant": "麵包師 - 麵包店"
    }
  }, {
    "key": "060004",
    "values": {
      "en": "Bank Teller",
      "zh-Hant": "銀行出納員"
    }
  }, {
    "key": "060005",
    "values": {
      "en": "Banker",
      "zh-Hant": "銀行家 "
    }
  }, {
    "key": "094001",
    "values": {
      "en": "Barge operator (Outside Local Harbour)",
      "zh-Hant": "駁船操作員 (非本地港口)"
    }
  }, {
    "key": "094002",
    "values": {
      "en": "Bargemaster (Outside Local Harbour)",
      "zh-Hant": "駁船船主 (非本地港口)"
    }
  }, {
    "key": "040001",
    "values": {
      "en": "Barrister",
      "zh-Hant": "大律師"
    }
  }, {
    "key": "055001",
    "values": {
      "en": "Bartender - Lounge / Karaoke/ Bar / Disco (high class hotel)",
      "zh-Hant": "調酒師 - 酒廊/卡拉OK/酒吧/的士高（高級酒店）"
    }
  }, {
    "key": "055002",
    "values": {
      "en": "Bartender - Lounge / Karaoke/ Bar / Disco (licensed)",
      "zh-Hant": "調酒師 - 酒廊/卡拉OK/酒吧/的士高(有牌)"
    }
  }, {
    "key": "017002",
    "values": {
      "en": "Beautician - Beauty Parlour ",
      "zh-Hant": "美容師 - 美容院"
    }
  }, {
    "key": "017005",
    "values": {
      "en": "Beautician - Parlour At home",
      "zh-Hant": "美容師 - 家庭式美容院"
    }
  }, {
    "key": "017003",
    "values": {
      "en": "Beauty Consultant - Beauty Parlour ",
      "zh-Hant": "美容顧問 - 美容院"
    }
  }, {
    "key": "058002",
    "values": {
      "en": "Beekeeper - Farmland",
      "zh-Hant": "養蜂員 - 農場"
    }
  }, {
    "key": "080001",
    "values": {
      "en": "Betting Seller  - Horse Racing",
      "zh-Hant": "博彩售票員 - 賽馬"
    }
  }, {
    "key": "097001",
    "values": {
      "en": "Blacksmith",
      "zh-Hant": "鐵匠"
    }
  }, {
    "key": "079002",
    "values": {
      "en": "Blaster - Highway, Road & Street Construction",
      "zh-Hant": "爆石工人 - 高速公路、道路及街道建築"
    }
  }, {
    "key": "081004",
    "values": {
      "en": "Bonesetter ",
      "zh-Hant": "跌打醫師"
    }
  }, {
    "key": "087001",
    "values": {
      "en": "Book-keeper - Library / Museum ",
      "zh-Hant": "簿記員 - 圖書館 /博物館"
    }
  }, {
    "key": "050001",
    "values": {
      "en": "Bookmaker ",
      "zh-Hant": "出版人"
    }
  }, {
    "key": "106001",
    "values": {
      "en": "Box Maker - Paper & Pulp Industry ",
      "zh-Hant": "紙盒製造員 - 紙及紙漿製造業"
    }
  }, {
    "key": "128006",
    "values": {
      "en": "Boxer: Amateur",
      "zh-Hant": "拳擊手: 業餘"
    }
  }, {
    "key": "128001",
    "values": {
      "en": "Boxer: Professional ",
      "zh-Hant": "職業拳手"
    }
  }, {
    "key": "058003",
    "values": {
      "en": "Breeder: Fish - Farmland",
      "zh-Hant": "養魚家 - 農場"
    }
  }, {
    "key": "058004",
    "values": {
      "en": "Breeder: Horses - Farmland",
      "zh-Hant": "養馬家 - 農場"
    }
  }, {
    "key": "037002",
    "values": {
      "en": "Bricklayer - Construction Industry (Work at Height)",
      "zh-Hant": "砌磚匠 - 建築業 (高空工作)"
    }
  }, {
    "key": "060006",
    "values": {
      "en": "Broker - Financial Institution (Banking, Investment and Insurance, etc) ",
      "zh-Hant": "經紀人 - 金融業 (銀行、投資及保險等)"
    }
  }, {
    "key": "122001",
    "values": {
      "en": "Broker - Insurance",
      "zh-Hant": "經紀人 - 保險"
    }
  }, {
    "key": "037003",
    "values": {
      "en": "Bulldozer Drive - Construction Industry",
      "zh-Hant": "推土機司機 - 建築業"
    }
  }, {
    "key": "049001",
    "values": {
      "en": "Bus Driver",
      "zh-Hant": "巴士司機"
    }
  }, {
    "key": "049002",
    "values": {
      "en": "Bus Station Inspector",
      "zh-Hant": "巴士站巡視員"
    }
  }, {
    "key": "024004",
    "values": {
      "en": "Butcher / Meat Cutter",
      "zh-Hant": "肉販 / 屠夫"
    }
  }, {
    "key": "136001",
    "values": {
      "en": "Buyer: Outdoor Work - Trading",
      "zh-Hant": "買手: 戶外工作 - 貿易 "
    }
  }, {
    "key": "094003",
    "values": {
      "en": "Cabin crew - Marine Industry (Outside Local Harbour)",
      "zh-Hant": "海員 - 航海業 (非本地港口)"
    }
  }, {
    "key": "053002",
    "values": {
      "en": "Cable Spicer - Electronic Industry",
      "zh-Hant": "編接電纜工人 - 電子業"
    }
  }, {
    "key": "094004",
    "values": {
      "en": "Captain - Fishing (Outside Local Harbour)",
      "zh-Hant": "船長 - 捕魚(非本地港口)"
    }
  }, {
    "key": "064001",
    "values": {
      "en": "Captain/Supervisor  - Restaurant, Fast food shop, Canteen, etc",
      "zh-Hant": "部長/主任 - 餐廳，快餐店，食堂等"
    }
  }, {
    "key": "025001",
    "values": {
      "en": "Card dealer  - Casino",
      "zh-Hant": "荷官 - 賭場"
    }
  }, {
    "key": "010001",
    "values": {
      "en": "Carpark Attendant",
      "zh-Hant": "停車場服務員"
    }
  }, {
    "key": "037004",
    "values": {
      "en": "Carpenter - Construction Industry (Work at Height)",
      "zh-Hant": "木匠 - 建造業 (高空工作)"
    }
  }, {
    "key": "023001",
    "values": {
      "en": "Carpenter (Indoor Duties, not in construction site)",
      "zh-Hant": "木匠 (室內工作, 非在地盤)"
    }
  }, {
    "key": "032001",
    "values": {
      "en": "Carpet Cleaner - Cleaning & Repairing (Indoor)",
      "zh-Hant": "地毯清潔員 - 清潔及維修(戶內)"
    }
  }, {
    "key": "032002",
    "values": {
      "en": "Carpet Installer - Cleaning & Repairing (Indoor)",
      "zh-Hant": "地毯安裝員 - 清潔及維修(戶內)"
    }
  }, {
    "key": "038002",
    "values": {
      "en": "Cartoonist",
      "zh-Hant": "漫畫家"
    }
  }, {
    "key": "015002",
    "values": {
      "en": "Cashier  - Bath / Massage Parlours (licensed)",
      "zh-Hant": "收銀員 - 浴室/按摩院 (持牌)"
    }
  }, {
    "key": "019002",
    "values": {
      "en": "Cashier  - Billiard Room / Snooker Centre",
      "zh-Hant": "收銀員 - 桌球室/桌球中心"
    }
  }, {
    "key": "055003",
    "values": {
      "en": "Cashier  - Lounge / Karaoke/ Bar / Disco (high class hotel)",
      "zh-Hant": "收銀員 - 酒廊/卡拉OK/酒吧/的士高（高級酒店）"
    }
  }, {
    "key": "092002",
    "values": {
      "en": "Cashier  - Mahjong School",
      "zh-Hant": "收銀員 - 麻雀館"
    }
  }, {
    "key": "005004",
    "values": {
      "en": "Cashier - Amusement Park",
      "zh-Hant": "收銀員 - 遊樂園"
    }
  }, {
    "key": "025002",
    "values": {
      "en": "Cashier - Casino",
      "zh-Hant": "收銀員 - 賭場"
    }
  }, {
    "key": "072003",
    "values": {
      "en": "Cashier - General",
      "zh-Hant": "收銀員 - 一般工作"
    }
  }, {
    "key": "026001",
    "values": {
      "en": "Caterer (Freelance)",
      "zh-Hant": "宴會籌備人（自由職業）"
    }
  }, {
    "key": "049003",
    "values": {
      "en": "Cement Truck Driver",
      "zh-Hant": "混凝土車司機"
    }
  }, {
    "key": "072004",
    "values": {
      "en": "CEO / General Manager / Managing Diretor - Office work only",
      "zh-Hant": "行政總裁 / 總經理 / 董事總經理 - 寫字樓工作"
    }
  }, {
    "key": "049004",
    "values": {
      "en": "Chauffeur",
      "zh-Hant": "私家車司機"
    }
  }, {
    "key": "094005",
    "values": {
      "en": "Chief Engineer - Marine Industry (Within Local Harbor) ",
      "zh-Hant": "首席工程師 - 航海業 (港內水域)"
    }
  }, {
    "key": "094006",
    "values": {
      "en": "Chief Officer - Ferry, within Local Harbor ",
      "zh-Hant": "總監 - 渡輪, 港內水域"
    }
  }, {
    "key": "023002",
    "values": {
      "en": "Chimney Sweep - Building Maintenance (Outside Building, Curtain Wall)",
      "zh-Hant": "掃煙囪者 - 建築維修（外面大廈幕牆）"
    }
  }, {
    "key": "081005",
    "values": {
      "en": "Chinese Medical Practitioner",
      "zh-Hant": "中醫師 "
    }
  }, {
    "key": "081006",
    "values": {
      "en": "Chiropractor",
      "zh-Hant": "脊醫"
    }
  }, {
    "key": "042001",
    "values": {
      "en": "Choreographer - Dancers",
      "zh-Hant": "編蹈員 - 舞蹈員"
    }
  }, {
    "key": "029002",
    "values": {
      "en": "Cinema Manager ",
      "zh-Hant": "電影院經理"
    }
  }, {
    "key": "030003",
    "values": {
      "en": "Circus Owner (Clerical & Managerial Worker)",
      "zh-Hant": "馬戲團東主 (文職及管理人員)"
    }
  }, {
    "key": "030004",
    "values": {
      "en": "Circus Worker (Clerical & Managerial Worker)",
      "zh-Hant": "馬戲團工人(文職及管理人員)"
    }
  }, {
    "key": "037005",
    "values": {
      "en": "Civil Engineer (Office Work)",
      "zh-Hant": "土木工程師 (辦公室工作)"
    }
  }, {
    "key": "060007",
    "values": {
      "en": "Claims Executive - Financial Institution (Banking, Investment and Insurance, etc) ",
      "zh-Hant": "理賠主任 - 金融業 (銀行、投資及保險等)"
    }
  }, {
    "key": "032003",
    "values": {
      "en": "Cleaner - Cleaning & Repairing (Indoor)",
      "zh-Hant": "清潔員  - 清潔及維修(戶內)"
    }
  }, {
    "key": "081007",
    "values": {
      "en": "Cleaner - Hospital & Clinic",
      "zh-Hant": "清潔員 - 醫院及診所"
    }
  }, {
    "key": "092003",
    "values": {
      "en": "Cleaner - Mahjong School",
      "zh-Hant": "清潔工人 - 麻雀館"
    }
  }, {
    "key": "012010",
    "values": {
      "en": "Cleaner - Outside Plane",
      "zh-Hant": "清潔工人 -  飛機外殼"
    }
  }, {
    "key": "071001",
    "values": {
      "en": "Cleaner: gas boiler",
      "zh-Hant": "燃氣鍋爐清潔工"
    }
  }, {
    "key": "060008",
    "values": {
      "en": "Clerical staff  - Financial Institution (Banking, Investment and Insurance, etc) ",
      "zh-Hant": "文職人員 - 金融業 (銀行、投資及保險等)"
    }
  }, {
    "key": "031001",
    "values": {
      "en": "Clerical Staff - Civil Servant (Office Work)",
      "zh-Hant": "文職人員 - 公務員 (辦公室工作)"
    }
  }, {
    "key": "006001",
    "values": {
      "en": "Clerk  - Artificial Leather / Flower Manufacturing Industry ",
      "zh-Hant": "文員 - 人造皮革/花卉製造業 "
    }
  }, {
    "key": "119001",
    "values": {
      "en": "Clerk  - Recruitment ",
      "zh-Hant": "文員 - 人才招聘"
    }
  }, {
    "key": "012011",
    "values": {
      "en": "Clerk - Aviation (Indoor Duties)",
      "zh-Hant": "文員 - 航空業（室內職務）"
    }
  }, {
    "key": "072005",
    "values": {
      "en": "Clerk - Office work only",
      "zh-Hant": "文員 - 寫字樓工作"
    }
  }, {
    "key": "106002",
    "values": {
      "en": "Clerk - Paper & Pulp Industry ",
      "zh-Hant": "文員 - 紙及紙漿製造業"
    }
  }, {
    "key": "111001",
    "values": {
      "en": "Clerk - Post Office ",
      "zh-Hant": "文員 - 郵政局"
    }
  }, {
    "key": "125001",
    "values": {
      "en": "Clerk - Shipping Industry (office work) ",
      "zh-Hant": "文員 - 船務業（辦公室工作）"
    }
  }, {
    "key": "136002",
    "values": {
      "en": "Clerk - Trading",
      "zh-Hant": "文員 - 貿易行業"
    }
  }, {
    "key": "117001",
    "values": {
      "en": "Clerk/Officer  - Radio & Television Industry ",
      "zh-Hant": "文員/主任 - 電台及電視行業"
    }
  }, {
    "key": "030005",
    "values": {
      "en": "Clown - Circus (Clerical & Managerial Worker)",
      "zh-Hant": "小丑 - 馬戲團 (文職及管理人員)"
    }
  }, {
    "key": "128007",
    "values": {
      "en": "Coach  - Sports ",
      "zh-Hant": "教練 - 運動"
    }
  }, {
    "key": "049005",
    "values": {
      "en": "Coach Driver",
      "zh-Hant": "旅遊車司機"
    }
  }, {
    "key": "050002",
    "values": {
      "en": "Columnist",
      "zh-Hant": "專欄作家"
    }
  }, {
    "key": "110001",
    "values": {
      "en": "Commercial Crime Officer (not in uniform)",
      "zh-Hant": "商業罪案調查人員(不用穿着制服)"
    }
  }, {
    "key": "127001",
    "values": {
      "en": "Community Worker - Social Welfare (Outdoor, Involved in Youth Functions, Community Development)",
      "zh-Hant": "社區工作者 - 社會福利 (戶外，參與青少年活動，社區發展)"
    }
  }, {
    "key": "100001",
    "values": {
      "en": "Composer - Musician",
      "zh-Hant": "作曲家 - 音樂家"
    }
  }, {
    "key": "082001",
    "values": {
      "en": "Concierge - Hotel Industry ",
      "zh-Hant": "服務臺職員 - 酒店業"
    }
  }, {
    "key": "036001",
    "values": {
      "en": "Conductor - Concert & Symphony Orchestra ",
      "zh-Hant": "指揮 - 音樂會及交響樂團"
    }
  }, {
    "key": "037006",
    "values": {
      "en": "Construction Engineer (Office Work)",
      "zh-Hant": "建築工程師 (辦公室工作)"
    }
  }, {
    "key": "035001",
    "values": {
      "en": "Consultant - Computer Industry ",
      "zh-Hant": "電腦顧問"
    }
  }, {
    "key": "072006",
    "values": {
      "en": "Consultant - Office work only",
      "zh-Hant": "顧問 - 寫字樓工作"
    }
  }, {
    "key": "049011",
    "values": {
      "en": "Container Truck Driver",
      "zh-Hant": "貨櫃車司機"
    }
  }, {
    "key": "037007",
    "values": {
      "en": "Contractor - Construction Industry (No Manual or Height Work)",
      "zh-Hant": "承包商 - 建造業 (無手動及高空工作)"
    }
  }, {
    "key": "037008",
    "values": {
      "en": "Contractor / sub-contractor - Interior decoration (no manual work involved)",
      "zh-Hant": "室內裝修判頭: 不參與手作工序"
    }
  }, {
    "key": "081008",
    "values": {
      "en": "Cook - Hospital & Clinic",
      "zh-Hant": "廚師 - 醫院及診所"
    }
  }, {
    "key": "082002",
    "values": {
      "en": "Cook - Hotel Industry ",
      "zh-Hant": "廚師 - 酒店業"
    }
  }, {
    "key": "064002",
    "values": {
      "en": "Cook - Restaurant, Fast food shop, Canteen, etc",
      "zh-Hant": "廚師  - 餐廳，快餐店，食堂等"
    }
  }, {
    "key": "002002",
    "values": {
      "en": "Copywriter",
      "zh-Hant": "撰稿員 - 廣告業"
    }
  }, {
    "key": "038003",
    "values": {
      "en": "Copywriter",
      "zh-Hant": "撰稿員"
    }
  }, {
    "key": "127002",
    "values": {
      "en": "Counsellor  - Social Welfare (Office Administration) ",
      "zh-Hant": "顧問 - 社會福利 (辦公室行政)"
    }
  }, {
    "key": "043001",
    "values": {
      "en": "Courier: by motocycle - Delivery, Express & Moving Companies (Document)",
      "zh-Hant": "電單車速遞員 - 運輸，速遞及搬運公司 (文件)"
    }
  }, {
    "key": "043002",
    "values": {
      "en": "Courier: by motocycle - Delivery, Express & Moving Companies (Goods, Non-Document)",
      "zh-Hant": "電單車速遞員 - 運輸，速遞及搬運公司 (貨物、非文件)"
    }
  }, {
    "key": "037009",
    "values": {
      "en": "Crane Driver  - Construction Industry",
      "zh-Hant": "吊機司機 - 建造業"
    }
  }, {
    "key": "037010",
    "values": {
      "en": "Crane Driver (In Control Room)  - Construction Industry ",
      "zh-Hant": "吊車司機（控制室內） - 建造業"
    }
  }, {
    "key": "087002",
    "values": {
      "en": "Curator - Museum",
      "zh-Hant": "博物館館長"
    }
  }, {
    "key": "060009",
    "values": {
      "en": "Currency Exchanger  - Financial Institution (Banking, Investment and Insurance, etc) ",
      "zh-Hant": "貨幣交易員 - 金融業 (銀行、投資及保險等)"
    }
  }, {
    "key": "133001",
    "values": {
      "en": "Customer Service - Telephone & Telegram ",
      "zh-Hant": "顧客服務主任 - 電話及電訊業"
    }
  }, {
    "key": "128008",
    "values": {
      "en": "Cyclist - Sports ",
      "zh-Hant": "單車手 - 運動"
    }
  }, {
    "key": "042002",
    "values": {
      "en": "Dancer",
      "zh-Hant": "舞蹈員"
    }
  }, {
    "key": "109003",
    "values": {
      "en": "Darkroom worker - Photographing ",
      "zh-Hant": "黑房工人 - 攝影"
    }
  }, {
    "key": "035002",
    "values": {
      "en": "Data Entry Worker  - Computer Industry ",
      "zh-Hant": "數據輸入員 - 電腦業"
    }
  }, {
    "key": "035003",
    "values": {
      "en": "Database Administrator - Computer Industry ",
      "zh-Hant": "數據庫管理員 - 電腦業"
    }
  }, {
    "key": "060010",
    "values": {
      "en": "Debt Collector - in bank",
      "zh-Hant": "收數人 - 銀行"
    }
  }, {
    "key": "060011",
    "values": {
      "en": "Debt Collector - other than bank",
      "zh-Hant": "收數人 - 銀行除外"
    }
  }, {
    "key": "043003",
    "values": {
      "en": "Deliveryman - Delivery, Express & Moving Companies (Manual Work) ",
      "zh-Hant": "送貨員 - 運輸，速遞及搬運公司(勞動工作)"
    }
  }, {
    "key": "037011",
    "values": {
      "en": "Demolition Worker (No explosives)",
      "zh-Hant": "拆卸工人：沒有爆炸物"
    }
  }, {
    "key": "044001",
    "values": {
      "en": "Dental Hygienist",
      "zh-Hant": "牙齒衛生員"
    }
  }, {
    "key": "044002",
    "values": {
      "en": "Dental Nurse",
      "zh-Hant": "牙科護士"
    }
  }, {
    "key": "044003",
    "values": {
      "en": "Dentist",
      "zh-Hant": "牙醫"
    }
  }, {
    "key": "085002",
    "values": {
      "en": "Designer  - Jewellery",
      "zh-Hant": "珠寶設計師"
    }
  }, {
    "key": "035004",
    "values": {
      "en": "Designer - Computer Graphics ",
      "zh-Hant": "電腦繪圖設計師"
    }
  }, {
    "key": "046001",
    "values": {
      "en": "Designer (Indoor Duties)",
      "zh-Hant": "設計師（室內工作）"
    }
  }, {
    "key": "067001",
    "values": {
      "en": "Digger: Grave ",
      "zh-Hant": "墓穴挖掘工"
    }
  }, {
    "key": "064003",
    "values": {
      "en": "Dim Sum Seller - Restaurant, Fast food shop, Canteen, etc",
      "zh-Hant": "點心售賣員 - 餐廳，快餐店，食堂等"
    }
  }, {
    "key": "117002",
    "values": {
      "en": "Director  - Radio & Television Industry ",
      "zh-Hant": "節目總監 - 電台及電視行業"
    }
  }, {
    "key": "067002",
    "values": {
      "en": "Director - Funeral ",
      "zh-Hant": "殯儀董事"
    }
  }, {
    "key": "072007",
    "values": {
      "en": "Director / Manager - Office work only",
      "zh-Hant": "總監 / 經理 - 寫字樓工作"
    }
  }, {
    "key": "059003",
    "values": {
      "en": "Director: Program - Film Production Industry ",
      "zh-Hant": "節目總監 - 電影製造業"
    }
  }, {
    "key": "109001",
    "values": {
      "en": "Director: Studio - Photographing",
      "zh-Hant": "影樓董事 - 攝影"
    }
  }, {
    "key": "055004",
    "values": {
      "en": "Disc Jockey  - Lounge / Karaoke/ Bar / Disco (high class hotel)",
      "zh-Hant": "唱片騎師 - 酒廊/卡拉OK/酒吧/的士高（高級酒店）"
    }
  }, {
    "key": "117003",
    "values": {
      "en": "Disc Jockey  - Radio & Television Industry ",
      "zh-Hant": "唱片騎師 - 電台及電視行業"
    }
  }, {
    "key": "064004",
    "values": {
      "en": "Dishwasher - Restaurant, Fast food shop, Canteen, etc",
      "zh-Hant": "洗碗工人  - 餐廳，快餐店，食堂等"
    }
  }, {
    "key": "110002",
    "values": {
      "en": "Diver - Police",
      "zh-Hant": "潛水員 - 警察"
    }
  }, {
    "key": "081009",
    "values": {
      "en": "Doctor",
      "zh-Hant": "醫生"
    }
  }, {
    "key": "043004",
    "values": {
      "en": "Document Courier - Delivery, Express & Moving Companies",
      "zh-Hant": "文件速遞員 - 運輸，速遞及搬運公司"
    }
  }, {
    "key": "048001",
    "values": {
      "en": "Domestic Helper: Full time",
      "zh-Hant": "家務助理: 全職"
    }
  }, {
    "key": "048002",
    "values": {
      "en": "Domestic Helper: Part time",
      "zh-Hant": "家務助理: 兼職"
    }
  }, {
    "key": "037012",
    "values": {
      "en": "Draftsman - Construction Industry (office work)",
      "zh-Hant": "繒圖員 - 建造業 (辦公室工作)"
    }
  }, {
    "key": "037013",
    "values": {
      "en": "Drill Operator - Construction Industry ",
      "zh-Hant": "鑽機操作員 - 建造業"
    }
  }, {
    "key": "116002",
    "values": {
      "en": "Driver - Public Transport (e.g. MTR, LTR, KCR, Tram)",
      "zh-Hant": "司機 - 公共交通 (如地鐵，輕鐵，九鐵，電車)"
    }
  }, {
    "key": "049006",
    "values": {
      "en": "Driver: Heavy goods vehicle",
      "zh-Hant": "重型貨車司機"
    }
  }, {
    "key": "049007",
    "values": {
      "en": "Driving Instructor",
      "zh-Hant": "駕駛導師"
    }
  }, {
    "key": "059004",
    "values": {
      "en": "Dubber - Film Production Industry ",
      "zh-Hant": "配音員 - 電影製造業"
    }
  }, {
    "key": "050003",
    "values": {
      "en": "Editor",
      "zh-Hant": "編輯"
    }
  }, {
    "key": "101002",
    "values": {
      "en": "Editor  - Newspaper Industry",
      "zh-Hant": "編輯 - 報業"
    }
  }, {
    "key": "050004",
    "values": {
      "en": "Editorial Assistant",
      "zh-Hant": "編輯助理"
    }
  }, {
    "key": "052001",
    "values": {
      "en": "Electrical Engineer (maintenance & repair)",
      "zh-Hant": "電機工程師 (維修及保養)"
    }
  }, {
    "key": "052002",
    "values": {
      "en": "Electrical Engineer (Supervising, Indoor)",
      "zh-Hant": "電機工程師 (監督, 戶內)"
    }
  }],
  "telCodeMapping": [{
    "key": "+852",
    "values": {
      "en": "852",
      "zh-Hant": "852"
    }
  }, {
    "key": "+86",
    "values": {
      "en": "856",
      "zh-Hant": "856"
    }
  }],
  "nameOrderMapping": [{
    "key": "F",
    "values": {
      "en": "Given name first",
      "zh-Hant": "Given name first"
    }
  }, {
    "key": "L",
    "values": {
      "en": "Surname first",
      "zh-Hant": "Surname first"
    }
  }],
  "addrProvinceMapping": [{
    "key": "province",
    "values": {
      "en": "Province",
      "zh-Hant": "省"
    }
  }],
  "maritalMapping": [{
    "key": "S",
    "values": {
      "en": "Single",
      "zh-Hant": "Single"
    }
  }, {
    "key": "M",
    "values": {
      "en": "Married",
      "zh-Hant": "Married"
    }
  }, {
    "key": "D",
    "values": {
      "en": "Divorced",
      "zh-Hant": "Divorced"
    }
  }, {
    "key": "W",
    "values": {
      "en": "Widowed",
      "zh-Hant": "Widowed"
    }
  }],
  "cityMapping": [{
    "key": "C1",
    "values": {
      "en": "北京市",
      "zh-Hant": "北京市"
    }
  }, {
    "key": "C2",
    "values": {
      "en": "重慶市",
      "zh-Hant": "重慶市"
    }
  }, {
    "key": "C3",
    "values": {
      "en": "上海市",
      "zh-Hant": "上海市"
    }
  }, {
    "key": "C4",
    "values": {
      "en": "天津市",
      "zh-Hant": "天津市"
    }
  }, {
    "key": "C5",
    "values": {
      "en": "東莞市",
      "zh-Hant": "東莞市"
    }
  }, {
    "key": "C6",
    "values": {
      "en": "廣州市",
      "zh-Hant": "廣州市"
    }
  }, {
    "key": "C7",
    "values": {
      "en": "中山市",
      "zh-Hant": "中山市"
    }
  }, {
    "key": "C8",
    "values": {
      "en": "深圳市",
      "zh-Hant": "深圳市"
    }
  }, {
    "key": "C9",
    "values": {
      "en": "惠州市",
      "zh-Hant": "惠州市"
    }
  }, {
    "key": "C10",
    "values": {
      "en": "江門市",
      "zh-Hant": "江門市"
    }
  }, {
    "key": "C11",
    "values": {
      "en": "珠海市",
      "zh-Hant": "珠海市"
    }
  }, {
    "key": "C12",
    "values": {
      "en": "汕頭市",
      "zh-Hant": "汕頭市"
    }
  }, {
    "key": "C13",
    "values": {
      "en": "佛山市",
      "zh-Hant": "佛山市"
    }
  }, {
    "key": "C14",
    "values": {
      "en": "湛江市",
      "zh-Hant": "湛江市"
    }
  }, {
    "key": "C15",
    "values": {
      "en": "河源市",
      "zh-Hant": "河源市"
    }
  }, {
    "key": "C16",
    "values": {
      "en": "肇慶市",
      "zh-Hant": "肇慶市"
    }
  }, {
    "key": "C17",
    "values": {
      "en": "潮州市",
      "zh-Hant": "潮州市"
    }
  }, {
    "key": "C18",
    "values": {
      "en": "清遠市",
      "zh-Hant": "清遠市"
    }
  }, {
    "key": "C19",
    "values": {
      "en": "韶關市",
      "zh-Hant": "韶關市"
    }
  }, {
    "key": "C20",
    "values": {
      "en": "揭陽市",
      "zh-Hant": "揭陽市"
    }
  }, {
    "key": "C21",
    "values": {
      "en": "陽江市",
      "zh-Hant": "陽江市"
    }
  }, {
    "key": "C22",
    "values": {
      "en": "雲浮市",
      "zh-Hant": "雲浮市"
    }
  }, {
    "key": "C23",
    "values": {
      "en": "茂名市",
      "zh-Hant": "茂名市"
    }
  }, {
    "key": "C24",
    "values": {
      "en": "梅州市",
      "zh-Hant": "梅州市"
    }
  }, {
    "key": "C25",
    "values": {
      "en": "汕尾市",
      "zh-Hant": "汕尾市"
    }
  }, {
    "key": "C26",
    "values": {
      "en": "漳州市",
      "zh-Hant": "漳州市"
    }
  }, {
    "key": "C27",
    "values": {
      "en": "泉州市",
      "zh-Hant": "泉州市"
    }
  }, {
    "key": "C28",
    "values": {
      "en": "厦門市",
      "zh-Hant": "厦門市"
    }
  }, {
    "key": "C29",
    "values": {
      "en": "福州市",
      "zh-Hant": "福州市"
    }
  }, {
    "key": "C30",
    "values": {
      "en": "莆田市",
      "zh-Hant": "莆田市"
    }
  }, {
    "key": "C31",
    "values": {
      "en": "寧德市",
      "zh-Hant": "寧德市"
    }
  }, {
    "key": "C32",
    "values": {
      "en": "三明市",
      "zh-Hant": "三明市"
    }
  }, {
    "key": "C33",
    "values": {
      "en": "南平市",
      "zh-Hant": "南平市"
    }
  }, {
    "key": "C34",
    "values": {
      "en": "龍岩市",
      "zh-Hant": "龍岩市"
    }
  }, {
    "key": "C35",
    "values": {
      "en": "蘇州市",
      "zh-Hant": "蘇州市"
    }
  }, {
    "key": "C36",
    "values": {
      "en": "徐州市",
      "zh-Hant": "徐州市"
    }
  }, {
    "key": "C37",
    "values": {
      "en": "鹽城市",
      "zh-Hant": "鹽城市"
    }
  }, {
    "key": "C38",
    "values": {
      "en": "無錫市",
      "zh-Hant": "無錫市"
    }
  }, {
    "key": "C39",
    "values": {
      "en": "南京市",
      "zh-Hant": "南京市"
    }
  }, {
    "key": "C40",
    "values": {
      "en": "南通市",
      "zh-Hant": "南通市"
    }
  }, {
    "key": "C41",
    "values": {
      "en": "連雲港市",
      "zh-Hant": "連雲港市"
    }
  }, {
    "key": "C42",
    "values": {
      "en": "常州市",
      "zh-Hant": "常州市"
    }
  }, {
    "key": "C43",
    "values": {
      "en": "揚州市",
      "zh-Hant": "揚州市"
    }
  }, {
    "key": "C44",
    "values": {
      "en": "鎮江市",
      "zh-Hant": "鎮江市"
    }
  }, {
    "key": "C45",
    "values": {
      "en": "淮安市",
      "zh-Hant": "淮安市"
    }
  }, {
    "key": "C46",
    "values": {
      "en": "泰州市",
      "zh-Hant": "泰州市"
    }
  }, {
    "key": "C47",
    "values": {
      "en": "宿遷市",
      "zh-Hant": "宿遷市"
    }
  }, {
    "key": "C48",
    "values": {
      "en": "温州市",
      "zh-Hant": "温州市"
    }
  }, {
    "key": "C49",
    "values": {
      "en": "寧波市",
      "zh-Hant": "寧波市"
    }
  }, {
    "key": "C50",
    "values": {
      "en": "杭州市",
      "zh-Hant": "杭州市"
    }
  }, {
    "key": "C51",
    "values": {
      "en": "台州市",
      "zh-Hant": "台州市"
    }
  }, {
    "key": "C52",
    "values": {
      "en": "嘉興市",
      "zh-Hant": "嘉興市"
    }
  }, {
    "key": "C53",
    "values": {
      "en": "金華市",
      "zh-Hant": "金華市"
    }
  }, {
    "key": "C54",
    "values": {
      "en": "湖州市",
      "zh-Hant": "湖州市"
    }
  }, {
    "key": "C55",
    "values": {
      "en": "紹興市",
      "zh-Hant": "紹興市"
    }
  }, {
    "key": "C56",
    "values": {
      "en": "舟山市",
      "zh-Hant": "舟山市"
    }
  }, {
    "key": "C57",
    "values": {
      "en": "麗水市",
      "zh-Hant": "麗水市"
    }
  }, {
    "key": "C58",
    "values": {
      "en": "衢州市",
      "zh-Hant": "衢州市"
    }
  }, {
    "key": "C59",
    "values": {
      "en": "三亞市",
      "zh-Hant": "三亞市"
    }
  }, {
    "key": "C60",
    "values": {
      "en": "海口市",
      "zh-Hant": "海口市"
    }
  }, {
    "key": "C61",
    "values": {
      "en": "琼海市",
      "zh-Hant": "琼海市"
    }
  }, {
    "key": "C62",
    "values": {
      "en": "文昌市",
      "zh-Hant": "文昌市"
    }
  }, {
    "key": "C63",
    "values": {
      "en": "東方市",
      "zh-Hant": "東方市"
    }
  }, {
    "key": "C64",
    "values": {
      "en": "昌江縣",
      "zh-Hant": "昌江縣"
    }
  }, {
    "key": "C65",
    "values": {
      "en": "陵水縣",
      "zh-Hant": "陵水縣"
    }
  }, {
    "key": "C66",
    "values": {
      "en": "樂東縣",
      "zh-Hant": "樂東縣"
    }
  }, {
    "key": "C67",
    "values": {
      "en": "五指山市",
      "zh-Hant": "五指山市"
    }
  }, {
    "key": "C68",
    "values": {
      "en": "保亭縣",
      "zh-Hant": "保亭縣"
    }
  }, {
    "key": "C69",
    "values": {
      "en": "澄邁縣",
      "zh-Hant": "澄邁縣"
    }
  }, {
    "key": "C70",
    "values": {
      "en": "萬寧市",
      "zh-Hant": "萬寧市"
    }
  }, {
    "key": "C71",
    "values": {
      "en": "儋州市",
      "zh-Hant": "儋州市"
    }
  }, {
    "key": "C72",
    "values": {
      "en": "臨高縣",
      "zh-Hant": "臨高縣"
    }
  }, {
    "key": "C73",
    "values": {
      "en": "白沙縣",
      "zh-Hant": "白沙縣"
    }
  }, {
    "key": "C74",
    "values": {
      "en": "定安縣",
      "zh-Hant": "定安縣"
    }
  }, {
    "key": "C75",
    "values": {
      "en": "琼中縣",
      "zh-Hant": "琼中縣"
    }
  }, {
    "key": "C76",
    "values": {
      "en": "屯昌縣",
      "zh-Hant": "屯昌縣"
    }
  }, {
    "key": "C77",
    "values": {
      "en": "濟南市",
      "zh-Hant": "濟南市"
    }
  }, {
    "key": "C78",
    "values": {
      "en": "青島市",
      "zh-Hant": "青島市"
    }
  }, {
    "key": "C79",
    "values": {
      "en": "臨沂市",
      "zh-Hant": "臨沂市"
    }
  }, {
    "key": "C80",
    "values": {
      "en": "濟寧市",
      "zh-Hant": "濟寧市"
    }
  }, {
    "key": "C81",
    "values": {
      "en": "菏澤市",
      "zh-Hant": "菏澤市"
    }
  }, {
    "key": "C82",
    "values": {
      "en": "烟台市",
      "zh-Hant": "烟台市"
    }
  }, {
    "key": "C83",
    "values": {
      "en": "泰安市",
      "zh-Hant": "泰安市"
    }
  }, {
    "key": "C84",
    "values": {
      "en": "淄博市",
      "zh-Hant": "淄博市"
    }
  }, {
    "key": "C85",
    "values": {
      "en": "潍坊市",
      "zh-Hant": "潍坊市"
    }
  }, {
    "key": "C86",
    "values": {
      "en": "日照市",
      "zh-Hant": "日照市"
    }
  }, {
    "key": "C87",
    "values": {
      "en": "威海市",
      "zh-Hant": "威海市"
    }
  }, {
    "key": "C88",
    "values": {
      "en": "濱州市",
      "zh-Hant": "濱州市"
    }
  }, {
    "key": "C89",
    "values": {
      "en": "東营市",
      "zh-Hant": "東营市"
    }
  }, {
    "key": "C90",
    "values": {
      "en": "聊城市",
      "zh-Hant": "聊城市"
    }
  }, {
    "key": "C91",
    "values": {
      "en": "德州市",
      "zh-Hant": "德州市"
    }
  }, {
    "key": "C92",
    "values": {
      "en": "萊蕪市",
      "zh-Hant": "萊蕪市"
    }
  }, {
    "key": "C93",
    "values": {
      "en": "棗莊市",
      "zh-Hant": "棗莊市"
    }
  }, {
    "key": "C94",
    "values": {
      "en": "貴港市",
      "zh-Hant": "貴港市"
    }
  }, {
    "key": "C95",
    "values": {
      "en": "玉林市",
      "zh-Hant": "玉林市"
    }
  }, {
    "key": "C96",
    "values": {
      "en": "北海市",
      "zh-Hant": "北海市"
    }
  }, {
    "key": "C97",
    "values": {
      "en": "柳州市",
      "zh-Hant": "柳州市"
    }
  }, {
    "key": "C98",
    "values": {
      "en": "桂林市",
      "zh-Hant": "桂林市"
    }
  }, {
    "key": "C99",
    "values": {
      "en": "梧州市",
      "zh-Hant": "梧州市"
    }
  }, {
    "key": "C100",
    "values": {
      "en": "欽州市",
      "zh-Hant": "欽州市"
    }
  }, {
    "key": "C101",
    "values": {
      "en": "來賓市",
      "zh-Hant": "來賓市"
    }
  }, {
    "key": "C102",
    "values": {
      "en": "河池市",
      "zh-Hant": "河池市"
    }
  }, {
    "key": "C103",
    "values": {
      "en": "百色市",
      "zh-Hant": "百色市"
    }
  }, {
    "key": "C104",
    "values": {
      "en": "賀州市",
      "zh-Hant": "賀州市"
    }
  }, {
    "key": "C105",
    "values": {
      "en": "崇左市",
      "zh-Hant": "崇左市"
    }
  }, {
    "key": "C106",
    "values": {
      "en": "防城港市",
      "zh-Hant": "防城港市"
    }
  }, {
    "key": "C107",
    "values": {
      "en": "南寧市",
      "zh-Hant": "南寧市"
    }
  }, {
    "key": "C108",
    "values": {
      "en": "吉林市",
      "zh-Hant": "吉林市"
    }
  }, {
    "key": "C109",
    "values": {
      "en": "長春市",
      "zh-Hant": "長春市"
    }
  }, {
    "key": "C110",
    "values": {
      "en": "松原市",
      "zh-Hant": "松原市"
    }
  }, {
    "key": "C111",
    "values": {
      "en": "白山市",
      "zh-Hant": "白山市"
    }
  }, {
    "key": "C112",
    "values": {
      "en": "白城市",
      "zh-Hant": "白城市"
    }
  }, {
    "key": "C113",
    "values": {
      "en": "延邊州",
      "zh-Hant": "延邊州"
    }
  }, {
    "key": "C114",
    "values": {
      "en": "遼源市",
      "zh-Hant": "遼源市"
    }
  }, {
    "key": "C115",
    "values": {
      "en": "通化市",
      "zh-Hant": "通化市"
    }
  }, {
    "key": "C116",
    "values": {
      "en": "四平市",
      "zh-Hant": "四平市"
    }
  }, {
    "key": "C117",
    "values": {
      "en": "大連市",
      "zh-Hant": "大連市"
    }
  }, {
    "key": "C118",
    "values": {
      "en": "沈陽市",
      "zh-Hant": "沈陽市"
    }
  }, {
    "key": "C119",
    "values": {
      "en": "鞍山市",
      "zh-Hant": "鞍山市"
    }
  }, {
    "key": "C120",
    "values": {
      "en": "錦州市",
      "zh-Hant": "錦州市"
    }
  }, {
    "key": "C121",
    "values": {
      "en": "營口市",
      "zh-Hant": "營口市"
    }
  }, {
    "key": "C122",
    "values": {
      "en": "丹東市",
      "zh-Hant": "丹東市"
    }
  }, {
    "key": "C123",
    "values": {
      "en": "遼陽市",
      "zh-Hant": "遼陽市"
    }
  }, {
    "key": "C124",
    "values": {
      "en": "葫蘆島市",
      "zh-Hant": "葫蘆島市"
    }
  }, {
    "key": "C125",
    "values": {
      "en": "朝陽市",
      "zh-Hant": "朝陽市"
    }
  }, {
    "key": "C126",
    "values": {
      "en": "撫順市",
      "zh-Hant": "撫順市"
    }
  }, {
    "key": "C127",
    "values": {
      "en": "阜新市",
      "zh-Hant": "阜新市"
    }
  }, {
    "key": "C128",
    "values": {
      "en": "本溪市",
      "zh-Hant": "本溪市"
    }
  }, {
    "key": "C129",
    "values": {
      "en": "盤錦市",
      "zh-Hant": "盤錦市"
    }
  }, {
    "key": "C130",
    "values": {
      "en": "鐵嶺市",
      "zh-Hant": "鐵嶺市"
    }
  }, {
    "key": "C131",
    "values": {
      "en": "武漢市",
      "zh-Hant": "武漢市"
    }
  }, {
    "key": "C132",
    "values": {
      "en": "宜昌市",
      "zh-Hant": "宜昌市"
    }
  }, {
    "key": "C133",
    "values": {
      "en": "襄樊市",
      "zh-Hant": "襄樊市"
    }
  }, {
    "key": "C134",
    "values": {
      "en": "荆州市",
      "zh-Hant": "荆州市"
    }
  }, {
    "key": "C135",
    "values": {
      "en": "恩施州",
      "zh-Hant": "恩施州"
    }
  }, {
    "key": "C136",
    "values": {
      "en": "孝感市",
      "zh-Hant": "孝感市"
    }
  }, {
    "key": "C137",
    "values": {
      "en": "黃岡市",
      "zh-Hant": "黃岡市"
    }
  }, {
    "key": "C138",
    "values": {
      "en": "十堰市",
      "zh-Hant": "十堰市"
    }
  }, {
    "key": "C139",
    "values": {
      "en": "咸寧市",
      "zh-Hant": "咸寧市"
    }
  }, {
    "key": "C140",
    "values": {
      "en": "黄石市",
      "zh-Hant": "黄石市"
    }
  }, {
    "key": "C141",
    "values": {
      "en": "仙桃市",
      "zh-Hant": "仙桃市"
    }
  }, {
    "key": "C142",
    "values": {
      "en": "隨州市",
      "zh-Hant": "隨州市"
    }
  }, {
    "key": "C143",
    "values": {
      "en": "天門市",
      "zh-Hant": "天門市"
    }
  }, {
    "key": "C144",
    "values": {
      "en": "荆門市",
      "zh-Hant": "荆門市"
    }
  }, {
    "key": "C145",
    "values": {
      "en": "潛江市",
      "zh-Hant": "潛江市"
    }
  }, {
    "key": "C146",
    "values": {
      "en": "鄂州市",
      "zh-Hant": "鄂州市"
    }
  }, {
    "key": "C147",
    "values": {
      "en": "神農架林區",
      "zh-Hant": "神農架林區"
    }
  }, {
    "key": "C148",
    "values": {
      "en": "成都市",
      "zh-Hant": "成都市"
    }
  }, {
    "key": "C149",
    "values": {
      "en": "綿陽市",
      "zh-Hant": "綿陽市"
    }
  }, {
    "key": "C150",
    "values": {
      "en": "廣元市",
      "zh-Hant": "廣元市"
    }
  }, {
    "key": "C151",
    "values": {
      "en": "達州市",
      "zh-Hant": "達州市"
    }
  }, {
    "key": "C152",
    "values": {
      "en": "南充市",
      "zh-Hant": "南充市"
    }
  }, {
    "key": "C153",
    "values": {
      "en": "德陽市",
      "zh-Hant": "德陽市"
    }
  }, {
    "key": "C154",
    "values": {
      "en": "廣安市",
      "zh-Hant": "廣安市"
    }
  }, {
    "key": "C155",
    "values": {
      "en": "阿壩州",
      "zh-Hant": "阿壩州"
    }
  }, {
    "key": "C156",
    "values": {
      "en": "巴中市",
      "zh-Hant": "巴中市"
    }
  }, {
    "key": "C157",
    "values": {
      "en": "遂寧市",
      "zh-Hant": "遂寧市"
    }
  }, {
    "key": "C158",
    "values": {
      "en": "内江市",
      "zh-Hant": "内江市"
    }
  }, {
    "key": "C159",
    "values": {
      "en": "涼山州",
      "zh-Hant": "涼山州"
    }
  }, {
    "key": "C160",
    "values": {
      "en": "攀枝花市",
      "zh-Hant": "攀枝花市"
    }
  }, {
    "key": "C161",
    "values": {
      "en": "樂山市",
      "zh-Hant": "樂山市"
    }
  }, {
    "key": "C162",
    "values": {
      "en": "自貢市",
      "zh-Hant": "自貢市"
    }
  }, {
    "key": "C163",
    "values": {
      "en": "瀘州市",
      "zh-Hant": "瀘州市"
    }
  }, {
    "key": "C164",
    "values": {
      "en": "雅安市",
      "zh-Hant": "雅安市"
    }
  }, {
    "key": "C165",
    "values": {
      "en": "宜賓市",
      "zh-Hant": "宜賓市"
    }
  }, {
    "key": "C166",
    "values": {
      "en": "資陽市",
      "zh-Hant": "資陽市"
    }
  }, {
    "key": "C167",
    "values": {
      "en": "眉山市",
      "zh-Hant": "眉山市"
    }
  }, {
    "key": "C168",
    "values": {
      "en": "甘孜州",
      "zh-Hant": "甘孜州"
    }
  }, {
    "key": "C169",
    "values": {
      "en": "太原市",
      "zh-Hant": "太原市"
    }
  }, {
    "key": "C170",
    "values": {
      "en": "臨汾市",
      "zh-Hant": "臨汾市"
    }
  }, {
    "key": "C171",
    "values": {
      "en": "運城市",
      "zh-Hant": "運城市"
    }
  }, {
    "key": "C172",
    "values": {
      "en": "大同市",
      "zh-Hant": "大同市"
    }
  }, {
    "key": "C173",
    "values": {
      "en": "長治市",
      "zh-Hant": "長治市"
    }
  }, {
    "key": "C174",
    "values": {
      "en": "晋城市",
      "zh-Hant": "晋城市"
    }
  }, {
    "key": "C175",
    "values": {
      "en": "忻州市",
      "zh-Hant": "忻州市"
    }
  }, {
    "key": "C176",
    "values": {
      "en": "呂梁市",
      "zh-Hant": "呂梁市"
    }
  }, {
    "key": "C177",
    "values": {
      "en": "晋中市",
      "zh-Hant": "晋中市"
    }
  }, {
    "key": "C178",
    "values": {
      "en": "陽泉市",
      "zh-Hant": "陽泉市"
    }
  }, {
    "key": "C179",
    "values": {
      "en": "朔州市",
      "zh-Hant": "朔州市"
    }
  }, {
    "key": "C180",
    "values": {
      "en": "秦皇島市",
      "zh-Hant": "秦皇島市"
    }
  }, {
    "key": "C181",
    "values": {
      "en": "唐山市",
      "zh-Hant": "唐山市"
    }
  }, {
    "key": "C182",
    "values": {
      "en": "石家莊市",
      "zh-Hant": "石家莊市"
    }
  }, {
    "key": "C183",
    "values": {
      "en": "邯鄲市",
      "zh-Hant": "邯鄲市"
    }
  }, {
    "key": "C184",
    "values": {
      "en": "滄州市",
      "zh-Hant": "滄州市"
    }
  }, {
    "key": "C185",
    "values": {
      "en": "保定市",
      "zh-Hant": "保定市"
    }
  }, {
    "key": "C186",
    "values": {
      "en": "邢台市",
      "zh-Hant": "邢台市"
    }
  }, {
    "key": "C187",
    "values": {
      "en": "廊坊市",
      "zh-Hant": "廊坊市"
    }
  }, {
    "key": "C188",
    "values": {
      "en": "張家口市",
      "zh-Hant": "張家口市"
    }
  }, {
    "key": "C189",
    "values": {
      "en": "河北區",
      "zh-Hant": "河北區"
    }
  }, {
    "key": "C190",
    "values": {
      "en": "衡水市",
      "zh-Hant": "衡水市"
    }
  }, {
    "key": "C191",
    "values": {
      "en": "承德市",
      "zh-Hant": "承德市"
    }
  }, {
    "key": "C192",
    "values": {
      "en": "昆明市",
      "zh-Hant": "昆明市"
    }
  }, {
    "key": "C193",
    "values": {
      "en": "曲靖市",
      "zh-Hant": "曲靖市"
    }
  }, {
    "key": "C194",
    "values": {
      "en": "紅河州",
      "zh-Hant": "紅河州"
    }
  }, {
    "key": "C195",
    "values": {
      "en": "大理州",
      "zh-Hant": "大理州"
    }
  }, {
    "key": "C196",
    "values": {
      "en": "文山州",
      "zh-Hant": "文山州"
    }
  }, {
    "key": "C197",
    "values": {
      "en": "德宏州",
      "zh-Hant": "德宏州"
    }
  }, {
    "key": "C198",
    "values": {
      "en": "昭通市",
      "zh-Hant": "昭通市"
    }
  }, {
    "key": "C199",
    "values": {
      "en": "楚雄州",
      "zh-Hant": "楚雄州"
    }
  }, {
    "key": "C200",
    "values": {
      "en": "保山市",
      "zh-Hant": "保山市"
    }
  }, {
    "key": "C201",
    "values": {
      "en": "玉溪市",
      "zh-Hant": "玉溪市"
    }
  }, {
    "key": "C202",
    "values": {
      "en": "麗江地區",
      "zh-Hant": "麗江地區"
    }
  }, {
    "key": "C203",
    "values": {
      "en": "臨滄地區",
      "zh-Hant": "臨滄地區"
    }
  }, {
    "key": "C204",
    "values": {
      "en": "思茅地區",
      "zh-Hant": "思茅地區"
    }
  }, {
    "key": "C205",
    "values": {
      "en": "西雙版納州",
      "zh-Hant": "西雙版納州"
    }
  }, {
    "key": "C206",
    "values": {
      "en": "怒江州",
      "zh-Hant": "怒江州"
    }
  }, {
    "key": "C207",
    "values": {
      "en": "迪慶州",
      "zh-Hant": "迪慶州"
    }
  }, {
    "key": "C208",
    "values": {
      "en": "西安市",
      "zh-Hant": "西安市"
    }
  }, {
    "key": "C209",
    "values": {
      "en": "寶鷄市",
      "zh-Hant": "寶鷄市"
    }
  }, {
    "key": "C210",
    "values": {
      "en": "咸陽市",
      "zh-Hant": "咸陽市"
    }
  }, {
    "key": "C211",
    "values": {
      "en": "延安市",
      "zh-Hant": "延安市"
    }
  }, {
    "key": "C212",
    "values": {
      "en": "榆林市",
      "zh-Hant": "榆林市"
    }
  }, {
    "key": "C213",
    "values": {
      "en": "漢中市",
      "zh-Hant": "漢中市"
    }
  }, {
    "key": "C214",
    "values": {
      "en": "渭南市",
      "zh-Hant": "渭南市"
    }
  }, {
    "key": "C215",
    "values": {
      "en": "安康市",
      "zh-Hant": "安康市"
    }
  }, {
    "key": "C216",
    "values": {
      "en": "商洛市",
      "zh-Hant": "商洛市"
    }
  }, {
    "key": "C217",
    "values": {
      "en": "銅川市",
      "zh-Hant": "銅川市"
    }
  }, {
    "key": "C218",
    "values": {
      "en": "南昌市",
      "zh-Hant": "南昌市"
    }
  }, {
    "key": "C219",
    "values": {
      "en": "赣州市",
      "zh-Hant": "赣州市"
    }
  }, {
    "key": "C220",
    "values": {
      "en": "九江市",
      "zh-Hant": "九江市"
    }
  }, {
    "key": "C221",
    "values": {
      "en": "上饒市",
      "zh-Hant": "上饒市"
    }
  }, {
    "key": "C222",
    "values": {
      "en": "吉安市",
      "zh-Hant": "吉安市"
    }
  }, {
    "key": "C223",
    "values": {
      "en": "新余市",
      "zh-Hant": "新余市"
    }
  }, {
    "key": "C224",
    "values": {
      "en": "撫州市",
      "zh-Hant": "撫州市"
    }
  }, {
    "key": "C225",
    "values": {
      "en": "宜春市",
      "zh-Hant": "宜春市"
    }
  }, {
    "key": "C226",
    "values": {
      "en": "景德鎮市",
      "zh-Hant": "景德鎮市"
    }
  }, {
    "key": "C227",
    "values": {
      "en": "萍鄉市",
      "zh-Hant": "萍鄉市"
    }
  }, {
    "key": "C228",
    "values": {
      "en": "鹰潭市",
      "zh-Hant": "鹰潭市"
    }
  }, {
    "key": "C229",
    "values": {
      "en": "長沙市",
      "zh-Hant": "長沙市"
    }
  }, {
    "key": "C230",
    "values": {
      "en": "岳陽市",
      "zh-Hant": "岳陽市"
    }
  }, {
    "key": "C231",
    "values": {
      "en": "株洲市",
      "zh-Hant": "株洲市"
    }
  }, {
    "key": "C232",
    "values": {
      "en": "衡陽市",
      "zh-Hant": "衡陽市"
    }
  }, {
    "key": "C233",
    "values": {
      "en": "常德市",
      "zh-Hant": "常德市"
    }
  }, {
    "key": "C234",
    "values": {
      "en": "郴州市",
      "zh-Hant": "郴州市"
    }
  }, {
    "key": "C235",
    "values": {
      "en": "邵陽市",
      "zh-Hant": "邵陽市"
    }
  }, {
    "key": "C236",
    "values": {
      "en": "湘潭市",
      "zh-Hant": "湘潭市"
    }
  }, {
    "key": "C237",
    "values": {
      "en": "永州市",
      "zh-Hant": "永州市"
    }
  }, {
    "key": "C238",
    "values": {
      "en": "懷化市",
      "zh-Hant": "懷化市"
    }
  }, {
    "key": "C239",
    "values": {
      "en": "婁底市",
      "zh-Hant": "婁底市"
    }
  }, {
    "key": "C240",
    "values": {
      "en": "益陽市",
      "zh-Hant": "益陽市"
    }
  }, {
    "key": "C241",
    "values": {
      "en": "張家界市",
      "zh-Hant": "張家界市"
    }
  }, {
    "key": "C242",
    "values": {
      "en": "湘西州",
      "zh-Hant": "湘西州"
    }
  }, {
    "key": "C243",
    "values": {
      "en": "鄭州市",
      "zh-Hant": "鄭州市"
    }
  }, {
    "key": "C244",
    "values": {
      "en": "洛陽市",
      "zh-Hant": "洛陽市"
    }
  }, {
    "key": "C245",
    "values": {
      "en": "開封市",
      "zh-Hant": "開封市"
    }
  }, {
    "key": "C246",
    "values": {
      "en": "平頂山",
      "zh-Hant": "平頂山"
    }
  }, {
    "key": "C247",
    "values": {
      "en": "安陽市",
      "zh-Hant": "安陽市"
    }
  }, {
    "key": "C248",
    "values": {
      "en": "新鄉市",
      "zh-Hant": "新鄉市"
    }
  }, {
    "key": "C249",
    "values": {
      "en": "焦作市",
      "zh-Hant": "焦作市"
    }
  }, {
    "key": "C250",
    "values": {
      "en": "許昌市",
      "zh-Hant": "許昌市"
    }
  }, {
    "key": "C251",
    "values": {
      "en": "商丘市",
      "zh-Hant": "商丘市"
    }
  }, {
    "key": "C252",
    "values": {
      "en": "周口市",
      "zh-Hant": "周口市"
    }
  }, {
    "key": "C253",
    "values": {
      "en": "駐馬店",
      "zh-Hant": "駐馬店"
    }
  }, {
    "key": "C254",
    "values": {
      "en": "南陽市",
      "zh-Hant": "南陽市"
    }
  }, {
    "key": "C255",
    "values": {
      "en": "信陽市",
      "zh-Hant": "信陽市"
    }
  }, {
    "key": "C256",
    "values": {
      "en": "平頂山市",
      "zh-Hant": "平頂山市"
    }
  }, {
    "key": "C257",
    "values": {
      "en": "駐馬店市",
      "zh-Hant": "駐馬店市"
    }
  }, {
    "key": "C258",
    "values": {
      "en": "濮陽市",
      "zh-Hant": "濮陽市"
    }
  }, {
    "key": "C259",
    "values": {
      "en": "三門峡市",
      "zh-Hant": "三門峡市"
    }
  }, {
    "key": "C260",
    "values": {
      "en": "漯河市",
      "zh-Hant": "漯河市"
    }
  }, {
    "key": "C261",
    "values": {
      "en": "鶴壁市",
      "zh-Hant": "鶴壁市"
    }
  }, {
    "key": "C262",
    "values": {
      "en": "濟源市",
      "zh-Hant": "濟源市"
    }
  }, {
    "key": "C263",
    "values": {
      "en": "哈爾濱市",
      "zh-Hant": "哈爾濱市"
    }
  }, {
    "key": "C264",
    "values": {
      "en": "大慶市",
      "zh-Hant": "大慶市"
    }
  }, {
    "key": "C265",
    "values": {
      "en": "齊齊哈爾市",
      "zh-Hant": "齊齊哈爾市"
    }
  }, {
    "key": "C266",
    "values": {
      "en": "佳木斯市",
      "zh-Hant": "佳木斯市"
    }
  }, {
    "key": "C267",
    "values": {
      "en": "雙鴨山市",
      "zh-Hant": "雙鴨山市"
    }
  }, {
    "key": "C268",
    "values": {
      "en": "牡丹江市",
      "zh-Hant": "牡丹江市"
    }
  }, {
    "key": "C269",
    "values": {
      "en": "雞西市",
      "zh-Hant": "雞西市"
    }
  }, {
    "key": "C270",
    "values": {
      "en": "黑河市",
      "zh-Hant": "黑河市"
    }
  }, {
    "key": "C271",
    "values": {
      "en": "綏化市",
      "zh-Hant": "綏化市"
    }
  }, {
    "key": "C272",
    "values": {
      "en": "鶴崗市",
      "zh-Hant": "鶴崗市"
    }
  }, {
    "key": "C273",
    "values": {
      "en": "伊春市",
      "zh-Hant": "伊春市"
    }
  }, {
    "key": "C274",
    "values": {
      "en": "大興安嶺地區",
      "zh-Hant": "大興安嶺地區"
    }
  }, {
    "key": "C275",
    "values": {
      "en": "七台河市",
      "zh-Hant": "七台河市"
    }
  }, {
    "key": "C276",
    "values": {
      "en": "合肥市",
      "zh-Hant": "合肥市"
    }
  }, {
    "key": "C277",
    "values": {
      "en": "蕪湖市",
      "zh-Hant": "蕪湖市"
    }
  }, {
    "key": "C278",
    "values": {
      "en": "安慶市",
      "zh-Hant": "安慶市"
    }
  }, {
    "key": "C279",
    "values": {
      "en": "六安市",
      "zh-Hant": "六安市"
    }
  }, {
    "key": "C280",
    "values": {
      "en": "宿州市",
      "zh-Hant": "宿州市"
    }
  }, {
    "key": "C281",
    "values": {
      "en": "阜陽市",
      "zh-Hant": "阜陽市"
    }
  }, {
    "key": "C282",
    "values": {
      "en": "馬鞍山市",
      "zh-Hant": "馬鞍山市"
    }
  }, {
    "key": "C283",
    "values": {
      "en": "蚌埠市",
      "zh-Hant": "蚌埠市"
    }
  }, {
    "key": "C284",
    "values": {
      "en": "淮北市",
      "zh-Hant": "淮北市"
    }
  }, {
    "key": "C285",
    "values": {
      "en": "淮南市",
      "zh-Hant": "淮南市"
    }
  }, {
    "key": "C286",
    "values": {
      "en": "宣城市",
      "zh-Hant": "宣城市"
    }
  }, {
    "key": "C287",
    "values": {
      "en": "黃山市",
      "zh-Hant": "黃山市"
    }
  }, {
    "key": "C288",
    "values": {
      "en": "銅陵市",
      "zh-Hant": "銅陵市"
    }
  }, {
    "key": "C289",
    "values": {
      "en": "亳州市",
      "zh-Hant": "亳州市"
    }
  }, {
    "key": "C290",
    "values": {
      "en": "池州市",
      "zh-Hant": "池州市"
    }
  }, {
    "key": "C291",
    "values": {
      "en": "巢湖市",
      "zh-Hant": "巢湖市"
    }
  }, {
    "key": "C292",
    "values": {
      "en": "滁州市",
      "zh-Hant": "滁州市"
    }
  }, {
    "key": "C293",
    "values": {
      "en": "貴陽市",
      "zh-Hant": "貴陽市"
    }
  }, {
    "key": "C294",
    "values": {
      "en": "黔東南州",
      "zh-Hant": "黔東南州"
    }
  }, {
    "key": "C295",
    "values": {
      "en": "黔南州",
      "zh-Hant": "黔南州"
    }
  }, {
    "key": "C296",
    "values": {
      "en": "遵義市",
      "zh-Hant": "遵義市"
    }
  }, {
    "key": "C297",
    "values": {
      "en": "黔西南州",
      "zh-Hant": "黔西南州"
    }
  }, {
    "key": "C298",
    "values": {
      "en": "畢節地區",
      "zh-Hant": "畢節地區"
    }
  }, {
    "key": "C299",
    "values": {
      "en": "銅仁地區",
      "zh-Hant": "銅仁地區"
    }
  }, {
    "key": "C300",
    "values": {
      "en": "安顺市",
      "zh-Hant": "安顺市"
    }
  }, {
    "key": "C301",
    "values": {
      "en": "六盤水市",
      "zh-Hant": "六盤水市"
    }
  }, {
    "key": "C302",
    "values": {
      "en": "蘭州市",
      "zh-Hant": "蘭州市"
    }
  }, {
    "key": "C303",
    "values": {
      "en": "天水市",
      "zh-Hant": "天水市"
    }
  }, {
    "key": "C304",
    "values": {
      "en": "慶陽市",
      "zh-Hant": "慶陽市"
    }
  }, {
    "key": "C305",
    "values": {
      "en": "武威市",
      "zh-Hant": "武威市"
    }
  }, {
    "key": "C306",
    "values": {
      "en": "酒泉市",
      "zh-Hant": "酒泉市"
    }
  }, {
    "key": "C307",
    "values": {
      "en": "張掖市",
      "zh-Hant": "張掖市"
    }
  }, {
    "key": "C308",
    "values": {
      "en": "隴南地區",
      "zh-Hant": "隴南地區"
    }
  }, {
    "key": "C309",
    "values": {
      "en": "白銀市",
      "zh-Hant": "白銀市"
    }
  }, {
    "key": "C310",
    "values": {
      "en": "定西地區",
      "zh-Hant": "定西地區"
    }
  }, {
    "key": "C311",
    "values": {
      "en": "平涼市",
      "zh-Hant": "平涼市"
    }
  }, {
    "key": "C312",
    "values": {
      "en": "嘉峪關市",
      "zh-Hant": "嘉峪關市"
    }
  }, {
    "key": "C313",
    "values": {
      "en": "臨夏回族自治州",
      "zh-Hant": "臨夏回族自治州"
    }
  }, {
    "key": "C314",
    "values": {
      "en": "金昌市",
      "zh-Hant": "金昌市"
    }
  }, {
    "key": "C315",
    "values": {
      "en": "甘南州",
      "zh-Hant": "甘南州"
    }
  }, {
    "key": "C316",
    "values": {
      "en": "西寧市",
      "zh-Hant": "西寧市"
    }
  }, {
    "key": "C317",
    "values": {
      "en": "海西州",
      "zh-Hant": "海西州"
    }
  }, {
    "key": "C318",
    "values": {
      "en": "海東地區",
      "zh-Hant": "海東地區"
    }
  }, {
    "key": "C319",
    "values": {
      "en": "海北州",
      "zh-Hant": "海北州"
    }
  }, {
    "key": "C320",
    "values": {
      "en": "果洛州",
      "zh-Hant": "果洛州"
    }
  }, {
    "key": "C321",
    "values": {
      "en": "玉樹州",
      "zh-Hant": "玉樹州"
    }
  }, {
    "key": "C322",
    "values": {
      "en": "黄南藏族自治州",
      "zh-Hant": "黄南藏族自治州"
    }
  }, {
    "key": "C323",
    "values": {
      "en": "烏魯木齊市",
      "zh-Hant": "烏魯木齊市"
    }
  }, {
    "key": "C324",
    "values": {
      "en": "伊犁州",
      "zh-Hant": "伊犁州"
    }
  }, {
    "key": "C325",
    "values": {
      "en": "昌吉州",
      "zh-Hant": "昌吉州"
    }
  }, {
    "key": "C326",
    "values": {
      "en": "石河子市",
      "zh-Hant": "石河子市"
    }
  }, {
    "key": "C327",
    "values": {
      "en": "哈密地區",
      "zh-Hant": "哈密地區"
    }
  }, {
    "key": "C328",
    "values": {
      "en": "阿克蘇地區",
      "zh-Hant": "阿克蘇地區"
    }
  }, {
    "key": "C329",
    "values": {
      "en": "巴音郭楞州",
      "zh-Hant": "巴音郭楞州"
    }
  }, {
    "key": "C330",
    "values": {
      "en": "喀什地區",
      "zh-Hant": "喀什地區"
    }
  }, {
    "key": "C331",
    "values": {
      "en": "塔城地區",
      "zh-Hant": "塔城地區"
    }
  }, {
    "key": "C332",
    "values": {
      "en": "克拉瑪依市",
      "zh-Hant": "克拉瑪依市"
    }
  }, {
    "key": "C333",
    "values": {
      "en": "和田地區",
      "zh-Hant": "和田地區"
    }
  }, {
    "key": "C334",
    "values": {
      "en": "阿勒泰州",
      "zh-Hant": "阿勒泰州"
    }
  }, {
    "key": "C335",
    "values": {
      "en": "吐魯番地區",
      "zh-Hant": "吐魯番地區"
    }
  }, {
    "key": "C336",
    "values": {
      "en": "阿拉爾市",
      "zh-Hant": "阿拉爾市"
    }
  }, {
    "key": "C337",
    "values": {
      "en": "博爾塔拉州",
      "zh-Hant": "博爾塔拉州"
    }
  }, {
    "key": "C338",
    "values": {
      "en": "五家渠市",
      "zh-Hant": "五家渠市"
    }
  }, {
    "key": "C339",
    "values": {
      "en": "克孜勒蘇州",
      "zh-Hant": "克孜勒蘇州"
    }
  }, {
    "key": "C340",
    "values": {
      "en": "圖木舒克市",
      "zh-Hant": "圖木舒克市"
    }
  }, {
    "key": "C341",
    "values": {
      "en": "拉薩市",
      "zh-Hant": "拉薩市"
    }
  }, {
    "key": "C342",
    "values": {
      "en": "山南地區",
      "zh-Hant": "山南地區"
    }
  }, {
    "key": "C343",
    "values": {
      "en": "林芝地區",
      "zh-Hant": "林芝地區"
    }
  }, {
    "key": "C344",
    "values": {
      "en": "日喀則地區",
      "zh-Hant": "日喀則地區"
    }
  }, {
    "key": "C345",
    "values": {
      "en": "阿里地區",
      "zh-Hant": "阿里地區"
    }
  }, {
    "key": "C346",
    "values": {
      "en": "昌都地區",
      "zh-Hant": "昌都地區"
    }
  }, {
    "key": "C347",
    "values": {
      "en": "那曲地區",
      "zh-Hant": "那曲地區"
    }
  }, {
    "key": "C348",
    "values": {
      "en": "赤峰市",
      "zh-Hant": "赤峰市"
    }
  }, {
    "key": "C349",
    "values": {
      "en": "包頭市",
      "zh-Hant": "包頭市"
    }
  }, {
    "key": "C350",
    "values": {
      "en": "通遼市",
      "zh-Hant": "通遼市"
    }
  }, {
    "key": "C351",
    "values": {
      "en": "呼和浩特市",
      "zh-Hant": "呼和浩特市"
    }
  }, {
    "key": "C352",
    "values": {
      "en": "烏海市",
      "zh-Hant": "烏海市"
    }
  }, {
    "key": "C353",
    "values": {
      "en": "鄂爾多斯市",
      "zh-Hant": "鄂爾多斯市"
    }
  }, {
    "key": "C354",
    "values": {
      "en": "呼倫貝爾市",
      "zh-Hant": "呼倫貝爾市"
    }
  }, {
    "key": "C355",
    "values": {
      "en": "興安盟",
      "zh-Hant": "興安盟"
    }
  }, {
    "key": "C356",
    "values": {
      "en": "巴彥淖爾盟",
      "zh-Hant": "巴彥淖爾盟"
    }
  }, {
    "key": "C357",
    "values": {
      "en": "烏蘭察布盟",
      "zh-Hant": "烏蘭察布盟"
    }
  }, {
    "key": "C358",
    "values": {
      "en": "錫林郭勒盟",
      "zh-Hant": "錫林郭勒盟"
    }
  }, {
    "key": "C359",
    "values": {
      "en": "阿拉善盟",
      "zh-Hant": "阿拉善盟"
    }
  }, {
    "key": "C360",
    "values": {
      "en": "銀川市",
      "zh-Hant": "銀川市"
    }
  }, {
    "key": "C361",
    "values": {
      "en": "吳忠市",
      "zh-Hant": "吳忠市"
    }
  }, {
    "key": "C362",
    "values": {
      "en": "中衛市",
      "zh-Hant": "中衛市"
    }
  }, {
    "key": "C363",
    "values": {
      "en": "石嘴山市",
      "zh-Hant": "石嘴山市"
    }
  }, {
    "key": "C364",
    "values": {
      "en": "固原市",
      "zh-Hant": "固原市"
    }
  }]
};

var WebUIContext = React.createContext();
WebUIContext.displayName = 'WebUIContext';

var WebUIProvider = function WebUIProvider(_ref) {
  var children = _ref.children;

  // 定义左边菜单是否open
  var _useState = React.useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      leftMenuOpened = _useState2[0],
      setLeftMenuOpened = _useState2[1];

  var toggleDrawer = function toggleDrawer() {
    console.log("========这里是点击调用了toggleDrawer方法=======");
    setLeftMenuOpened(!leftMenuOpened);
  }; // 定义语言


  var _useState3 = React.useState("en"),
      _useState4 = _slicedToArray(_useState3, 2),
      localization = _useState4[0],
      setLocalization = _useState4[1]; //定义登录之后获得登录用户信息, 目前没有login page, 所以在userProfile.json设置登录userId
  // const userProfile = require(`mock/userProfile.json`)
  // const [agentCode] = useState(userProfile.agentCode);


  var _useState5 = React.useState(null),
      _useState6 = _slicedToArray(_useState5, 2),
      loginUser = _useState6[0],
      setLoginUser = _useState6[1]; //获得所有的dropdown list mapping
  // const mapping = require(`mock/mapping.json`);
  // 这里定义context级别的function


  var contextFunction = {
    toggleDrawer: toggleDrawer
  };
  return React__default.createElement(WebUIContext.Provider, {
    value: {
      leftMenuOpened: leftMenuOpened,
      setLeftMenuOpened: setLeftMenuOpened,
      localization: localization,
      setLocalization: setLocalization,
      loginUser: loginUser,
      setLoginUser: setLoginUser,
      contextFunction: contextFunction,
      mapping: mapping
    }
  }, children);
};

WebUIProvider.propTypes = {
  children: PropTypes.node.isRequired
}; // WebUIProvider.defaultProps = {
var WebUIConsumer = WebUIContext.Consumer;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function MainControl(props) {
  var style = props.style,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      dataObj = props.dataObj,
      items = props.items;
  var dataUrl = cmsProps.dataUrl;
  var cxt = React.useContext(WebUIContext);

  var _useState = React.useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      data = _useState2[0],
      setData = _useState2[1];

  console.log("这里是进入到MainControl.js, 页面data =>", data);
  var fetchPageData = React.useRef(
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  _regeneratorRuntime.mark(function _callee() {
    var pageData;
    return _regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return generateWebUIData(dataUrl, cxt, dataObj, {});

          case 2:
            pageData = _context.sent;
            setData(pageData ? pageData : {});

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }))); // 页面加载完成后会执行这里拿数据

  React.useEffect(function () {
    fetchPageData.current();
  }, []);
  return React__default.createElement("div", {
    style: style
  }, data && items.map(function (item, i) {
    return generateWebUI(item.cmsComponentType, _objectSpread({}, item, {
      dataObj: data
    }));
  }));
}

function ownKeys$1(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$1(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$1(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$1(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function Header(props) {
  var cxt = React.useContext(WebUIContext); //appBarPosition: PropTypes.oneOf(['absolute', 'fixed']), //fixed - 页面滚动的时候, appBar始终显示在页头
  // const functions = { ...cxt.contextFunction, ...props.functions };

  var muiProps = props.muiProps,
      style = props.style,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      dataObj = props.dataObj,
      items = props.items,
      _props$appBarPosition = props.appBarPosition,
      appBarPosition = _props$appBarPosition === void 0 ? "absolute" : _props$appBarPosition,
      callbackFunctions = props.callbackFunctions,
      actionFunctions = props.actionFunctions;
  var dataUrl = cmsProps.dataUrl; // console.log("cssClass:", cssClass);

  console.log("style:", style);
  console.log("cmsProps:", cmsProps);
  console.log("appBarPosition:", appBarPosition);
  var data = generateWebUIData(dataUrl, cxt, dataObj, {});
  return React__default.createElement(React__default.Fragment, null, React__default.createElement(AppBar, _extends({}, muiProps, {
    style: style
  }), React__default.createElement(Toolbar, null, items.map(function (item) {
    return generateWebUI(item.cmsComponentType, _objectSpread$1({}, item, {
      dataObj: data,
      callbackFunctions: callbackFunctions,
      actionFunctions: actionFunctions
    }));
  }))), React__default.createElement(Toolbar, {
    style: style
  }));
}

function ownKeys$2(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$2(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$2(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$2(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function GridRow(props) {
  // direction: PropTypes.oneOf(['row', 'row-reverse', 'column', 'column-reverse']),
  // justify: PropTypes.oneOf(['flex-start', 'center', 'flex-end', 'space-between', 'space-around', 'space-evenly']),
  // alignItems: PropTypes.oneOf(['flex-start', 'center', 'flex-end', 'stretch', 'baseline']),
  console.log("这里是传进来GridRowItems的pageJsonObj: ", props); //定义三个属性
  // const { direction = "row", justify = "flex-start", alignItems = "center", spacing = 0 } = props;
  //解构

  var muiProps = props.muiProps,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      dataObj = props.dataObj,
      items = props.items,
      style = props.style,
      actionFunctions = props.actionFunctions;
  var dataUrl = cmsProps.dataUrl;
  var cxt = React.useContext(WebUIContext);
  var data = generateWebUIData(dataUrl, cxt, dataObj, {});
  return React__default.createElement(core.Grid, _extends({
    container: true
  }, muiProps, {
    style: style
  }), items.map(function (item) {
    return generateWebUI(item.cmsComponentType, _objectSpread$2({}, item, {
      dataObj: data,
      actionFunctions: actionFunctions
    }));
  }));
}

function ownKeys$3(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$3(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$3(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$3(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function GridColumn(props) {
  // direction: PropTypes.oneOf(['row', 'row-reverse', 'column', 'column-reverse']),
  // justify: PropTypes.oneOf(['flex-start', 'center', 'flex-end', 'space-between', 'space-around', 'space-evenly']),
  // alignItems: PropTypes.oneOf(['flex-start', 'center', 'flex-end', 'stretch', 'baseline']),
  console.log("这里是传进来GridColumnItem的props: ", props); //定义三个属性
  // const { direction, justify, alignItems } = props.pageJsonObj;
  //解构

  var muiProps = props.muiProps,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style,
      items = props.items,
      actionFunctions = props.actionFunctions;
  var dataUrl = cmsProps.dataUrl,
      align = cmsProps.align;
  var columnJustify = "";

  if (align === "left") {
    columnJustify = "flex-start";
  } else if (align === "right") {
    columnJustify = "flex-end";
  } else if (align === "center") {
    columnJustify = "center";
  }

  var cxt = React.useContext(WebUIContext);
  var data = generateWebUIData(dataUrl, cxt, dataObj, {});
  return React__default.createElement(core.Grid, _extends({
    item: true
  }, muiProps, {
    style: style
  }), React__default.createElement(core.Grid, {
    container: true,
    direction: "row",
    justify: columnJustify,
    alignItems: "center"
  }, items.map(function (item, i) {
    return generateWebUI(item.cmsComponentType, _objectSpread$3({}, item, {
      dataObj: data,
      actionFunctions: actionFunctions
    }));
  })));
}

var icons = {
  Folder: React__default.createElement(Folder, null),
  People: React__default.createElement(People, null),
  Star: React__default.createElement(Star, null),
  Schedule: React__default.createElement(Schedule, null),
  OfflinePin: React__default.createElement(OfflinePin, null),
  Publish: React__default.createElement(Publish, null),
  Backup: React__default.createElement(Backup, null),
  Delete: React__default.createElement(Delete, null),
  Settings: React__default.createElement(Settings, null),
  Menu: React__default.createElement(Menu, null),
  Search: React__default.createElement(Search, null),
  Close: React__default.createElement(Close, null),
  Add: React__default.createElement(Add, null),
  Create: React__default.createElement(Create, null),
  ArrowBack: React__default.createElement(ArrowBack, null)
};

function ownKeys$4(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$4(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$4(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$4(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function EABButton(props) {
  console.log("====>这里是创建iconbutton, props: ", props);
  var cxt = React.useContext(WebUIContext);
  var cmsComponentType = props.cmsComponentType,
      muiProps = props.muiProps,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style,
      actionFunctions = props.actionFunctions;
  var dataUrl = cmsProps.dataUrl,
      actionType = cmsProps.actionType,
      actionUrl = cmsProps.actionUrl,
      iconName = cmsProps.iconName,
      fullScreen = cmsProps.fullScreen,
      displayText = cmsProps.displayText;
  var data = generateWebUIData(dataUrl, cxt, dataObj, {}); // 下面dialogOpened和toggleDialog是为了点击这个button弹出一个dialog使用的

  var _useState = React.useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      dialogOpened = _useState2[0],
      setDialogOpened = _useState2[1];

  var openDialog = function openDialog() {
    setDialogOpened(true);
  };

  var closeDialog = function closeDialog() {
    setDialogOpened(false);
  }; // 定义这个button事件, 如果actionType="dialog", 就是打开dialog, 否则就是调用useContext里面定义的方法
  // 目前定义在useContext里面的方法只有打开/关闭leftmenu


  var btnHandleClick;
  var dialogProps;

  if (actionType === "dialog") {
    btnHandleClick = openDialog; // 下面这里构建dialog需要的props, 把关闭dialog的方法传进去

    dialogProps = {
      cmsProps: {
        actionUrl: actionUrl,
        fullScreen: fullScreen
      },
      dialogOpened: dialogOpened,
      closeDialog: closeDialog
    };
  } else {
    // 这里将contextFunction和actionFunctions合并, 因为这个button有可能是要关闭dialog, 有可能是打开leftmenu
    var funcs = _objectSpread$4({}, cxt.contextFunction, {}, actionFunctions);

    btnHandleClick = funcs[actionUrl];
  }

  var generateButtonUI = function generateButtonUI() {
    switch (cmsComponentType) {
      case "ICON_BUTTON":
        return React__default.createElement(IconBtn, _extends({
          edge: "start"
        }, muiProps, {
          style: style,
          onClick: btnHandleClick
        }), icons[iconName]);

      case "BUTTON":
        return React__default.createElement(Button, _extends({}, muiProps, {
          style: style,
          onClick: btnHandleClick
        }), displayText);
    }
  };

  return React__default.createElement(React__default.Fragment, null, generateButtonUI(), dialogOpened && generateWebUI("EABDialog", _objectSpread$4({}, dialogProps, {
    dataObj: data,
    actionFunctions: actionFunctions
  })));
}

function EABLabel(props) {
  // const cxt = useContext(WebUIContext);
  // const { localization } = cxt;
  // const { css_class, align, valign, width, display, variant } = element;
  // const style = { textAlign: align, verticalAlign: valign, width, display };
  var muiProps = props.muiProps,
      variant = props.variant,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style;
  var value = cmsProps.value; // let data = generateWebUIData(dataUrl, cxt, dataObj);

  return React__default.createElement(core.Typography, _extends({
    variant: variant || "inherit"
  }, muiProps, {
    style: style
  }), value);
}

function ownKeys$5(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$5(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$5(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$5(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var useStyles = styles.makeStyles(function (theme) {
  return {
    root: {
      display: "flex",
      flexDirection: "column"
    },
    content: {
      flex: 1
    },
    padding: {
      padding: "24px"
    }
  };
});

function LeftMenu(props) {
  var classes = useStyles();
  var style = props.style,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      items = props.items;
  var dataUrl = cmsProps.dataUrl;
  console.log("这里是进入到LeftMenu.js, 页面items =>", items);
  var cxt = React.useContext(WebUIContext);
  var leftMenuOpened = cxt.leftMenuOpened,
      contextFunction = cxt.contextFunction; //从useContext中获得数据, 传递给所有的

  var _useState = React.useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      data = _useState2[0],
      setData = _useState2[1];

  console.log("这里是进入到LeftMenu.js, 页面data =>", data);
  var fetchPageData = React.useRef(
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  _regeneratorRuntime.mark(function _callee() {
    var pageData;
    return _regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return generateWebUIData(dataUrl, cxt, dataObj, {});

          case 2:
            pageData = _context.sent;
            setData(pageData ? pageData : {});

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }))); // 页面加载完成后会执行这里拿数据

  React.useEffect(function () {
    fetchPageData.current();
  }, []);
  return React__default.createElement(Drawer, {
    open: leftMenuOpened,
    onClose: contextFunction["toggleDrawer"]
  }, React__default.createElement("div", {
    className: classes.root,
    style: style
  }, data && items.map(function (item, i) {
    return generateWebUI(item.cmsComponentType, _objectSpread$5({}, item, {
      dataObj: data
    }));
  })));
}

LeftMenu.propTypes = {
  width: PropTypes.string,
  headerJson: PropTypes.object,
  contentJson: PropTypes.object,
  footerJson: PropTypes.object
};

function ownKeys$6(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$6(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$6(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$6(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function Blank(props) {
  var cssClass = props.cssClass,
      style = props.style,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      items = props.items,
      actionFunctions = props.actionFunctions;
  var dataUrl = cmsProps.dataUrl,
      params = cmsProps.params;

  var _useState = React.useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      pageData = _useState2[0],
      setPageData = _useState2[1];

  var cxt = React.useContext(WebUIContext);
  var fetchPageData = React.useRef(
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  _regeneratorRuntime.mark(function _callee() {
    var rquest, _pageData, _pageData2;

    return _regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!params) {
              _context.next = 8;
              break;
            }

            rquest = util.resolveParams(dataObj, dataUrl, params);
            _context.next = 4;
            return util.fetchData(rquest.dataUrl, rquest.params);

          case 4:
            _pageData = _context.sent;
            setPageData(_pageData ? _pageData : {});
            _context.next = 12;
            break;

          case 8:
            _context.next = 10;
            return generateWebUIData(dataUrl, cxt, dataObj, {});

          case 10:
            _pageData2 = _context.sent;
            setPageData(_pageData2 ? _pageData2 : {});

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));
  React.useEffect(function () {
    fetchPageData.current();
  }, []);
  return React__default.createElement("div", {
    className: cssClass,
    style: style
  }, pageData && items.map(function (item) {
    return generateWebUI(item.cmsComponentType, _objectSpread$6({}, item, {
      dataObj: pageData,
      actionFunctions: actionFunctions
    }));
  }));
}

function EABAvatar(props) {
  var muiProps = props.muiProps,
      keys = props.keys,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style;
  var dataUrl = cmsProps.dataUrl;
  var cxt = React.useContext(WebUIContext);
  var data = generateWebUIData(dataUrl, cxt, dataObj, {});
  return React__default.createElement(Avatar, _extends({}, muiProps, {
    style: style
  }));
}

var lookup = function lookup(key, mappingKey, localization, mapping) {
  if (key && key.length > 0) {
    var mapObj = _.get(mapping, mappingKey, []).filter(function (item) {
      return item.key === key;
    });

    return mapObj[0].values[localization];
  } else {
    return "";
  }
};

function EABText(props) {
  var cxt = React.useContext(WebUIContext);

  var _useContext = React.useContext(WebUIContext),
      localization = _useContext.localization,
      mapping = _useContext.mapping;

  var muiProps = props.muiProps,
      dataObj = props.dataObj,
      variant = props.variant,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style;
  var dataUrl = cmsProps.dataUrl,
      value = cmsProps.value; //拿到数据

  var data = generateWebUIData(dataUrl, cxt, dataObj, {});
  console.log("这里是创建Text, 应该能够拿到data呀: ", data); // const { css_class, align, valign, width, display, needBr, variant } = element;
  // const style = { textAlign: align, verticalAlign: valign, width: width, display: display };

  var getValueFromData = function getValueFromData(data, key) {
    console.log("这里是从数据对象里面取得数据", data);
    console.log("这里是key", key);

    if (key.indexOf("#lookup") === -1) {
      return data ? _.get(data, key.substr(1), "") : key;
    } else {
      //#lookup('#CAFE.gender','genderMapping')
      var position_first_character = key.indexOf('(');
      var position_second_character = key.replace('(', '@').indexOf(')');
      var mappingKey = key.substring(position_first_character + 1, position_second_character).replace(/'/g, "").split(",");

      var _value = _.get(data, mappingKey[0].substr(1), "");

      return lookup(_value, mappingKey[1], localization, mapping);
    }
  };

  return React__default.createElement(React__default.Fragment, null, React__default.createElement(Typography, _extends({
    variant: variant || "inherit"
  }, muiProps, {
    style: style
  }), getValueFromData(data, value)));
}

function ButtonLink(props) {
  var cssClass = props.cssClass,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style;
  var actionUrl = cmsProps.actionUrl,
      iconName = cmsProps.iconName,
      displayText = cmsProps.displayText;
  return React__default.createElement(ListItem, {
    button: true,
    className: cssClass,
    onClick: function onClick() {
      console.log("==这里跳转到===>", actionUrl);
    },
    style: style
  }, React__default.createElement(ListItemIcon, null, icons[iconName]), React__default.createElement(ListItemText, {
    primary: displayText
  }));
}

var css$1 = "/* 这里看起来是设置轮播图整体样式的 */\n.awssld {\n  --organic-arrow-thickness: 4px;\n  --organic-arrow-height: 40px;\n  --slider-height-percentage: 60%;\n  --loader-bar-color: #851515;\n  --loader-bar-height: 6px;\n  --control-button-width: 10%;\n  --control-button-height: 25%;\n  --control-button-opacity: 0.5;\n  --control-button-hover-opacity: 0.75;\n  --control-button-background: transparent;\n  --transition-bezier: cubic-bezier(0.5,0.075,0.25,0.95);\n  --slider-transition-duration: 200ms;\n  --organic-arrow-color: #6a6a6a;\n  --organic-arrow-border-radius: 0;\n  --control-bullet-color: #6a6a6a;\n  --control-bullet-active-color: #6a6a6a;\n  --content-background-color: #FFF;\n  max-width: 100%;\n}\n\n/* 这里看起来是轮播图主体位置信息的 */\n.awssld, .awssld__wrapper {\n  display: block;\n  position: static;\n  width: 100%;\n}\n\n/* 这里设置轮播图下方圆点区域位置信息 */\n.awssld__bullets {\n  position: fixed;\n  bottom: 20px;\n  width: 100%;\n  z-index: 2;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n/* 这里设置轮播图下方圆点样式 */\n.awssld__bullets button {\n  padding: 0;\n  display: block;\n  width: 12px;\n  height: 12px;\n  margin: 5px;\n  border-radius: 50%;\n  background: var(--control-bullet-color);\n  text-indent: -9999px;\n  overflow: hidden;\n  cursor: pointer;\n  border: none;\n  transition: transform .225s cubic-bezier(.8,1.35,.75,1.45),background-color .175s ease-out;\n}\n\n/* 这里必须要加上, 因为AwesomeSlider插件将它里面只要是div的都包了一层div */\n.awssld__content>div{\n  width: 100%;\n  height: 100%;\n}\n\n/* 这里主要是修改padding=0, 不然会出现滚动条 */\n.awssld__container{\n  display: block;\n  width: 100%;\n  height: 0;\n  padding: 0px;\n}";
styleInject(css$1);

function ownKeys$7(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$7(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$7(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$7(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function Carousell(props) {
  var cxt = React.useContext(WebUIContext);
  var style = props.style,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      dataObj = props.dataObj,
      items = props.items;
  var dataUrl = cmsProps.dataUrl;
  var data = generateWebUIData(dataUrl, cxt, dataObj, {});
  console.log("=====>>>这里是carousell里面的数据", items);
  return React__default.createElement(AwesomeSlider, {
    style: style
  }, items.map(function (item, inx) {
    return React__default.createElement(React__default.Fragment, {
      key: inx
    }, generateWebUI(item.cmsComponentType, _objectSpread$7({}, item, {
      dataObj: data
    })));
  }));
}

function EABImage(props) {
  console.log("这里是创建image的, props : ", props);
  var cxt = React.useContext(WebUIContext);
  var cssClass = props.cssClass,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style;
  var alt = cmsProps.alt,
      src = cmsProps.src;
  return React__default.createElement("img", {
    className: cssClass,
    style: style,
    src: src,
    alt: alt
  });
}

function ownKeys$8(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$8(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$8(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$8(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function EABDialog(props) {
  console.log("===你好呀, 我这里是创建了一个dialog: ", props);
  var cxt = React.useContext(WebUIContext);
  var dialogOpened = props.dialogOpened,
      closeDialog = props.closeDialog,
      callbackFunctions = props.callbackFunctions,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      dataObj = props.dataObj; // 下面两个字段是从IconButton传递过来的, 打开dialog一定会有一个button触发

  var actionUrl = cmsProps.actionUrl,
      fullScreen = cmsProps.fullScreen; // 每个dialog有两个state属性, dialogContentConfig代表这个dialog显示什么内容, dialogData代表这个dialog需要的数据

  var _useState = React.useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      dialogContentConfig = _useState2[0],
      setDialogContentConfig = _useState2[1];

  var _useState3 = React.useState(null),
      _useState4 = _slicedToArray(_useState3, 2),
      dialogData = _useState4[0],
      setDialogData = _useState4[1];

  React.useEffect(function () {
    console.log("这里是执行dialog里面的useEffect方法, actionUrl = ", actionUrl);

    function fetchDialogConfig() {
      return _fetchDialogConfig.apply(this, arguments);
    }

    function _fetchDialogConfig() {
      _fetchDialogConfig = _asyncToGenerator(
      /*#__PURE__*/
      _regeneratorRuntime.mark(function _callee() {
        var configRes;
        return _regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return util.fetchPageJsonData(actionUrl);

              case 2:
                configRes = _context.sent;
                // 这里根据json里面structure.dataUrl去获得整个dialog需要的数据
                // if ((configRes.data.structure || {}).dataUrl) {
                //   const dataRes = await axios.get(`${configRes.data.structure.dataUrl}`);
                //   setDialogData(dataRes.data);
                // } else {
                //   // let dataRes = generateWebUIData(dataUrl, cxt, dataObj);
                //   setDialogData(dataObj);
                // }
                setDialogData(dataObj); // 存储到state中

                setDialogContentConfig(configRes);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));
      return _fetchDialogConfig.apply(this, arguments);
    }

    fetchDialogConfig();
  }, []);
  console.log("===这里是dialog中重建functions, 里面有两个方法, 一个是关闭dialog, 一个是callback, 这个callback理论上专门提供给cafe form使用");
  var actionFunctions = {
    closeDialog: closeDialog
  }; // 设置callback

  actionFunctions.callbackFunction = function () {
    console.log("======你好呀, 这里是callback function");
  };

  return React__default.createElement(Dialog, {
    fullScreen: fullScreen,
    open: dialogOpened,
    onClose: closeDialog,
    disableBackdropClick: true,
    maxWidth: false
  }, dialogContentConfig === null ? React__default.createElement("div", {
    style: {
      width: "64px",
      height: "64px"
    }
  }, "loading data ...") : generateWebUI(dialogContentConfig.cmsComponentType, _objectSpread$8({}, dialogContentConfig, {
    dataObj: dialogData,
    actionFunctions: actionFunctions
  })));
}

var useStyles$1 = styles.makeStyles(function (theme) {
  return {
    root: {
      width: "100%",
      height: "100%",
      minWidth: "64px",
      minHeight: "64px",
      "& > div:first-child": {
        width: "100%",
        height: "100%"
      }
    }
  };
});
function CafeForm(props) {
  var clases = useStyles$1();
  console.log("这里是进入了CafeForm的创建js, 传进来的props: ", props); // 下面几个还不知道怎么获得

  var isPreview = props.isPreview,
      pageIndex = props.pageIndex; //得到传递过来的callbackFunctions, 这里的callbackFunctions一般是在iconButton那里创建的, 是一个array

  var cmsProps = props.cmsProps,
      dataObj = props.dataObj,
      actionFunctions = props.actionFunctions;
  var formId = cmsProps.formId,
      responseID = cmsProps.responseID,
      docType = cmsProps.docType;

  var _useState = React.useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      resID = _useState2[0],
      setResID = _useState2[1]; // 定义cafeForm的回调函数


  var webuiCallback = function webuiCallback() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    console.log("这里是cafe callback", state);

    if (state.success) {
      //循环调用callbackFunctions, 注意关闭事件应该在最后面一个
      actionFunctions["callbackFunction"]();
      actionFunctions["closeDialog"](); // callbackFunctions.map((func, i) => {
      //   console.log(`这里是执行callback function [${i}]`);
      //   func();
      // });
      // console.log("===这里是返回的MQ_docID:", state.MQ_docID);
      // console.log("===这里是传过来的functions:", functions);
      // if (functions["setNeedRefresh"]) {
      //   functions["setNeedRefresh"](needRefresh => !needRefresh);
      // }
      // functions["onClose"]();
      // MQ_docID
    }
  };

  React.useEffect(function () {
    if (responseID.indexOf("#") === 0) {
      if (dataObj && _.get(dataObj, responseID.substr(1), "")) {
        setResID(_.get(dataObj, responseID.substr(1), null));
      }
    }
  }, []);
  console.log("responseID ? configConstants.MQ.EDIT : configConstants.MQ.CREATE>", resID ? configConstants.MQ.EDIT : configConstants.MQ.CREATE);
  return React__default.createElement("div", {
    className: clases.root
  }, React__default.createElement(CafeDisplayEngine, {
    apiHost: configConstants.CAFE_API,
    formID: formId || "",
    responseID: resID || "",
    isPreview: isPreview || false,
    pageIndex: pageIndex || 0,
    isWebUI: true,
    onDone: webuiCallback,
    mqChannel: resID ? configConstants.MQ.EDIT : configConstants.MQ.CREATE,
    mqDocType: docType || "",
    mqDataType: configConstants.MQREADFROMMESSAGE
  }));
}

function ownKeys$9(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$9(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$9(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$9(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function Search$1(props) {
  var cssClass = props.cssClass,
      style = props.style,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      items = props.items,
      actionFunctions = props.actionFunctions;
  var dataUrl = cmsProps.dataUrl,
      params = cmsProps.params;
  var cxt = React.useContext(WebUIContext);

  var _useState = React.useState(""),
      _useState2 = _slicedToArray(_useState, 2),
      searchStr = _useState2[0],
      setSearchStr = _useState2[1];

  var _useState3 = React.useState(null),
      _useState4 = _slicedToArray(_useState3, 2),
      searchResult = _useState4[0],
      setSearchResult = _useState4[1];

  var data = {
    // ...generateWebUIData(dataUrl, cxt, dataObj),
    searchStr: searchStr
  };
  React.useEffect(function () {
    if (cmsProps.autoSearch) {
      console.log("execAutoSearch");
    }
  }, [searchStr]);

  var handleClear = function handleClear() {
    setSearchStr("");
    setSearchResult(null);
  };

  var fetchPageData = React.useRef(
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  _regeneratorRuntime.mark(function _callee() {
    var rquest, pageData;
    return _regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            rquest = util.resolveParams(data, dataUrl, params);
            _context.next = 3;
            return util.fetchData(rquest.dataUrl, rquest.params);

          case 3:
            pageData = _context.sent;
            setSearchResult(pageData);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));

  var handleSearch = function handleSearch() {
    fetchPageData.current();
  };

  var searchFunctions = _objectSpread$9({}, actionFunctions, {
    setSearchStr: setSearchStr,
    handleClear: handleClear,
    handleSearch: handleSearch
  });

  return React__default.createElement("div", {
    className: cssClass,
    style: style
  }, items.map(function (item, i) {
    return generateWebUI(item.cmsComponentType, _objectSpread$9({}, item, {
      dataObj: _objectSpread$9({
        searchStr: searchStr,
        searchResult: searchResult
      }, dataObj),
      actionFunctions: searchFunctions
    }));
  }));
}

function Input(props) {
  console.log("====>这里是创建Input, props: ", props);
  var cssClass = props.cssClass,
      dataObj = props.dataObj,
      muiProps = props.muiProps,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style,
      actionFunctions = props.actionFunctions;
  var name = cmsProps.name,
      _onChange = cmsProps.onChange;
  return React__default.createElement(TextField, _extends({
    className: cssClass
  }, muiProps, {
    style: style,
    value: dataObj[name] || "",
    onChange: function onChange(event) {
      if (_onChange) {
        actionFunctions[_onChange](event.target.value);
      }
    }
  }));
}

function ownKeys$a(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$a(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$a(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$a(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function SearchResult(props) {
  var _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style,
      items = props.items,
      dataObj = props.dataObj,
      actionFunctions = props.actionFunctions;
  var align = cmsProps.align,
      xs = cmsProps.xs,
      actionType = cmsProps.actionType,
      actionUrl = cmsProps.actionUrl,
      fullScreen = cmsProps.fullScreen,
      dataUrl = cmsProps.dataUrl;
  var cxt = React.useContext(WebUIContext);

  var _useState = React.useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      currentData = _useState2[0],
      setCurrentData = _useState2[1];

  var _useState3 = React.useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      dialogOpened = _useState4[0],
      setDialogOpened = _useState4[1];

  var closeDialog = function closeDialog() {
    setCurrentData(null);
    setDialogOpened(false);
  };

  React.useEffect(function () {
    if (currentData && actionType === "dialog") {
      setDialogOpened(true);
    }
  }, [currentData]);
  var result = dataObj[dataUrl];
  console.log("SearchResult result data => ", result);
  var columnJustify = "";

  if (align === "left") {
    columnJustify = "flex-start";
  } else if (align === "right") {
    columnJustify = "flex-end";
  } else if (align === "center") {
    columnJustify = "center";
  }

  return React__default.createElement(core.Grid, {
    container: true,
    style: style,
    direction: "row",
    justify: columnJustify,
    alignItems: "center"
  }, React__default.createElement(core.Grid, {
    item: true,
    xs: xs
  }, Array.isArray(result) && result.map(function (data, inx) {
    return React__default.createElement("div", {
      key: inx,
      onClick: function onClick() {
        setCurrentData(data);
      }
    }, items.map(function (item) {
      return generateWebUI(item.cmsComponentType, _objectSpread$a({}, item, {
        dataObj: data,
        actionFunctions: _objectSpread$a({}, actionFunctions, {
          setCurrentData: setCurrentData
        })
      }));
    }));
  })), dialogOpened && generateWebUI("EABDialog", {
    cmsProps: {
      actionUrl: actionUrl,
      fullScreen: fullScreen
    },
    dialogOpened: dialogOpened,
    closeDialog: closeDialog,
    dataObj: currentData,
    actionFunctions: actionFunctions
  }));
}

function ownKeys$b(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$b(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$b(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$b(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function EABCard(props) {
  var muiProps = props.muiProps,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      cmsProps = _props$cmsProps === void 0 ? {} : _props$cmsProps,
      style = props.style,
      items = props.items,
      actionFunctions = props.actionFunctions;
  var dataUrl = cmsProps.dataUrl;
  var cxt = React.useContext(WebUIContext);
  var data = generateWebUIData(dataUrl, cxt, dataObj, {});
  return React__default.createElement(React__default.Fragment, null, React__default.createElement(Card, _extends({}, muiProps, {
    style: style
  }), React__default.createElement(CardContent, null, items.map(function (item) {
    return generateWebUI(item.cmsComponentType, _objectSpread$b({}, item, {
      dataObj: data,
      actionFunctions: actionFunctions
    }));
  }))));
}

function TabPanel(props) {
  var children = props.children,
      value = props.value,
      index = props.index,
      style = props.style,
      other = _objectWithoutProperties(props, ["children", "value", "index", "style"]);

  return React__default.createElement(Typography, _extends({
    component: "div",
    role: "tabpanel",
    hidden: value !== index,
    id: "simple-tabpanel-".concat(index),
    "aria-labelledby": "simple-tab-".concat(index)
  }, other), value === index && React__default.createElement(Box, {
    style: style,
    p: 3
  }, children));
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function ownKeys$c(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$c(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$c(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$c(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function TabsComponent(props) {
  var muiProps = props.muiProps,
      style = props.style,
      dataObj = props.dataObj,
      _props$cmsProps = props.cmsProps,
      items = props.items,
      actionFunctions = props.actionFunctions;

  var _useState = React.useState(0),
      _useState2 = _slicedToArray(_useState, 2),
      tabsValue = _useState2[0],
      setTabsValue = _useState2[1];

  var _useState3 = React.useState(null),
      _useState4 = _slicedToArray(_useState3, 2),
      currentPage = _useState4[0],
      setCurrentPage = _useState4[1];

  var cxt = React.useContext(WebUIContext);
  var tabs = [];

  if (Array.isArray(items)) {
    tabs = items.filter(function (item) {
      return item.cmsComponentType === "TAB";
    });
  }

  React.useEffect(function () {
    fetchCurrentPage();
  }, [tabsValue]);

  var fetchCurrentPage =
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    _regeneratorRuntime.mark(function _callee() {
      var configRes;
      return _regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return util.fetchPageJsonData(tabs[tabsValue].cmsProps.actionUrl);

            case 2:
              configRes = _context.sent;
              setCurrentPage(configRes);

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function fetchCurrentPage() {
      return _ref.apply(this, arguments);
    };
  }();

  return React__default.createElement(React__default.Fragment, null, React__default.createElement(Tabs, _extends({}, muiProps, {
    style: style,
    value: tabsValue,
    onChange: function onChange(event, newValue) {
      setTabsValue(newValue);
    }
  }), tabs.map(function (tab, inx) {
    return React__default.createElement(Tab, _extends({
      key: inx
    }, tab.muiProps, {
      style: tab.style
    }));
  })), React__default.createElement(Divider, null), tabs.map(function (tab, inx) {
    return tabsValue === inx ? React__default.createElement(TabPanel, {
      key: inx,
      value: tabsValue,
      index: inx,
      style: tab.style
    }, !currentPage ? null : generateWebUI(currentPage.cmsComponentType, _objectSpread$c({}, currentPage, {
      dataObj: dataObj,
      actionFunctions: actionFunctions
    }))) : React__default.createElement(TabPanel, {
      key: inx,
      value: tabsValue,
      index: inx
    });
  }));
}

function ownKeys$d(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$d(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$d(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$d(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
function LineSeparator(props) {
  var muiProps = props.muiProps,
      style = props.style,
      _props$cmsProps = props.cmsProps,
      actionFunctions = props.actionFunctions;
  return React__default.createElement(Divider, _extends({}, muiProps, {
    style: _objectSpread$d({}, style, {
      width: "100%"
    })
  }));
}

var generateMainControl = function generateMainControl(_ref) {
  var props = _extends({}, _ref);

  console.log("这里执行generateMainControl", props);
  return React__default.createElement(MainControl, props);
};

var generateHeader = function generateHeader(_ref2) {
  var props = _extends({}, _ref2);

  return React__default.createElement(Header, props);
};

var generateGridRow = function generateGridRow(_ref3) {
  var props = _extends({}, _ref3);

  return React__default.createElement(GridRow, props);
};

var generateGridColumn = function generateGridColumn(_ref4) {
  var props = _extends({}, _ref4);

  return React__default.createElement(GridColumn, props);
};

var generateButton = function generateButton(_ref5) {
  var props = _extends({}, _ref5);

  return React__default.createElement(EABButton, props);
};

var generateLabel = function generateLabel(_ref6) {
  var props = _extends({}, _ref6);

  return React__default.createElement(EABLabel, props);
};

var generateLeftMenu = function generateLeftMenu(_ref7) {
  var props = _extends({}, _ref7);

  return React__default.createElement(LeftMenu, props);
};

var generateBlank = function generateBlank(_ref8) {
  var props = _extends({}, _ref8);

  return React__default.createElement(Blank, props);
};

var generateSearch = function generateSearch(_ref9) {
  var props = _extends({}, _ref9);

  return React__default.createElement(Search$1, props);
};

var generateImageAvatar = function generateImageAvatar(_ref10) {
  var props = _extends({}, _ref10);

  return React__default.createElement(EABAvatar, props);
};

var generateText = function generateText(_ref11) {
  var props = _extends({}, _ref11);

  return React__default.createElement(EABText, props);
};

var generateButtonLink = function generateButtonLink(_ref12) {
  var props = _extends({}, _ref12);

  return React__default.createElement(ButtonLink, props);
};

var generateCarsousell = function generateCarsousell(_ref13) {
  var props = _extends({}, _ref13);

  return React__default.createElement(Carousell, props);
};

var generateImage = function generateImage(_ref14) {
  var props = _extends({}, _ref14);

  return React__default.createElement(EABImage, props);
};

var generateEABDialog = function generateEABDialog(_ref15) {
  var props = _extends({}, _ref15);

  return React__default.createElement(EABDialog, props);
};

var generateCafeForm = function generateCafeForm(_ref16) {
  var props = _extends({}, _ref16);

  return React__default.createElement(CafeForm, props);
};

var generateInput = function generateInput(_ref17) {
  var props = _extends({}, _ref17);

  return React__default.createElement(Input, props);
};

var generateSearchResult = function generateSearchResult(_ref18) {
  var props = _extends({}, _ref18);

  return React__default.createElement(SearchResult, props);
};

var generateCard = function generateCard(_ref19) {
  var props = _extends({}, _ref19);

  return React__default.createElement(EABCard, props);
};

var generateTabs = function generateTabs(_ref20) {
  var props = _extends({}, _ref20);

  return React__default.createElement(TabsComponent, props);
};

var generateLineSeparator = function generateLineSeparator(_ref21) {
  var props = _extends({}, _ref21);

  return React__default.createElement(LineSeparator, props);
}; //这里将所有的创建方法和type做一个映射


var generateFunctions = new Map([["MAIN_CONTROL", generateMainControl], ["HEADER", generateHeader], ["GRID_ROW", generateGridRow], ["GRID_COLUMN", generateGridColumn], ["ICON_BUTTON", generateButton], ["BUTTON", generateButton], ["LABEL", generateLabel], ["LEFT_MENU", generateLeftMenu], ["BLANK", generateBlank], ["IMAGE_AVATAR", generateImageAvatar], ["TEXT", generateText], ["BUTTON_LINK", generateButtonLink], ["CARSOUSELL", generateCarsousell], ["IMAGE", generateImage], ["EABDialog", generateEABDialog], ["CAFE_FORM", generateCafeForm], ["SEARCH", generateSearch], ["INPUT", generateInput], ["SEARCH_RESULT", generateSearchResult], ["CARD", generateCard], ["TABS", generateTabs], ["LINE_SEPARATOR", generateLineSeparator]]);

var generateWebUI = function generateWebUI(key, props) {
  console.log("调用generateWebUI:", key, props);
  var generateFunction = generateFunctions.get(key);

  if (generateFunction) {
    return generateFunction(props);
  }
};

var generateWebUIData = function generateWebUIData(dataUrl, cxt, dataObj, params) {
  if (dataUrl) {
    if (dataObj && dataObj[dataUrl]) {
      return dataObj[dataUrl];
    } else if (cxt[dataUrl]) {
      return cxt[dataUrl];
    } else {
      console.log("这里generateWebUIData调用的url > ", dataUrl);
      var data = webAjax.get(dataUrl, params).then(function (res) {
        console.log("这里generateWebUIData返回的response > ", res);

        if (res.success && res.result) {
          console.log("==这里是根据dataUrl查询到的数据:", res.result[0]);
          return res.result[0];
        } else {
          return {
            msg: "数据查询不是success!"
          };
        }
      });
      return data;
    }
  } else {
    return dataObj;
  }
};

function DisplayEngine(props) {
  console.log("===这里调用display engine传递过来的organizationId, projectId分别是 :=>", props);
  var _props$params = props.params,
      params = _props$params === void 0 ? {} : _props$params;
  var organizationId = params.organizationId,
      projectId = params.projectId;

  var _useState = React.useState(props.data),
      _useState2 = _slicedToArray(_useState, 1),
      displayEngineData = _useState2[0];

  var _useState3 = React.useState(null),
      _useState4 = _slicedToArray(_useState3, 2),
      displayConfig = _useState4[0],
      setDisplayConfig = _useState4[1]; // 定义拿到这个project的firstPageId


  var fetchFirstPageConfig = React.useRef(
  /*#__PURE__*/
  _asyncToGenerator(
  /*#__PURE__*/
  _regeneratorRuntime.mark(function _callee() {
    var projectInfo, _displayConfig, _projectInfo, _displayConfig2;

    return _regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(configConstants.BACKEND_URL_FOR_PAGE.indexOf("getJsonByDocId") != -1)) {
              _context.next = 12;
              break;
            }

            _context.next = 3;
            return util.fetchPageJsonData("first_page");

          case 3:
            projectInfo = _context.sent;
            console.log("这里拿到firstPage对象:", projectInfo);
            _context.next = 7;
            return util.fetchPageJsonData(projectInfo.firstPageId);

          case 7:
            _displayConfig = _context.sent;
            console.log("这里拿到firstPage里面配置的json对象:", _displayConfig);
            setDisplayConfig(_displayConfig);
            _context.next = 21;
            break;

          case 12:
            _context.next = 14;
            return util.fetchProjectJsonData(organizationId, projectId);

          case 14:
            _projectInfo = _context.sent;
            console.log("这里拿到firstPage对象:", _projectInfo);
            _context.next = 18;
            return util.fetchPageJsonData(_projectInfo.firstPageId);

          case 18:
            _displayConfig2 = _context.sent;
            console.log("这里拿到firstPage里面配置的json对象:", _displayConfig2);
            setDisplayConfig(_displayConfig2);

          case 21:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  })));
  React.useEffect(function () {
    if (displayEngineData) {
      setDisplayConfig(displayEngineData);
    } else {
      fetchFirstPageConfig.current();
    }
  }, [displayEngineData]);

  var displayEngine = function displayEngine() {
    // const { type, pageOrComponent, items } = displayConfig;
    // return (pageOrComponent === "page" ? createWebUIPage({ type, items }) : createWebUIComponent({ type, items }));
    console.log("这里是displayEngine: ", displayConfig);
    return generateWebUI(displayConfig.cmsComponentType, displayConfig);
  };

  return React__default.createElement(React__default.Fragment, null, displayConfig && displayEngine());
}

// import FullScreenDialog from 'bak/FullScreenDialog';
// import DisplayEngineProvider from 'componentsByWebUI/displayEngine/DisplayEngineProvider';

var Routes = function Routes(_ref) {
  var displayJson = _ref.displayJson;
  return React__default.createElement(reactRouterDom.Switch, null, React__default.createElement(reactRouterDom.Route, {
    path: '/',
    exact: true,
    render: function render() {
      return React__default.createElement(DisplayEngine, {
        data: displayJson
      });
    }
  }), React__default.createElement(reactRouterDom.Route, {
    path: '/:organizationId/:projectId',
    exact: true,
    render: function render(_ref2) {
      var params = _ref2.match.params;
      return React__default.createElement(DisplayEngine, {
        data: displayJson,
        params: params
      });
    }
  }));
};

var hist = history.createBrowserHistory();
var useStyles$2 = styles.makeStyles({
  root: {
    display: 'flex'
  }
});
function App(props) {
  var classes = useStyles$2();
  return React__default.createElement("div", {
    className: classes.root
  }, React__default.createElement(CssBaseline, null), React__default.createElement(WebUIProvider, null, React__default.createElement(styles$1.ThemeProvider, {
    theme: customTheme()
  }, React__default.createElement(reactRouterDom.Router, {
    history: hist
  }, React__default.createElement(Routes, {
    displayJson: props.displayJson
  })))));
}

module.exports = App;
//# sourceMappingURL=index.js.map
