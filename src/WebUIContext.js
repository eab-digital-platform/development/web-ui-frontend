import React, { useRef, useState, createContext } from 'react';
import PropTypes from 'prop-types';
import mapping from "config/mapping";
import configConstants from "config/configConstants";
import Loading from 'componentsByWebUI/components/Loading/Loading';

const WebUIContext = createContext();
WebUIContext.displayName = 'WebUIContext';

const WebUIProvider = ({ children }) => {
  // 定义左边菜单是否open
  const [leftMenuOpened, setLeftMenuOpened] = useState(false);
  const toggleDrawer = () => {
    console.log("========这里是点击调用了toggleDrawer方法=======");
    setLeftMenuOpened(!leftMenuOpened);
  };
  // 定义语言
  const [localization, setLocalization] = useState("en");
  //定义登录之后获得登录用户信息, 目前没有login page, 所以在userProfile.json设置登录userId
  // const userProfile = require(`mock/userProfile.json`)
  // const [agentCode] = useState(userProfile.agentCode);
  const [loginUser, setLoginUser] = useState(null);
  //获得所有的dropdown list mapping
  // const mapping = require(`mock/mapping.json`);

  // 定义loading ref
  const loadingRef = useRef(null);
  // 定义调用loading显示隐藏的方法
  const showLoading = () => {
    loadingRef.current.showBackdrop();
  };
  const hideLoading = () => {
    loadingRef.current.hideBackdrop();
  };

  // 这里定义context级别的function
  const contextFunction = {
    toggleDrawer: toggleDrawer,
    showLoading: showLoading,
    hideLoading: hideLoading
  };

  //关闭默认控制台打印
  if (!configConstants.DEBUG) {
    window['console']['log'] = function () { };
  }

  return (
    <WebUIContext.Provider value={{
      leftMenuOpened,
      setLeftMenuOpened,
      localization,
      setLocalization,
      loginUser,
      setLoginUser,
      contextFunction,
      mapping
    }}>
      {children}
      <Loading ref={loadingRef} />
    </WebUIContext.Provider>
  );
};

WebUIProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

// WebUIProvider.defaultProps = {
//   initialOpened: false,
//   initialCollapsed: false,
//   initialSecondaryOpened: false,
//   initialSecondaryCollapsed: false,
//   config: defaultLayoutPreset,
//   parseConfig: c => c,
// };

export { WebUIProvider };

export const WebUIConsumer = WebUIContext.Consumer;

export default WebUIContext;
