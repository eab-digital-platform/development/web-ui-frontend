import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    // maxWidth: 345,
    backgroundColor:""
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function PaymentForm() {
  const classes = useStyles();

  return (
    <Grid container direction="row" justify="center" alignItems="center" style={{ padding: "50px" }}>
      <Grid item xs={5}>
        <Card className={classes.root}>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                C
          </Avatar>
            }
            title="Global Payments"
            subheader="Pay securely using your credit card."
          />
          <CardContent>
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <TextField required id="cardName" label="Name on card" variant="outlined" fullWidth />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField required id="cardNumber" label="Card number" variant="outlined" fullWidth />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField required id="expDate" label="Expiry date" variant="outlined" fullWidth />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  required
                  id="cvv"
                  label="CVV"
                  helperText="Last three digits on signature strip"
                  variant="outlined"
                  fullWidth
                />
              </Grid>
            </Grid>
          </CardContent>
          <CardContent>
            <Typography variant="body2" color="textSecondary" component="p">
              This impressive paella is a perfect party dish and a fun meal to cook together with your
              guests. Add 1 cup of frozen peas along with the mussels, if you like.
          </Typography>
          </CardContent>
          <CardActions disableSpacing>
            <Button variant="contained" color="primary" fullWidth>Place order</Button>
          </CardActions>
        </Card>
      </Grid>
    </Grid>
  );
}
