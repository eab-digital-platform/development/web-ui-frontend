import _ from 'lodash';

const lookup = (key, mappingKey, localization, mapping) => {
  if (key && key.length > 0) {
    const mapObj = _.get(mapping, mappingKey, []).filter(item => item.key === key);
    return mapObj.length > 0 ? mapObj[0].values[localization] : "";
  } else {
    return "";
  }
}

const getValueFromData = (data, key, localization, mapping) => {
  console.log("这里是从数据对象里面取得数据", data);
  console.log("这里是key", key);

  if (key) {
    if (key.indexOf("#lookup") === -1) {
      return data ? _.get(data, key.substr(1), "") : key;
    } else {
      //#lookup('#CAFE.gender','genderMapping')
      const position_first_character = key.indexOf('(');
      const position_second_character = key.replace('(', '@').indexOf(')');
      const mappingKey = key.substring(position_first_character + 1, position_second_character).replace(/'/g, "").split(",");
      const value = _.get(data, mappingKey[0].substr(1), "");
      return lookup(value, mappingKey[1], localization, mapping)
    }
  }else{
    return "";
  }
  
}

export default { getValueFromData };