import axios from "axios";
import qs from 'qs';
// let axiosPreURl = "http://localhost:30001/";//配置全局的代理
let axiosTimeOut = 10000;

// 这里应该是每次请求拦截器
axios.interceptors.request.use(config => {//config包含每次请求的内容
  if (localStorage.getItem('api_token')) {
    config.headers.apiToken = `${localStorage.getItem('api_token')}`;
  }
  if (config.method === 'post') {
    config.data = qs.stringify(config.data);
  }
  return config;
}, err => {
  return Promise.reject(err);
});

// 这里应该是每次响应(返回数据)拦截器
axios.interceptors.response.use(response => {
  // 这里可以写对于返回数据的拦截处理, 这里可以仅仅是返回response.data数据
  return response;
}, error => {
  console.log("=====数据返回错误=====", error);
  return Promise.resolve(error.response)
});

// 检查状态码的方法
function checkStatus(response) {
  console.log("=====这里调用ajax返回的response=====", response);
  if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
    // 检查返回的status, 如果没问题, 仅仅返回response.data数据
    console.log("=====response.status没有问题, 将直接返回response.data=====");
    return response.data;
  } else {
    console.log("=====response.status出现问题, response > =====", response);
    return {
      status: -404,
      msg: '网络异常'
    }
  }
}

// 如果失败，弹出错误信息
function checkCode(response) {
  if (response.status === -404) {
    // alert(response.msg);
    console.log("======axios检查返回状态出错=====", response);
  }
  return response;
}

export default {
  post(url, data) {
    console.log("======这里执行的是axios的post方法=====", url, data);
    return axios({
      method: 'post',
      // baseURL: axiosPreURl,
      url,
      data: data,
      timeout: axiosTimeOut,
    })
      .then(res => checkStatus(res))
      .then(res => checkCode(res))
  },

  get(url, params) {
    return axios({
      method: 'get',
      // baseURL: axiosPreURl,
      url: url,
      params,
      timeout: axiosTimeOut,
      headers: {
        'x-Requested-With': 'XMLHttpRequest'
      }
    })
      .then(res => checkStatus(res))
      .then(res => checkCode(res))
  }
}
