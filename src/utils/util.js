import _ from 'lodash';
import webAjax from 'utils/request';
import configConstants from "config/configConstants";

const resolveParams = (data, dataUrl, params) => {
  const result = { dataUrl, params: {} }
  Array.isArray(params) && params.forEach(param => {
    // console.log("=====>>>>>paramparamparam",typeof param.value);
    if (param.name && param.name !== "") {
      // 如果是一个数组
      if (Array.isArray(param.value) && param.value.length > 0) {
        let paramObj = {};
        param.value.forEach(item => {
          if (typeof item.value === "string" && item.value.indexOf("#") === 0) {
            paramObj[item.name] = _.get(data, item.value.substr(1), "");
          } else {
            paramObj[item.name] = item.value;
          }
        });
        result.params[param.name] = paramObj;
      }
      // 不是数组
      else if (typeof param.value === "string" && param.value.indexOf("#") === 0) {
        if (data && _.get(data, param.value.substr(1), "")) {
          result.params[param.name] = _.get(data, param.value.substr(1), "");
        }
      } else {
        result.params[param.name] = param.value;
      }
    } else if (param.value && param.value !== "") {
      if (param.value.indexOf("#") === 0) {
        if (data && _.get(data, param.value.substr(1), "")) {
          result.dataUrl = `${result.dataUrl}/${_.get(data, param.value.substr(1), "")}`;
        }
      } else {
        result.dataUrl = `${result.dataUrl}/${param.value}`;
      }
    }
  });
  return result;
}

const resolveParamsValue = (data, params) => {
  const result = {};
  Array.isArray(params) && params.forEach(param => {
    if (param.name && param.name !== "") {
      if (Array.isArray(param.value) && param.value.length > 0) { // 如果是一个数组
        let paramObj = {};
        param.value.forEach(item => {
          if (typeof item.value === "string" && item.value.indexOf("#") === 0) {
            paramObj[item.name] = _.get(data, item.value.substr(1), "");
          } else {
            paramObj[item.name] = item.value;
          }
        });
        result[param.name] = paramObj;
      } else if (typeof param.value === "string" && param.value.indexOf("#") === 0) { // 不是数组
        if (data && _.get(data, param.value.substr(1), "")) {
          result[param.name] = _.get(data, param.value.substr(1), "");
        }
      } else {
        result[param.name] = param.value;
      }
    } else if (param.value && param.value !== "") {
      if (param.value.indexOf("#") === 0) {
        if (data && _.get(data, param.value.substr(1), "")) {
          result[param.value.substr(1)] = _.get(data, param.value.substr(1), "");
        }
      } else {
        result[param.value] = param.value;
      }
    }
  });
  return result;
}

const fetchData = (dataUrl = "", params = {}, requestType = "get") => {
  console.log("run fetchData > ", dataUrl);
  requestType = requestType === "post" ? requestType : "get";
  return webAjax[requestType](dataUrl, params).then(res => {
    console.log("======这里调用了fetchData返回了res==== > ", res);
    if (res.success) {
      if (res.result) {
        return res.result;
      } else if (res.profile) {
        return res.profile;
      } else {
        return res;
      }
    } else {
      console.error("没有获取到结果: ", res);
      return null;
    }
  });
}

const fetchPageJsonData = (pageId, params = {}) => {
  console.log("===这里是得到page json数据的=== > ", pageId);
  // http://cmsadminapi-dp.eab.openshift/pageMaster/id/
  return webAjax.get(`${configConstants.BACKEND_URL_FOR_PAGE}/${pageId}`, params);
}

const fetchProjectJsonData = (organizationId, projectId, params = {}) => {
  // http://cmsadminapi-dp.eab.openshift/project/5e572b0450b21a0032f5c444
  console.log("===这里是得到project setting数据的=== > ", organizationId, projectId);
  return webAjax.get(`${configConstants.BACKEND_URL_FOR_PROJECT}/${organizationId}/${projectId}`, params);
}

// const fetchPageJsonDataForDev = (pageId, params={}) => {
//   console.log("===这里是本地测试用的, 得到page json数据的=== > ", pageId);
//   // http://cmsadminapi-dp.eab.openshift/pageMaster/id/
//   return webAjax.get(`${configConstants.BACKEND_URL_FOR_DEV}/getJsonByDocId/${pageId}`, params);
// }

export default {
  resolveParams,
  resolveParamsValue,
  fetchData,
  fetchPageJsonData,
  fetchProjectJsonData
};