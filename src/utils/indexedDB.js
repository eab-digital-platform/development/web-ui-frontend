let db;
export const initDB = () => {
  // 这里使用了indexedDB, IndexedDB 就是浏览器提供的本地数据库，它可以被网页脚本创建和操作。IndexedDB 允许储存大量数据，提供查找接口，还能建立索引
  let dbRequest = window.indexedDB.open("eab_digital_platform");
  // 数据库升级方法, 如果指定的版本号(上面的version)，大于数据库的实际版本号，就会发生数据库升级事件.
  // IndexedDB 数据库有版本的概念。同一个时刻，只能有一个版本的数据库存在。如果要修改数据库结构（新增或删除表、索引或者主键），只能通过升级数据库版本完成
  dbRequest.onupgradeneeded = (event) => {
    db = event.target.result;
    // 如果table不存在db里面就创建table, 创建索引
    if (!db.objectStoreNames.contains("templates")) {
      let objectStore = db.createObjectStore("templates", { keyPath: "_id" });
      objectStore.createIndex("_id", "_id", { unique: false });
    }
  };
  // 成功打开数据库
  dbRequest.onsuccess = () => {
    console.log("===数据库打开成功===: dbName:[eab_digital_platform] dbTable[templates]");
    db = dbRequest.result;
  };
  // 打开数据库失败
  dbRequest.onerror = () => {
    console.log("===数据库打开报错===: dbName:[eab_digital_platform] dbTable[templates]");
  };
};

export const addPageJson = (payload) => {
  const addRequest = db.transaction(['templates'], 'readwrite').objectStore('templates').add({ ...payload });
  addRequest.onsuccess = () => {
    console.log("===数据写入成功===");
  };
  addRequest.onerror = ()=> {
    console.log("===数据写入失败===");
  }
};

export const getPageJsonById = (_id, resolve) => {
  console.log(`===我要获取[${_id}]数据====`);
  const getRequest = db.transaction(['templates'], 'readonly').objectStore('templates').get(_id);
  getRequest.onsuccess = (event) => {
    let pageJson = event.target.result;
    if (pageJson) {
      resolve(pageJson);
    } else {
      console.log(`===未获取[${_id}]数据===`);
      resolve({});
    }
  };
  getRequest.onerror = ()=> {
    console.log(`===获取数据失败[${_id}]===`);
    resolve({errMsg:`获取数据失败[${_id}]`});
  };
};
