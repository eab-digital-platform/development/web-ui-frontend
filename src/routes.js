import React from 'react';
import { Route, Switch } from 'react-router-dom';

import DisplayEngine from 'componentsByWebUI/displayEngine/DisplayEngine';
import PaymentForm from 'mock/PaymentForm'
// import SydTest from 'bak/SydTest';
// import FullScreenDialog from 'bak/FullScreenDialog';
// import DisplayEngineProvider from 'componentsByWebUI/displayEngine/DisplayEngineProvider';

const Routes = ({ displayJson }) => {
  return (
    <Switch>
      <Route path={'/'} exact render={() => (<DisplayEngine />)} />
      <Route path={'/cmsPreview'} exact render={() => (<DisplayEngine data={displayJson} isPreview={true} />)} />
      <Route path={'/:organizationId/:projectId'} exact render={({ match: { params } }) => (<DisplayEngine params={params} />)} />
      <Route path={'/payment'} exact component={ PaymentForm } />
      {/* <Route path={'/test'} exact component={ SydTest } /> */}
      {/* <Route path={'/page/:docId'} exact component={ DisplayEngine } /> */}
      {/* <Route component={ NotFound }/> */}
    </Switch>
  );
};

export default Routes;
