export default {
    // BACKEND_URL: "http://localhost:3001/api",
    // CAFE_API: "http://api-dev.eab-cafe.k8s-inno1",
    // CORE_API: "http://10.0.0.164:3500/core-api",
    // SEARCH_API: "http://10.0.0.164:3000/search-api",
    // BACKEND_URL: "http://web-ui-backend-dp.eab.openshift/api",
    BACKEND_URL_FOR_PROJECT: "http://cms-admin-backend.dp.k8s-dev",
    // 下面这个是cms api
    BACKEND_URL_FOR_PAGE: "http://cms-admin-backend.dp.k8s-dev/pageMaster/id",
    // 下面这个是旧的web-ui backend
    // BACKEND_URL_FOR_PAGE: "http://web-ui-backend-dp.eab.openshift/api/getJsonByDocId",
    // 下面这个是本地的web-ui backend
    // BACKEND_URL_FOR_PAGE: "http://10.0.0.123:3001/api/getJsonByDocId",
    
    CAFE_API: "http://api-dev.eab-cafe.k8s-inno1",
    CORE_API: "http://core-api.dp.k8s-dev/core-api",
    SEARCH_API: "http://search-api.dp.k8s-dev/search-api",
    POS_API: "http://core-api.dp.k8s-dev/pos-api",
    MQREADFROMMESSAGE: "Y",
    AXIOS_TIME_OUT: 5000,
    MQ: {
        CREATE: "LOL15",
        EDIT: "LOL16"
    },
    DEBUG: true
};