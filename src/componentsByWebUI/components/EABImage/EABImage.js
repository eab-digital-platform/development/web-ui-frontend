import React from 'react';
import util from "utils/util";

function EABImage(props) {
  // console.log("这里是创建image的, props : ", props);
  // const cxt = useContext(WebUIContext);
  const { cssClass, dataObj, cmsProps = {}, style } = props;
  const { alt, src, actionUrl, params = [] } = cmsProps;
  let request = null;
  if (actionUrl) {
    request = util.resolveParams(dataObj, actionUrl, params);
  }

  return (
    <React.Fragment>
      {
        request && request.dataUrl
          ?
          <a href={request.dataUrl} target="_blank" rel="noopener noreferrer">
            <img className={cssClass} style={style} src={src} alt={alt} />
          </a>
          :
          <img className={cssClass} style={style} src={src} alt={alt} />
      }
    </React.Fragment>

  );
}

export default EABImage;