import React, { useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import WebUIContext from 'WebUIContext';
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  }
}));

export default function Carousell(props) {
  const classes = useStyles();

  const cxt = useContext(WebUIContext);
  const { style, cmsProps = {}, dataObj, items } = props;
  const { dataUrl } = cmsProps;
  let data = generateWebUIData(dataUrl, cxt, dataObj, {});

  const [activeStep, setActiveStep] = useState(0);
  const maxSteps = items.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <div className={classes.root}>
      <SwipeableViews
        axis="x"
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
        containerStyle={{ height: "calc(100vh - 112px)" }}
      >
        {
          items.map((item, inx) => {
            return <React.Fragment key={inx}>{generateWebUI(item.cmsComponentType, { ...item, dataObj: data })}</React.Fragment>
          })
        }

      </SwipeableViews>
      <MobileStepper
        steps={maxSteps}
        position="static"
        variant="dots"
        activeStep={activeStep}
        nextButton={
          <Button size="small" onClick={handleNext} disabled={activeStep === maxSteps - 1} style={{ position: "absolute", top: "calc(50% - 50px)", right: "0px" }}>
            <KeyboardArrowRight style={{
              color:"rgba(0, 0, 0, 0.26)",
              width: "100px",
              height: "100px"
            }} />
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0} style={{ position: "absolute", top: "calc(50% - 50px)", left: "0px" }}>
            <KeyboardArrowLeft style={{
              color:"rgba(0, 0, 0, 0.26)",
              width: "100px",
              height: "100px"
            }}/>
          </Button>
        }
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center"
        }}
      />
    </div>
  );
}
