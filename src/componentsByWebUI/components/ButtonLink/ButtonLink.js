import React, { useContext } from 'react';
import WebUIContext from 'WebUIContext';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconsUtils from 'componentsByWebUI/displayEngine/IconsUtils';
import lookup from 'utils/lookup';
import _ from 'lodash';

export default function ButtonLink(props) {
  console.log("===你好呀, 这里是打印出来buttonLink的props===>", props);
  const { cssClass, dataObj, cmsProps = {}, muiProps, style } = props;
  const { iconName, displayText, anchorName } = cmsProps;

  const cxt = useContext(WebUIContext);
  const { localization, mapping } = cxt;

  // 这里处理displayText从dataObj里面动态获取
  let listItemText = "";
  if (displayText.indexOf("#") >= 0) {
    listItemText = lookup.getValueFromData(dataObj, displayText, localization, mapping)
  } else {
    listItemText = displayText;
  }

  //定义滚动到锚点的方法
  const scrollToAnchor = ({ anchorName }) => {
    if (anchorName) {
      // 找到锚点
      let anchorElement = document.getElementById(anchorName);
      // 如果对应id的锚点存在，就跳转到锚点
      if (anchorElement) { anchorElement.scrollIntoView({ block: 'start', behavior: 'smooth' }); }
    }
  }


  //定义list的onclick方法, 目前默认为null
  let handleOnClick = () => { };
  let _anchorName = "";
  if (anchorName) {
    handleOnClick = scrollToAnchor;
    _anchorName = _.get(dataObj, anchorName.substr(1), "");
  }

  return (
    <ListItem {...muiProps} button className={cssClass} style={style} onClick={() => handleOnClick({ anchorName: _anchorName })}>
      {iconName && <ListItemIcon>{IconsUtils[iconName]}</ListItemIcon>}
      <ListItemText primary={listItemText} />
    </ListItem>
  )
}
