import React, { useState, useEffect } from 'react'
import util from "utils/util";
// import { PDFReader } from 'react-read-pdf'; //这个渲染的是图片来着, 并不是真正的pdf显示
// import PDF from 'react-pdf-js';

export default function PDFPreview(props) {
  // console.log("====这里是创建一个显示pdf的object====", props);

  const { cmsProps = {}, dataObj, style } = props;
  const { dataUrl, params } = cmsProps;

  const [pdfUrl, setPdfUrl] = useState(null);

  const combinePdfUrl = async () => {
    if (params && params.length > 0) {
      const rquest = await util.resolveParams(dataObj, dataUrl, params);
      setPdfUrl(rquest.dataUrl);
    }
  }

  useEffect(() => {
    combinePdfUrl();
  }, []);

  // console.log("===这里查看pdfUrl==>>>", pdfUrl);

  return (
    <React.Fragment>
      {
        pdfUrl && <object data={pdfUrl} type="application/pdf" style={style} aria-label="" />
      }
      {/* <PDFReader showAllPage={true} url={"http://minio.dp.k8s-dev/pdfs/loadSignedPDF.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=minioadmin%2F20200311%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200311T092039Z&X-Amz-Expires=3000&X-Amz-SignedHeaders=host&X-Amz-Signature=b358adfb4c1918a0b7332156e65d262aaed8846753e87935b856f19ddd01b372"} /> */}
      {
        // <PDF file={"https://www.w3docs.com/uploads/media/default/0001/01/540cb75550adf33f281f29132dddd14fded85bfc.pdf"} />
      }
    </React.Fragment>
  )
}
