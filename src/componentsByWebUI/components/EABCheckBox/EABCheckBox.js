import React, { useEffect, useState } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import _ from 'lodash';

export default function EABCheckBox(props) {
  // console.log("====>这里是创建EABCheckBox, props: ", props);
  const { dataObj, muiProps, cmsProps = {}, style, actionFunctions } = props;
  const { actionUrl, dataUrl } = cmsProps;
  const [checked, setChecked] = useState(false);
  
  useEffect(() => {
    if (actionFunctions && actionUrl && actionFunctions[actionUrl]) {
      actionFunctions[actionUrl]({
        checked,
        value: _.get(dataObj, dataUrl.substr(1), undefined)
      });
    }
  }, [checked])

  return (
    <Checkbox
      checked={checked}
      onChange={() => {
        setChecked(!checked)
      }}
      style={style}
      {...muiProps}
    />
  );
}
