import React, {useState, useEffect} from 'react';
import TextField from '@material-ui/core/TextField';
import _ from 'lodash';

export default function Input(props) {
  console.log("====这里创建Input组件===", props);
  const { cssClass, dataObj, muiProps, cmsProps = {}, style, actionFunctions } = props;
  const { name, onChange} = cmsProps;
  const [inputText, setInputText] = useState("");

  // useEffect(() => {
  //   if (typeof dataObj[name] === "string") {
  //     if (!_.isEqual(dataObj[name], inputText)) {
  //       setInputText(dataObj[name]);
  //     }
  //   }
  // }, [dataObj]);

  if (muiProps.placeholder && muiProps.placeholder.indexOf("#") === 0) {
    muiProps.placeholder = _.get(dataObj, muiProps.placeholder.substr(1), muiProps.placeholder);
  }

  return (
    <TextField
      className={cssClass}
      {...muiProps}
      style={style}
      value={inputText}
      onChange={event => {
        if (onChange && actionFunctions[onChange]) {
          actionFunctions[onChange](event.target.value);
        }
        setInputText(event.target.value);
      }}
    />
  );
}