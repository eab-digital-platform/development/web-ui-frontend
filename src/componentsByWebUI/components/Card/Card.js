import React, { useContext } from 'react'
import Card from '@material-ui/core/Card';
import WebUIContext from 'WebUIContext'
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider'

export default function EABCard(props) {
  const { muiProps, dataObj, cmsProps={}, style, items, actionFunctions } = props;
  const { dataUrl } = cmsProps;
  const cxt = useContext(WebUIContext);
  let data = generateWebUIData(dataUrl, cxt, dataObj, {});
  
  return (
    <React.Fragment>
      <Card {...muiProps} style={style}>
        {items.map((item) => {
          return generateWebUI(item.cmsComponentType, { ...item, dataObj:data, actionFunctions})
        })}
      </Card>
    </React.Fragment>
  )
}
