import React, { useEffect, useRef, useState } from 'react';
import Loading from '../Loading/Loading';
import keys from 'lodash/keys';
import join from 'lodash/join';
import get from 'lodash/get';
import util from "utils/util";
import axios from "axios";

export default function IFrame(props) {
  const { cssClass, dataObj, muiProps, cmsProps = {}, style, actionFunctions } = props;
  const { frameborder, marginheight, marginwidth, name, scrolling, src, preHandle, callbackFunction } = cmsProps;
  console.log("=====开始加载iframe啦=====");
  // 定义loading ref
  const loadingRef = useRef(null);
  // 定义调用loading显示隐藏的方法
  const showLoading = () => {
    loadingRef.current.showBackdrop();
  };
  const hideLoading = () => {
    loadingRef.current.hideBackdrop();
  };

  useEffect(() => {
    showLoading();
    handleIframeSrc();
  }, [])

  // 监听iframe传递过来的数据
  const [statusFromIframe, setStatusFromIframe] = useState(null);
  useEffect(() => {
    const receiveMessageFromIndex = (event) => {
      if (event !== undefined) {
        // console.log( '我是react,我接受到了来自iframe的模型ID：', event.data );
        setStatusFromIframe(event.data);
      }
    }

    //监听message事件
    window.addEventListener("message", receiveMessageFromIndex, false);
    return () => {
      window.removeEventListener("message", receiveMessageFromIndex, false)
      // window.addEventListener("message", receiveMessageFromIndex, false);
    }
  }, []);

  //这里获得iframe传递过来的数据之后, 关闭dialog
  if (statusFromIframe) {
    console.log("=====你好呀, 我这里获得了iframe传递过来的数据哦===>", statusFromIframe);
    console.log("=====你好呀, ===>", actionFunctions);
    callbackFunction.forEach(funcName => {
      const handleCallbackFunction = get(actionFunctions, funcName, () => { console.log("==这个function不能在actionFunctions找到, 请检查代码=="); });
      handleCallbackFunction();
    });
    // actionFunctions.proposalsAddQuote_standard.closeDialog();
    // actionFunctions.handleSearch();
  }

  // 定义传递给iframe的参数
  const [iframeSrc, SetIframeSrc] = useState(null);

  const preHandleFunction = async (preHandle) => {
    const { api, type, params } = preHandle;
    console.log("===========iframe===========", dataObj);
    const request = util.resolveParams(dataObj, api, params);
    console.log("===========request===========", request);
    console.log("===========type===========", type);
    return await util.fetchData(request.dataUrl, request.params, type);
  }

  //the quotation api = /:iCid/:pCid/:productId/:ccy
  // const quotationParamKeys = ["iCid","pCid","productId","ccy"];
  const quotationParamKeys = {
    iCid: "#cid",
    pCid: "#cid",
    productId: "08_product_SAV_4",
    ccy: "SGD"
  };
  // let productId = '08_product_SAV_4';
  // let iCid = 'CP001008-00199';
  // let pCid = 'CP001008-00199';
  // let ccy = 'SGD';


  // 定义preHandle方法
  const handleIframeSrc = async () => {
    if (preHandle) {
      //调用preHandle定义的ajax
      const returnObj = await preHandleFunction(preHandle);
      console.log("======这里是调用iframe定义的preHandle api 返回数据::", returnObj);
      //根据返回的Obj构造iframe src
      const param = join(keys(quotationParamKeys).map(
        key =>
          (quotationParamKeys[key] && quotationParamKeys[key].indexOf("#") >= 0)
            ?
            get(returnObj, quotationParamKeys[key].substr(1), "")
            :
            quotationParamKeys[key]
      ), '/');
      console.log("======这里打印出iframe的src======", (src + "/" + param));

      SetIframeSrc(src + "/" + param);
    } else {
      SetIframeSrc(src);
    }
  };

  return (
    <React.Fragment>
      {iframeSrc && <iframe
        frameborder={frameborder}
        marginheight={marginheight}
        marginwidth={marginwidth}
        name={name}
        scrolling={scrolling}
        src={iframeSrc}
        style={style} onLoad={() => { hideLoading(); }} />}
      <Loading ref={loadingRef} />
    </React.Fragment>
  )
}
