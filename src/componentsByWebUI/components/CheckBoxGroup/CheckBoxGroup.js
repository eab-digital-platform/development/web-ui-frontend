import React, { useState, useEffect } from 'react';
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider'
import _ from 'lodash';

export default function CheckBoxGroup(props) {
  const { dataObj, cmsProps={}, style, items, actionFunctions } = props;
  const { dataUrl, actionUrl } = cmsProps;
  const [values, setValues] = useState([]);
  
  useEffect(() => {
    if (actionUrl && actionFunctions[actionUrl]) {
      actionFunctions[actionUrl]({key: _.get(dataObj, dataUrl.substr(1), []), value: values});
    }
  }, [values]);

  const handleCheckBoxGroup = ({checked, value}) => {
    if (value !== undefined) {
      const data = _.cloneDeep(values);
      const index = data.indexOf(value);
      if (index !== -1 && !checked) {
        data.splice(index, 1);
      } else if (checked) {
        data.push(value);
      }
      setValues(data);
    }
  }

  return (
    <div style={style}>
      {items.map((item) => {
        return generateWebUI(item.cmsComponentType, { ...item, dataObj: {...dataObj}, actionFunctions: {...actionFunctions, handleCheckBoxGroup}})
      })}
    </div>
  )
}
