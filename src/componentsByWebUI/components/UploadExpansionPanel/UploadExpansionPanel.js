import React, { useState } from 'react';
import { Grid } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: "100%",
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

function UploadExpansionPanel(props) {
  const classes = useStyles();
  console.log("这里是创建UploadExpansionPanel的, props : ", props);
  // const { key, dataObj, cmsProps = {}, muiProps, style, items, actionFunctions } = props;
  // const { defaultExpanded, iconName = "ExpandMore", uploadLabel, uploadIconName, uploadUrl, uploadParams } = cmsProps;

  const [showCategorySetion, setShowCategorySetion] = useState(false);
  return (
    <Grid container spacing={2} style={{ marginTop: "8px", marginBottom:"8px" }}>
      {showCategorySetion && <Grid item xs={12}>
        <Paper component="form" className={classes.root}>
          <InputBase
            className={classes.input}
            placeholder="Create a new category"
          />
          <Divider className={classes.divider} orientation="vertical" />
          <IconButton color="primary" className={classes.iconButton} aria-label="directions">
            <SaveIcon />
          </IconButton>
        </Paper>
      </Grid>
      }
      <Grid item xs={12}>
        <Button variant="contained" color="primary" fullWidth={true} onClick={() => setShowCategorySetion(!showCategorySetion)}>
          Create a new category
      </Button>
      </Grid>
    </Grid>
  );
}

export default UploadExpansionPanel;

