import React, { useContext, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';
import WebUIContext from 'WebUIContext';
// import Loading from 'componentsByWebUI/components/Loading/Loading';

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
  },
  content: {
    flex: 1
  },
  padding: {
    padding: "24px",
  }
}));

function LeftMenu(props) {
  console.log("===LeftMenu props:===", props);
  const classes = useStyles();
  const { style, dataObj, cmsProps = {}, items } = props;
  const { dataUrl } = cmsProps;

  // console.log("这里是进入到LeftMenu.js, 页面items =>", items);
  const cxt = useContext(WebUIContext);
  const { leftMenuOpened, contextFunction } = cxt;
  //从useContext中获得数据, 传递给所有的
  const [data, setData] = useState(null);
  // console.log("这里是进入到LeftMenu.js, 页面data =>", data);
  const fetchPageData = useRef(async () => {
    const pageData = await generateWebUIData(dataUrl, cxt, dataObj, {});
    setData(pageData ? pageData : {});
  });
  // 页面加载完成后会执行这里拿数据
  useEffect(() => {
    fetchPageData.current();
  }, [])

  return (
    <Drawer open={leftMenuOpened} onClose={contextFunction["toggleDrawer"]}>
      <div className={classes.root} style={style}>
        {
          data && items.map((item, i) => {
            return generateWebUI(item.cmsComponentType, { ...item, dataObj: data });
          })
        }
      </div>
    </Drawer>
  )
}

LeftMenu.propTypes = {
  width: PropTypes.string,
  headerJson: PropTypes.object,
  contentJson: PropTypes.object,
  footerJson: PropTypes.object,
}

export default LeftMenu

