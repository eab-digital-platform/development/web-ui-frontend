import React, { useContext } from 'react'
import { Grid } from '@material-ui/core'
import WebUIContext from 'WebUIContext'
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider'

export default function GridRow(props) {
  // console.log("这里是传进来GridRowItems的pageJsonObj: ", props);
  //解构
  const { muiProps, cmsProps = {}, dataObj, items, style, actionFunctions, dialogName } = props;
  const { dataUrl } = cmsProps;

  const cxt = useContext(WebUIContext);
  let data = generateWebUIData(dataUrl, cxt, dataObj, {});

  return (
    <Grid container {...muiProps} style={style}>
      {items.map((item) => {
        return generateWebUI(item.cmsComponentType, { ...item, dialogName, dataObj: data, actionFunctions })
      })}
    </Grid>
  )
}

