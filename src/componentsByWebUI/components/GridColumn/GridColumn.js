import React, { useContext } from 'react'
import { Grid } from '@material-ui/core'
import WebUIContext from 'WebUIContext'
import { makeStyles } from '@material-ui/core/styles';
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider'


const useStyles = makeStyles(theme => ({
  root: { height: "100%", width: "100%" }
}));

export default function GridColumn(props) {
  const classes = useStyles();
  // console.log("这里是传进来GridColumnItem的props: ", props);
  //解构
  const { muiProps, dataObj, cmsProps = {}, style, items, actionFunctions, dialogName } = props;
  const { dataUrl, align } = cmsProps;

  let columnJustify = "";
  if (align === "left") {
    columnJustify = "flex-start";
  } else if (align === "right") {
    columnJustify = "flex-end";
  } else if (align === "center") {
    columnJustify = "center";
  }

  const cxt = useContext(WebUIContext);
  let data = generateWebUIData(dataUrl, cxt, dataObj, {});

  return (
    <Grid
      item
      {...muiProps}
      style={style}
    >
      <Grid container direction="row" justify={columnJustify} alignItems="center" className={classes.root}>
        {items.map((item, i) => {
          return generateWebUI(item.cmsComponentType, { ...item, dialogName, dataObj: data, actionFunctions })
        })}
      </Grid>
    </Grid>
  )
}


