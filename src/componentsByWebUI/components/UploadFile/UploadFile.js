import React, { useEffect, useState } from 'react';
import axios from "axios";
import _ from 'lodash';
import { useDropzone } from 'react-dropzone';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Fab from '@material-ui/core/Fab';
import { makeStyles } from '@material-ui/core/styles';
import IconsUtils from 'componentsByWebUI/displayEngine/IconsUtils';
import util from "utils/util";
import './UploadFile.css';

const useStyles = makeStyles({
  media: {
    height: 140,
  },
  thumbsGrid: {
    position: "relative"
  },
  cancel: {
    position: "absolute",
    top: 0,
    right: 0,
  }
});

export default function UploadFile(props) {
  const { key, dataObj, cmsProps = {} } = props;
  const { label, iconName, accept, multiple, maxSize,
    uploadedListUrl, uploadedListParams, uploadedSingleUrl, uploadedSingleParams,
    uploadUrl, uploadParams, deleteUrl, deleteParams } = cmsProps;

  console.log("====>这里传递过来的是upload数据: ", dataObj);
  console.log("====>这里传递过来的是uploadUrl数据: ", uploadUrl);
  console.log("====>这里传递过来的是uploadParams数据: ", uploadParams);

  const config = {
    headers: { "Content-Type": "multipart/form-data;boundary=" + new Date().getTime() }
  };

  const classes = useStyles();
  const { getRootProps, getInputProps } = useDropzone({
    accept: accept,
    multiple: multiple,
    maxSize: maxSize,
    noKeyboard: true,
    onDropAccepted: acceptedFiles => {
      acceptedFiles.forEach(file => {
        axios.post(uploadUrl, provideFormData(file), config)
          .then(function (response) {
            console.log("==========这里是上传成功之后的返回::", response);
            setCurrRefreshCnt(cnt => cnt + 1)
          })
          .catch(function (error) {
            console.log("==========这里是上传失败之后的返回::", error);
          });
      })
    },
    onDropRejected: rejectedFiles => {
      console.log("这里打印出来的是什么呢?? ", rejectedFiles);
    }
  });

  // 上传文件, 准备formData数据
  const provideFormData = (file) => {
    console.log("====这里是file=>>>>>", file);
    let formData = new FormData();
    formData.append("uploadFile", file, file.name);
    uploadParams.forEach(item => {
      formData.append(item.name, _.get(dataObj, item.value.substr(1), ""));
    });
    return formData;
  }
  // 这里是拿到已经上传了的文件list
  const [files, setFiles] = useState([]);
  const getUploadedFiles = async () => {
    const rquest = util.resolveParams(dataObj, uploadedListUrl, uploadedListParams);
    const uploadedFiles = await util.fetchData(rquest.dataUrl, rquest.params);
    console.log("这里能打印出来拿到上传的文件的list数据: ", uploadedFiles);
    setFiles(uploadedFiles);
  }
  // 这里构造每个文件的url
  const handleUploadedSingleUrl = (dataObj) => {
    const rquest = util.resolveParams(dataObj, uploadedSingleUrl, uploadedSingleParams);
    return rquest.dataUrl;
  }
  // 这里构造删除每个文件的api url
  const removeUploadedFile = async (file) => {
    const rquest = util.resolveParams(file, deleteUrl, deleteParams);
    const removeRes = await util.fetchData(rquest.dataUrl, rquest.params, "post");
    console.log("这里看一下删除已经上传文件返回的什么: ", removeRes);
    if (removeRes === "Removed!") {
      setCurrRefreshCnt(cnt => cnt + 1)
    }
  }

  // 这里将在上传成功之后, 或者删除成功之后更新页面显示
  const [currRefreshCnt, setCurrRefreshCnt] = useState(0);
  useEffect(() => {
    getUploadedFiles();
  }, [currRefreshCnt])

  return (
    <section key={key} className="container">
      <div {...getRootProps({ className: 'dropzone' })}>
        <input {...getInputProps()} />
        <Grid container spacing={2}>
          {/* 左边上传的文字提示 */}
          <Grid container
            direction="row"
            justify="flex-start"
            alignItems="center"
            item xs={6}>
            {label}
          </Grid>
          {/* 右边上传的图标 */}
          <Grid container
            direction="row"
            justify="flex-end"
            alignItems="center"
            item xs={6}>
            {IconsUtils[iconName]}
          </Grid>
        </Grid>
      </div>
      <Grid container spacing={2} style={{ marginTop: "10px" }}>
        <Grid container item justify="flex-start" xs={12}>Uploaded {files ? files.length : 0} files</Grid>
        {
          files && files.map((file, i) => {
            let fileUrl = handleUploadedSingleUrl(file);
            return (
              <Grid key={i} item xs={4} className={classes.thumbsGrid}>
                <Card>
                  <CardMedia
                    className={classes.media}
                    image={fileUrl}
                  />
                </Card>
                <Fab size="small" color="secondary" className={classes.cancel} onClick={() => removeUploadedFile(file)}>
                  {IconsUtils["Close"]}
                </Fab>
              </Grid>
            )
          })
        }
      </Grid>
    </section>
  );
}