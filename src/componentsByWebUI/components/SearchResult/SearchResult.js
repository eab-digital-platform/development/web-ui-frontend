import React, { useState } from 'react';
import { Grid } from '@material-ui/core';
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';
import _ from 'lodash';

export default function SearchResult(props) {
  console.log("===这里创建的是SearchResult===>", props);
  const { cmsProps = {}, style, items, dataObj, actionFunctions, dialogName } = props;
  const { align, xs, actionUrl, fullScreen, dataUrl, isGrid, anchorName } = cmsProps;
  const [currentData, setCurrentData] = useState(null);
  const [dialogOpened, setDialogOpened] = useState(false);

  const closeDialog = () => {
    setDialogOpened(false);
  }

  const handleClick = (data) => {
    if (actionUrl) {
      setCurrentData(data);
      setDialogOpened(true);
    }
  }

  let result = dataUrl ? _.get(dataObj, dataUrl, []) : dataObj;
  console.log("===这里是结果集显示的result data => ", result);

  let columnJustify = "";
  if (align === "left") {
    columnJustify = "flex-start";
  } else if (align === "right") {
    columnJustify = "flex-end";
  } else if (align === "center") {
    columnJustify = "center";
  }

  // 这里设置锚点位置div
  let anchorId = "";
  if (anchorName) {
    anchorId = _.get(dataObj, anchorName.substr(1), "");
  }

  return (
    <Grid container style={style} direction="row" justify={columnJustify} alignItems={isGrid ? "space-around" : "center"}>
      {anchorId && <div id={anchorId} />}
      {
        isGrid ?
          <React.Fragment>
            {Array.isArray(result) && result.map((data, inx) => {
              return items.map((item) => {
                return generateWebUI(item.cmsComponentType, { ...item, dialogName, dataObj: data, actionFunctions: { ...actionFunctions, setCurrentData } })
              })
            })}
          </React.Fragment>
          :
          <Grid item xs={xs}>
            {Array.isArray(result) && result.map((data, inx) => {
              return (
                <div key={inx} onClick={() => { handleClick(data) }} style={{ cursor: actionUrl ? "pointer" : "default" }}>
                  {items.map((item) => {
                    return generateWebUI(item.cmsComponentType, { ...item, dialogName, dataObj: data, actionFunctions: { ...actionFunctions, setCurrentData } })
                  })}
                </div>
              );
            })}
          </Grid>
      }
      {dialogOpened && generateWebUI("EABDialog", { cmsProps: { actionUrl, fullScreen }, dialogOpened, closeDialog, dataObj: currentData, actionFunctions })}
    </Grid>
  )
}
