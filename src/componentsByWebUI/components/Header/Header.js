import React, { useContext } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider'
import WebUIContext from 'WebUIContext'

function Header(props) {
  const cxt = useContext(WebUIContext);
  //appBarPosition: PropTypes.oneOf(['absolute', 'fixed']), //fixed - 页面滚动的时候, appBar始终显示在页头
  // const functions = { ...cxt.contextFunction, ...props.functions };
  const { muiProps, style, cmsProps={}, dataObj, items, callbackFunctions, actionFunctions } = props;
  const { dataUrl } = cmsProps;
  // console.log("cssClass:", cssClass);
  // console.log("style:", style);
  // console.log("cmsProps:", cmsProps);
  // console.log("appBarPosition:", appBarPosition);

  let data = generateWebUIData(dataUrl, cxt, dataObj, {});

  return (
    <React.Fragment>
      <AppBar {...muiProps} style={style}>
        <Toolbar>
          {
            items.map((item) => {
              return generateWebUI(item.cmsComponentType, { ...item, dataObj:data, callbackFunctions, actionFunctions })
            })
          }
        </Toolbar>
      </AppBar>
      <Toolbar style={style} />
    </React.Fragment>
  )
}

export default Header