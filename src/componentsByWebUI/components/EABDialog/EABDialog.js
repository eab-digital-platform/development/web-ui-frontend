import React, { useEffect, useState, useContext } from 'react';
import Dialog from '@material-ui/core/Dialog';
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider'
import util from "utils/util";
import WebUIContext from 'WebUIContext';

export default function EABDialog(props) {
  console.log("===你好呀, 我这里是创建了一个dialog: ", props);
  const { dialogOpened, closeDialog, cmsProps = {}, dataObj, actionFunctions = {} } = props;
  // 下面两个字段是从IconButton传递过来的, 打开dialog一定会有一个button触发
  const { actionUrl, fullScreen, fullWidth = false, maxWidth = false } = cmsProps;

  const cxt = useContext(WebUIContext);
  const { showLoading, hideLoading } = cxt.contextFunction;

  // 这里定义每个dialog自己所有的属性
  const [currFullScreen, setCurrFullScreen] = useState(false);//是否全屏显示
  const [currMaxWidth, setCurrMaxWidth] = useState("lg");//dialog非全屏状态下的宽度
  const [prevActionUrl, setPrevActionUrl] = useState(null);//dialog上一次显示的url
  const [currActionUrl, setCurrActionUrl] = useState(null);//目前dialog显示的url
  const [dialogContentConfig, setDialogContentConfig] = useState(null);//目前dialog显示页面的json配置
  const [actionUrls, setActionUrls] = useState([]);//这里保存的是这个dialog跳转的action list
  const [extraData, setExtraData] = useState(null);

  const fetchPageData = async (pageId) => {
    showLoading();
    const configRes = await util.fetchPageJsonData(pageId);
    setDialogContentConfig(configRes);
    hideLoading();
  };

  // 页面加载完成后会执行这里拿数据
  useEffect(() => {
    if (fullWidth && maxWidth && maxWidth !== currMaxWidth) {
      setCurrMaxWidth(maxWidth);
    }
    fetchPageData(actionUrl);
    setCurrActionUrl(actionUrl);
    setPrevActionUrl(actionUrl);
    setCurrFullScreen(fullScreen);
    // console.log("=====>>这里是设置保存actionUrls开始>>=======", actionUrls);
    setActionUrls([...actionUrls, actionUrl]);
    // console.log("=====>>这里是设置保存actionUrls结束>>=======", actionUrls);
  }, []);

  // console.log("你好呀, 这里打印出这个dialog将要显示的json", dialogContentConfig);
  // console.log("你好呀, 这里打印的是dialog的currActionUrl", currActionUrl);
  // console.log("你好呀, 这里打印的是dialog的prevActionUrl", prevActionUrl);

  // 这个effect是dialog里面页面跳转使用的
  useEffect(() => {
    if (prevActionUrl && currActionUrl && currActionUrl !== prevActionUrl) {
      // console.log("这里将重新渲染dialog, 根据当前的action, 也就是dialog显示的pageId", currActionUrl);
      // 先清空这个dialog的dialogContentConfig数据
      setDialogContentConfig(null)
      fetchPageData(currActionUrl);
      setPrevActionUrl(currActionUrl);
      // console.log("=====>>这里是设置保存actionUrls开始>>=======", actionUrls);
      setActionUrls([...actionUrls, currActionUrl]);
      // console.log("=====>>这里是设置保存actionUrls结束>>=======", actionUrls);
    }
  }, [prevActionUrl, currActionUrl]);

  //handle actionFunctions - [S]
  // console.log("===这里是dialog中重建functions, 里面有两个方法, 一个是关闭dialog, 一个是callback, 这个callback理论上专门提供给cafe form使用");
  // actionFunctions是传进来的, 这个actionFunctions对象应该贯穿整个dialog的生命周期, 怎么样贯穿整个生命周期呢? 不停的传递到下层组件?
  // 这里按照actionUrl, 也就是dialog将要展示的json id创建一个对象, 默认将添加当前的closeDialog方法
  const [radioCardSelectedObj, setRadioCardSelectedObj] = useState({});
  if (currActionUrl) {
    actionFunctions[`${currActionUrl}`] = { closeDialog: closeDialog };
    // for RadioCard event
    console.log("======>>>>>>请打印这里的radioCardSelectedObj对象出来===========", radioCardSelectedObj);
    actionFunctions.radioCardSelectedObj = radioCardSelectedObj;
    const handleRadioCardSelect = (name, selectedObj) => {
      console.log(`你好呀, 这里是${currActionUrl}执行选择radio事件====`, selectedObj);
      setRadioCardSelectedObj(preObj => {
        preObj[name] = selectedObj;
        return { ...preObj };
      });
      console.log("你好呀, 这里是执行选择radio事件====", actionFunctions);
    };
    actionFunctions.handleRadioCardSelect = handleRadioCardSelect;

    // 这里是dialog切换到另外一个页面function
    const handleDialogRedirect = (callbackCmsProps) => {
      if (callbackCmsProps && callbackCmsProps.actionUrl ) {
        setCurrActionUrl(callbackCmsProps.actionUrl);
      }
      if (callbackCmsProps && callbackCmsProps.fullWidth && callbackCmsProps.maxWidth) {
        console.log("===你好呀, 我这里设置dialog的宽度:::===>" , callbackCmsProps.maxWidth);
        setCurrMaxWidth(callbackCmsProps.maxWidth)
      }
      if (callbackCmsProps && callbackCmsProps.fullScreen) {
        setCurrFullScreen(callbackCmsProps.fullScreen)
      }
    };
    actionFunctions[`${currActionUrl}`].handleDialogRedirect = handleDialogRedirect;

    // 这里是dialog切换到另外一个页面function
    const handleDialogGoNext = (callbackCmsProps) => {
      if (callbackCmsProps && callbackCmsProps.radioCardSelectedName && radioCardSelectedObj && radioCardSelectedObj[callbackCmsProps.radioCardSelectedName].actionUrl) {
        setCurrActionUrl(radioCardSelectedObj[callbackCmsProps.radioCardSelectedName].actionUrl);
      }
      if (callbackCmsProps && callbackCmsProps.fullWidth && callbackCmsProps.maxWidth && callbackCmsProps.maxWidth !== currMaxWidth) {
        setCurrMaxWidth(callbackCmsProps.maxWidth)
      }
      if (callbackCmsProps && callbackCmsProps.fullScreen) {
        setCurrFullScreen(callbackCmsProps.fullScreen)
      }
    };
    actionFunctions[`${currActionUrl}`].handleDialogGoNext = handleDialogGoNext;

    // 这里是dialog切换了actionUrl之后, 再回到上一个actionUrl
    const handleDialogGoPrevious = (callbackCmsProps) => {
      if (actionUrls && actionUrls.length >= 2) {
        let previousActionUrl = actionUrls[actionUrls.length - 2];
        setActionUrls([...actionUrls.slice(0, actionUrls.length - 2)]);
        setCurrActionUrl(previousActionUrl);
      }
    };
    actionFunctions[`${currActionUrl}`].handleDialogGoPrevious = handleDialogGoPrevious;

    // 这里是eapp->add选择了quote之后的confirm button
    const handleEappSelectedQuoteConfirmCallback = (callbackCmsProps, callbackObj) => {
      // console.log("这里打印一下传递过来的数据是什么样子的:",callbackCmsProps, callbackObj);
      if (callbackObj) {
        setExtraData(callbackObj);
      }
      if (callbackCmsProps && callbackCmsProps.radioCardSelectedName && radioCardSelectedObj && radioCardSelectedObj[callbackCmsProps.radioCardSelectedName].actionUrl) {
        setCurrActionUrl(radioCardSelectedObj[callbackCmsProps.radioCardSelectedName].actionUrl);
      }
      if (callbackCmsProps && callbackCmsProps.fullWidth && callbackCmsProps.maxWidth && callbackCmsProps.maxWidth !== currMaxWidth) {
        setCurrMaxWidth(callbackCmsProps.maxWidth)
      }
      if (callbackCmsProps && callbackCmsProps.fullScreen) {
        setCurrFullScreen(callbackCmsProps.fullScreen)
      }
    };
    actionFunctions[`${currActionUrl}`].handleEappSelectedQuoteConfirmCallback = handleEappSelectedQuoteConfirmCallback;

    // 这里是client->add之后, 跳转到client summary page
    const handleClientAddCallback = (callbackCmsProps, callbackObj) => {
      // console.log("这里打印一下传递过来的数据是什么样子的:",callbackCmsProps, callbackObj);
      if (callbackObj && callbackObj.success) {
        setExtraData({ _id: callbackObj.MQ_docID });
      }
      if (callbackCmsProps && callbackCmsProps.actionUrl) {
        setCurrActionUrl(callbackCmsProps.actionUrl);
      }
      if (callbackCmsProps && callbackCmsProps.fullWidth && callbackCmsProps.maxWidth && callbackCmsProps.maxWidth !== currMaxWidth) {
        setCurrMaxWidth(callbackCmsProps.maxWidth)
      }
      if (callbackCmsProps && callbackCmsProps.fullScreen) {
        setCurrFullScreen(callbackCmsProps.fullScreen)
      }
    };
    actionFunctions[`${currActionUrl}`].handleClientAddCallback = handleClientAddCallback;
  }
  //handle actionFunctions - [E]

  console.log("==这里打印出dialog构建的actionFunctions==", actionFunctions);

  return (
    <Dialog
      fullScreen={currFullScreen}
      open={dialogOpened}
      onClose={closeDialog}
      disableBackdropClick
      fullWidth={fullWidth}
      maxWidth={currMaxWidth}
    >
      {
        dialogContentConfig && currActionUrl && generateWebUI(
          dialogContentConfig.cmsComponentType,
          {
            ...dialogContentConfig,
            dialogName: currActionUrl,
            dataObj:
            {
              ...dataObj,
              ...actionFunctions,
              ...extraData
            },
            actionFunctions
          }
        )
      }
    </Dialog>
  );
}
