import React from "react";
import _ from 'lodash';
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';

const getSelectedValueFromData = (dataObj, key) => {
  if (key) {
    if (key.indexOf("#") === 0) {
      if (key === "#ALLDATA") {
        return dataObj; 
      } else {
        return dataObj ? _.get(dataObj, key.substr(1), "") : key;
      }
    } else {
      return key;
    }
  } else {
    return "";
  }
}

export default function RadioCard(props) {
  console.log("=====这里是创建radioCard的props :", props);
  const { actionFunctions, cmsProps = {}, dataObj, style, items, dialogName } = props;
  //原本这里有一个name, 目前不需要
  const { radioCardStyle, radioCardActiveStyle, name, value, actionUrl } = cmsProps;
  const { radioCardSelectedObj, handleRadioCardSelect } = actionFunctions;

  // console.log("radioCardActiveStyle:", radioCardActiveStyle);
  // console.log("radioCardStyle:", radioCardStyle);
  // console.log("value:", value);
  // console.log("actionUrl:", actionUrl);
  console.log("radioCardSelectedObj:", radioCardSelectedObj);

  const selectedValue = getSelectedValueFromData(dataObj, value);

  // #7E58A9
  return (
    <Card style={style} onClick={() => handleRadioCardSelect(name, { selectedValue, actionUrl })}>
      <CardActionArea style={radioCardSelectedObj[name] && radioCardSelectedObj[name].selectedValue && radioCardSelectedObj[name].selectedValue === selectedValue ? radioCardActiveStyle : radioCardStyle}>
        {items && items.map((item) => {
          return generateWebUI(item.cmsComponentType, { ...item, dialogName, dataObj })
        })}
      </CardActionArea>
    </Card>
  );
}
