import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import util from "utils/util";
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import _ from 'lodash';

export default function Esign(props) {
  const { dataObj, cmsProps = {}, style } = props;
  const { dataUrl, params, preViewApi, startApi, saveApi, esignApi } = cmsProps;
  const classes = useStyles();
  const [itemMaps, setItemMaps] = useState([]);
  const [currentItem, setCurrentItem] = useState(null);
  const [dataList, setDataList] = useState([]);
  const [currentData, setCurrentData] = useState(null);
  const [loading, setLoading] = useState(false)
  const [iframeSrc, setIframeSrc] = useState(null);
  const [removeEvent, setRemoveEvent] = useState(null);
  
  const fetchData = async (data, api) => {
    const {dataUrl="", params=[], requestType="get"} = api;
    const rquest = util.resolveParams(data, dataUrl, params);
    const result = await util.fetchData(rquest.dataUrl, rquest.param, requestType);
    return result;
  }

  const resolveParamsValue = (data, api) => {
    return data && util.resolveParamsValue(data, api.params);
  }

  const fetchItemMaps = async () => {
    let result = await fetchData(dataObj, {dataUrl, params});
    setItemMaps(result ? result : []);
  }

  useEffect(() => { 
    setLoading(true);
    fetchItemMaps();
    setLoading(false);
  }, []);

  useEffect(() => {
    if (itemMaps) {
      if (currentItem) {
        let index = itemMaps.findIndex((item) => {
          return item.docId === currentItem.docId;
        });
        if (index !== -1) {
          setCurrentItem(itemMaps[index]);
        }
      } else {
        itemMaps[0] && setCurrentItem(itemMaps[0]);
      }
    }
  }, [itemMaps]);

  useEffect(() => {
    setCurrentData(null);
    if (currentItem) {
      if (currentItem.isFullSigned) {
        const {dataUrl="", params=[]} = preViewApi;
        const rquest = util.resolveParams(currentItem, dataUrl, params);
        setIframeSrc(rquest.dataUrl);
      } else {
        const getCurrentData = async () => {
          const dataKey = resolveParamsValue(currentItem, startApi);
          const dataIndex = dataList.findIndex((item) => {
            return _.isEqual(item.key, dataKey);
          });
          setLoading(true);
          let currentData = null;
          if (dataIndex !== -1) {
            currentData = {
              key: dataList[dataIndex].key,
              value: dataList[dataIndex].value
            };
          } else {
            const result = await fetchData(currentItem, startApi);
            if (result) {
              currentData = {
                key: dataKey,
                value: resolveParamsValue(result, esignApi)
              };
              dataList.push(currentData);
              setDataList(dataList);
            } else {
              alert("Request data failed");
            }
          }
          if (currentData) {
            setCurrentData(currentData);
            setIframeSrc(esignApi.dataUrl);
          }
          setLoading(false);
        }
        getCurrentData();
      }
    }
  }, [currentItem]);

  useEffect(() => {
    if (removeEvent) {
      window.removeEventListener("message", removeEvent.func, false);
    }
    if (iframeSrc) {
      // const childFrameObj = document.getElementById('esignatureFrontend');
      const sendMessage = (event) => {
        if(event !== undefined && event.data){
          if (event.data === "getSessionID") {
            const childFrameObj = document.getElementById('esignatureFrontend');
            childFrameObj.contentWindow.postMessage(currentData.value, '*'); //window.postMessage
          } else if (event.data.save) {
            const saveSigned = async () => {
              const data = {...currentItem, sessionID: currentData.value.sessionID};
              // const {dataUrl="", params=[], requestType="get"} = saveApi;
              // const rquest = util.resolveParams(data, dataUrl, params);
              // const saveStatus = await util.fetchData(rquest.dataUrl, rquest.param, requestType);
              const saveStatus = await fetchData(data, saveApi);
              if (saveStatus) {
                fetchItemMaps();
              }
            }
            setCurrentData(null);
            saveSigned();
          }
        }
      }
      window.addEventListener("message", sendMessage, false);
      setRemoveEvent({func: sendMessage});
    }
  }, [iframeSrc]);

  return (
    <Grid className={classes.root} container style={style}>
      <List className={classes.sidebar}>
        {itemMaps && itemMaps.map((item, inx) => {
          const isCurrent = currentItem && currentItem.docId === item.docId;
          return (
            <Button key={inx}
              classes={{
                root: isCurrent ? classes.currentItem : classes.listItem,
                label: classes.listItemText,
                startIcon: classes.listItemIcon
              }}
              style={{color: isCurrent && "inherit"}}
              disabled={isCurrent}
              startIcon={<CheckCircleIcon className={`${classes.startIcon} ${item.isFullSigned ? classes.fullSigned : classes.notFullSigned}`}/>}
              onClick={() => {
                setIframeSrc(null);
                setCurrentItem(item);
              }}
            >
              {item.type}
            </Button>
          )
        })}
      </List>
      <Grid className={classes.constent}>
        {iframeSrc &&
        <iframe
          id={"esignatureFrontend"}
          className={classes.iframe}
          name={"esignature"}
          style={{width:"100%",height:"100%", backgroundColor: "rgba(230,230,230, 1)", display: iframeSrc?"block":"none"}}
          src={iframeSrc || null}
          title="e-Sign"
        />
        }
      </Grid>
      {loading &&
        <Grid className={classes.loading} container direction="row" justify="center" alignItems="center">
          <CircularProgress style={{color: "rgba(169, 0, 47, 1)"}}/>
        </Grid>
      }
    </Grid>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
    // height: '100%',
    width: '100%',
    position: "relative"
  },
  sidebar: {
    height: "100%", 
    width: "300px",
    padding: "0px",
    margin: "0px",
    borderLeft: "1px solid rgba(202, 196, 196, 1)",
  },
  listItem: {
    width: "100%",
    padding: "18px 22px",
    fontWeight: "bold",
    borderBottom: "1px solid rgba(202, 196, 196, 1)",
    justifyContent: "flex-start",
    borderRadius: "0"
  },
  listItemText: {
  },
  listItemIcon: {
    // color: "green",
    paddingRight: "7px"
  },
  currentItem: {
    width: "100%",
    padding: "18px 22px",
    fontWeight: "bold",
    borderBottom: "1px solid rgba(202, 196, 196, 1)",
    justifyContent: "flex-start",
    borderRadius: "0",
    backgroundColor: "rgba(230, 230, 230, 1)",
  },
  constent: {
    backgroundColor: "rgba(243, 242, 241, 1)",
    border:"1px solid rgba(202, 196, 196, 1)",
    height: "100%",
    flexGrow: 1
  },
  startIcon: {
    width: "24px",
    height: "24px"
  },
  fullSigned: {
    color: "rgba(0, 182, 100, 1)"
  },
  notFullSigned: {
    color: "rgba(0, 0, 0, 0.26)"
  },
  loading: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      width: "100%",
      height: "100%",
      color: "rgba(237, 237, 237, 1)",
      backgroundColor: "rgba(85, 84, 84, 0.1)",
  }
}));