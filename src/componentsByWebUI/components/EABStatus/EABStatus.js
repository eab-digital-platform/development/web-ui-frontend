import React, { useContext } from 'react';
import Chip from '@material-ui/core/Chip';
import IconsUtils from 'componentsByWebUI/displayEngine/IconsUtils';
import WebUIContext from 'WebUIContext';
import lookup from 'utils/lookup';
import _ from 'lodash';

export default function EABStatus(props) {
  const { muiProps, cmsProps = {}, dataObj, style } = props;
  const { iconName, value, visible } = cmsProps;
  const { localization, mapping } = useContext(WebUIContext);
  const canShow = visible ? _.get(dataObj, visible.substr(1), false) : false;

  return (
    <React.Fragment>
      {canShow &&
        <Chip
          style={style}
          icon={IconsUtils[iconName]}
          label={value ? lookup.getValueFromData(dataObj, value, localization, mapping) : ""}
          {...muiProps}
        />
      }
    </React.Fragment>
  )
}
