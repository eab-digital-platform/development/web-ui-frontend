import React from 'react';
import Divider from '@material-ui/core/Divider';

export default function LineSeparator(props) {
  const { muiProps, style } = props;
  return (
    <Divider {...muiProps} style={{ ...style, width: "100%" }} />
  )
}
