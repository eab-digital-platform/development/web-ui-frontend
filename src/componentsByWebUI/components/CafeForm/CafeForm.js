import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CafeDisplayEngine from "cafe-interface-engine";
import configConstants from "config/configConstants";
import _ from 'lodash';
import util from "utils/util";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    height: "100%",
    minWidth: "64px",
    minHeight: "64px",
    "& > div:first-child": {
      width: "100%",
      height: "100%"
    }
  }
}));

export default function CafeForm(props) {
  const clases = useStyles();
  console.log("这里是进入了CafeForm的创建js, 传进来的props: ", props);
  // 下面几个还不知道怎么获得
  const { isPreview, pageIndex } = props;
  //得到传递过来的callbackFunctions, 这里的callbackFunctions一般是在iconButton那里创建的, 是一个array
  const { cmsProps, dataObj, actionFunctions } = props;
  const { formId, responseID, docType, dataUrl, params, callbackParamsAfterSubmit } = cmsProps;
  const [resID, setResID] = useState(null);
  const [isReadonly, setIsReadonly] = useState(false);

  // 定义cafeForm的回调函数
  const webuiCallback = (state = {}) => {
    console.log("这里是cafe callback", state);
    console.log("这里是dialog传递进来的actionFunctions: ", actionFunctions);
    if (state.success) {
      //循环调用callbackFunctions, 注意关闭事件应该在最后面一个
      if (callbackParamsAfterSubmit && callbackParamsAfterSubmit.length > 0) {
        callbackParamsAfterSubmit.forEach((item) => {
          const callbackFunction = _.get(actionFunctions, item.functionName, null);
          if (callbackFunction) {
            callbackFunction(item, state);
          }
        })
      }
    }
  }

  useEffect(() => {
    // 从数据对象中拿到对应的response id
    const fetchResId = (data) => {
      if (responseID && responseID.indexOf("#") === 0) {
        console.log("===这里cafe form打印出responseId===>", responseID);
        console.log("===这里cafe form打印出数据===>", data);
        console.log(_.get(data, responseID.substr(1), ""));
        setResID(_.get(data, responseID.substr(1), ""));
      }
    }

    // 主要是FNA打开cafe form的时候需要先根据dataUrl拿到client information , 然后重新生成一个response id
    if (dataUrl) {
      const fetchResponse = async () => {
        const rquest = util.resolveParams({ ...dataObj, formId }, dataUrl, params);
        const result = await util.fetchData(rquest.dataUrl, rquest.params);
        fetchResId(result);
      }
      fetchResponse();
    } else {
      fetchResId(dataObj);
    }
    // 设置cafe form是否是readonly
    setIsReadonly(_.get(dataObj, "viewShow", false));
  }, []);

  return (
    <div className={clases.root}>
      {
        resID === null ?
          null
          :
          <CafeDisplayEngine
            apiHost={configConstants.CAFE_API}
            formID={formId || ""}
            responseID={resID || ""}
            isReadonly={isReadonly || false}
            isPreview={isPreview || false}
            pageIndex={pageIndex || 0}
            isWebUI={true}
            onDone={webuiCallback}
            mqChannel={resID ? configConstants.MQ.EDIT : configConstants.MQ.CREATE}
            mqDocType={docType || ""}
            mqDataType={configConstants.MQREADFROMMESSAGE}
          />
      }

    </div>
  )
}
