import React from 'react';
import Avatar from '@material-ui/core/Avatar';

export default function EABAvatar(props) {
  const { muiProps, style } = props;
  return (
    <Avatar {...muiProps} style={style} />
  )
}
