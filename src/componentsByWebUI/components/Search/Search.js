import React, { useState, useEffect, useRef, useContext } from 'react';
import util from "utils/util";
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';
import _ from 'lodash';
import WebUIContext from 'WebUIContext';

export default function Search(props) {
  console.log("===这里是构建search开始===", props);
  const { cssClass, style, dataObj, cmsProps = {}, items, actionFunctions, dialogName, _id } = props;
  const { dataUrl, params, requestType = "get" } = cmsProps;
  // state
  // const [searchStr, setSearchStr] = useState("");
  const [searchParam, setSearchParam] = useState({});
  const [searchResult, setSearchResult] = useState(null);
  const [searchComponents, setSearchComponents] = useState(null);
  const searchStrRef = useRef("");

  // 加载显示隐藏loading的方法
  const cxt = useContext(WebUIContext);
  const { showLoading, hideLoading } = cxt.contextFunction;

  // 按键处理事件
  const handleEnterKey = (e) => {
    if (e.keyCode === 13) {
      console.log("=====这里控制按下了回车====>>>>>");
      handleSearch();
    }
  }

  // 添加监听键盘事件
  useEffect(() => {
    document.addEventListener("keydown", handleEnterKey);
    return () => { document.removeEventListener("keydown", handleEnterKey); };
  }, []);

  // 清空事件
  const handleClear = () => {
    console.log("===你执行了handleClear吗===");
    // setSearchStr("");
    searchStrRef.current = "";
    setSearchResult(null);
  }

  const handleSearchStr = (value) => {
    console.log("===你执行了handleSearchStr之前===", value);
    searchStrRef.current = value;
  }

  const fetchPageData = async (data) => {
    showLoading();
    const rquest = util.resolveParams(data, dataUrl, params);
    const pageData = await util.fetchData(rquest.dataUrl, rquest.params, requestType);
    setSearchResult(pageData)
    hideLoading();
  };

  // useEffect(() => {
  //   searchStrRef.current = searchStr;
  // }, [searchStr]);

  const handleSearch = () => {
    console.log("===你执行了handleSearch吗===", searchStrRef.current);
    fetchPageData({ searchStr: searchStrRef.current, searchParam, ...dataObj });
  }

  const handleSearchParam = ({ key, value }) => {
    console.log("===你执行了handleSearchParam吗===", searchParam);
    const data = _.cloneDeep(searchParam);
    if (value && !_.isEmpty(value)) {
      data[key] = value;
    } else if (typeof (data[key]) !== undefined) {
      delete data[key];
    }
    setSearchParam(data);
  }

  const searchFunctions = {
    ...actionFunctions,
    handleSearchStr,
    handleClear,
    handleSearch,
    handleSearchParam
  };

  // 创建一个search的refresh数据function, 保存到actionFunctions里面, function name目前固定为json id + refreshData
  if (dialogName && dataUrl && actionFunctions && actionFunctions[`${dialogName}`]) {
    actionFunctions[`${dialogName}`][`${_id}_refreshData`] = () => {
      handleSearch();
    }
  }

  //根据json文件创建search组件的子组件
  const createSearchComponents = (items, searchResult) => {
    return items.map((item, i) => generateWebUI(item.cmsComponentType, { ...item, dialogName, dataObj: { searchResult, ...dataObj }, actionFunctions: searchFunctions }))
  };

  //配置如果searchResult发生变化, 重新渲染整个页面
  useEffect(() => {
    console.log("====这里searchResult变化了就执行===");
    setSearchComponents(() => createSearchComponents(items, searchResult));
  }, [searchResult])

  return (
    <div className={cssClass} style={style}>
      {
        searchComponents
      }
    </div>
  )
}
