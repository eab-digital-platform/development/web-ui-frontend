import React, { useState } from 'react';
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";

export default function ButtonGroup(props) {
  const { style, dataObj, cmsProps = {}, items } = props;
  const [choice, setChoice] = useState("");
  const btnHandle = e => {
    // console.log("===>" + e.currentTarget.getAttribute("data-value"));
    const _choice = e.currentTarget.getAttribute("data-value");
    setChoice(_choice);
  };
  return (
    <ButtonGroup color="primary">
      <Button data-value="1" onClick={btnHandle} variant={choice === "1" ? "contained" : ""}>
        One
      </Button>
      <Button data-value="2" onClick={btnHandle} variant={choice === "2" ? "contained" : ""}>
        Two
      </Button>
    </ButtonGroup>
  );
}
