import React, { useContext, useState, useEffect } from 'react';
import WebUIContext from 'WebUIContext';
import util from "utils/util";
// import Loading from 'componentsByWebUI/components/Loading/Loading';
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';

export default function Blank(props) {
  console.log("========这里是blank获取数据的props========", props);
  const { cssClass, style, dataObj, cmsProps = {}, items, actionFunctions, dialogName, _id } = props;
  const { dataUrl, params } = cmsProps;
  const [pageData, setPageData] = useState(null);
  const cxt = useContext(WebUIContext);
  const { showLoading, hideLoading } = cxt.contextFunction;

  const fetchPageData = async () => {
    showLoading();
    let pageData = {};
    console.log("=====这里是blank的一个params的打印: ", params);
    console.log("=====这里是blank的一个params的打印: ", dataObj);
    console.log("=====这里是blank的一个params的打印: ", dataUrl);
    if (dataUrl && dataUrl.indexOf(".json") >=0 ) {
      pageData = require(`mock/${dataUrl}`);
    }else{
      if (params && params.length > 0) {
        const rquest = util.resolveParams(dataObj, dataUrl, params);
        // console.log("=====这里是blank的构造api的url rquest的打印: ", rquest);
        pageData = await util.fetchData(rquest.dataUrl, rquest.params);
      } else {
        pageData = await generateWebUIData(dataUrl, cxt, dataObj, {});
      }
    }
    
    console.log("get blank pageData => ", pageData);
    setPageData(pageData ? pageData : {});
    hideLoading();
  }

  useEffect(() => {
    console.log("====这里是blank的useEffect==dataObj改变了执行===");
    fetchPageData();
  }, []);

  // 创建一个blank的refresh数据function, 保存到actionFunctions里面, function name目前固定为json id + refreshData
  if (dialogName && actionFunctions && dataUrl) {
    actionFunctions[`${dialogName}`][`${_id}_refreshData`] = () => {
      setPageData(null);
      fetchPageData();
    }
  }

  // console.log(`===========这里是哪一个blank呢, 是这个[${dialogName}]下面的, 这里的actionFunctions::`, pageData);
  return (
    <React.Fragment>
      {
        pageData &&
        <div className={cssClass} style={style}>
          {
            items.map((item) => {
              return generateWebUI(item.cmsComponentType, { ...item, dialogName, dataObj: pageData, actionFunctions })
            })
          }
        </div>
      }
    </React.Fragment>
  )
}
