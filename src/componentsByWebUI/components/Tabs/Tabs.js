import React, { useEffect, useState } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider';
import util from "utils/util";
import _ from 'lodash';
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';

export default function TabsComponent(props) {
  console.log("==这里打印出Tabs的props==", props);
  const { muiProps, style, dataObj, items, actionFunctions = {}, dialogName } = props;
  const [tabsValue, setTabsValue] = useState(0);
  const [currentPage, setCurrentPage] = useState(null);
  const [tabsData, setTabsData] = useState(dataObj);

  // 构造整个tabs有哪些tab
  const filterTab = (tabObj) => {
    const { cmsComponentType: tabCmsComponentType, cmsProps: tabCmsProps = {} } = tabObj;
    const { visible: tabVisible } = tabCmsProps;
    let tabShow = true;
    if (tabVisible) {
      tabShow = _.get(tabsData, tabVisible.substr(1), false);
    }
    return tabCmsComponentType === "TAB" && tabShow;
  }

  let tabs = [];
  if (Array.isArray(items)) {
    // const {dataUrl, actionUrl, visible} = cmsProps;
    tabs = items.filter(item => filterTab(item));
  }

  useEffect(() => {
    console.log("===这里打印出来的是tabs的useEffect===");
    fetchCurrentPage(setCurrentPage);
  }, [tabsValue, tabsData])

  const fetchCurrentPage = async (setCurrentPage) => {
    const configRes = await util.fetchPageJsonData(tabs[tabsValue].cmsProps.actionUrl);
    setCurrentPage(configRes);
  };

  const checkoutCompleted = () => {
    console.log("====这里是定义了checkoutCompleted===");
    console.log("====前面额数据===", tabsData);
    setCurrentPage(null);
    setTabsData(oldObj => ({ ...oldObj, continueEappShow: false, viewEappShow: true }));
    console.log("====后面额数据===", tabsData);
  }

  actionFunctions["checkoutCompleted"] = checkoutCompleted;

  return (tabsData && <React.Fragment>
    <Tabs {...muiProps} style={style} value={tabsValue} onChange={(event, newValue) => {
      if (tabsValue!==newValue) {
        setCurrentPage(null);
        setTabsValue(newValue);
      }
    }}>
      {tabs.map((tab, inx) => {
        return <Tab key={inx} {...tab.muiProps} style={tab.style} />;
      })}
    </Tabs>
    {(muiProps && (muiProps.orientation === "horizontal")) && <Divider />}
    <div style={tabs[tabsValue].style}>
      {currentPage && generateWebUI(currentPage.cmsComponentType, { ...currentPage, dialogName, dataObj: tabsData, actionFunctions })}
    </div>
  </React.Fragment>
  )
}