import React, { useContext, useState, useEffect, useRef } from 'react';
import WebUIContext from 'WebUIContext';
import { generateWebUI, generateWebUIData } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';
// import Loading from 'componentsByWebUI/components/Loading/Loading';

function MainControl(props) {
  const { style, cmsProps = {}, dataObj, items } = props;
  const { dataUrl } = cmsProps;
  const cxt = useContext(WebUIContext);
  const { showLoading, hideLoading } = cxt.contextFunction;

  const [data, setData] = useState(null);
  // console.log("这里是进入到MainControl.js, 页面data =>", data);
  const fetchPageData = useRef(async () => {
    showLoading();//打开loading
    const pageData = await generateWebUIData(dataUrl, cxt, dataObj, {});
    setData(pageData ? pageData : {});
    hideLoading();
    // 5秒后关闭loading
    // setTimeout(() => {
    //   hideLoading();
    // }, 5000);
  });
  // 页面加载完成后会执行这里拿数据
  useEffect(() => {
    fetchPageData.current();
  }, [])

  return (
    <div style={style}>
      {
        data &&
        items.map((item, i) => {
          return generateWebUI(item.cmsComponentType, { ...item, dataObj: data });
        })
      }
    </div>
  );
}

export default MainControl