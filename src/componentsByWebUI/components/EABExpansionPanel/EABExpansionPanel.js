import React from 'react';
import { Grid } from '@material-ui/core'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
// import ExpandMore from '@material-ui/icons/ExpandMore';
import IconsUtils from 'componentsByWebUI/displayEngine/IconsUtils';
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider'
// import { generateWebUI } from 'componentsByWebUI/components/UploadFile/UploadFile'

function EABExpansionPanel(props) {
  // console.log("这里是创建ExpansionPanel, 这里是一个普通的展开的, props : ", props);
  const { key, dataObj, cmsProps = {}, muiProps, style, items, actionFunctions } = props;
  const { defaultExpanded, iconName, leftText, leftTextStyle, rightText, rightTextStyle, panelDetailsStyle } = cmsProps;

  const leftObj = {
    cmsComponentType: leftText && leftText.indexOf("#") >= 0 ? "TEXT" : "LABEL",
    key: key + "_leftText",
    style: leftTextStyle,
    cmsProps: {
      value: leftText
    }
  };

  const rightObj = {
    cmsComponentType: rightText && rightText.indexOf("#") >= 0 ? "TEXT" : "LABEL",
    key: key + "_rightText",
    style: rightTextStyle,
    cmsProps: {
      value: rightText
    }
  };

  return (
    <ExpansionPanel key={key} style={style} defaultExpanded={defaultExpanded}>
      <ExpansionPanelSummary
        expandIcon={IconsUtils[iconName]}
        {...muiProps}
      >
        <Grid container>
          <Grid item xs={6}>
            <Grid container justify="flex-start">
              {leftText && generateWebUI(leftObj.cmsComponentType, { ...leftObj, dataObj: dataObj, actionFunctions })}
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <Grid container justify="flex-end">
              {rightText && generateWebUI(rightObj.cmsComponentType, { ...rightObj, dataObj: dataObj, actionFunctions })}
            </Grid>
          </Grid>
        </Grid>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails style={panelDetailsStyle}>
        {/* 这里创建items里面的元素 */}
        {items && items.map((item) => {
          return generateWebUI(item.cmsComponentType, { ...item, dataObj: dataObj, actionFunctions })
        })}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default EABExpansionPanel;

