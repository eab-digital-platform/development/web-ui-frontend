import React, { useContext } from 'react';
import WebUIContext from 'WebUIContext';
import lookup from 'utils/lookup';
import Typography from '@material-ui/core/Typography';
import _ from 'lodash';

export default function EABText(props) {
  const cxt = useContext(WebUIContext);
  const { localization, mapping } = cxt;
  const { muiProps, dataObj, variant, cmsProps = {}, style } = props;
  const { value, visible } = cmsProps;

  let display = true;
  if (visible) {
    display = _.get(dataObj, visible.substr(1), false);
  }

  return (
    display &&
    <React.Fragment>
      <Typography variant={variant || "inherit"} {...muiProps} style={style}>
        {lookup.getValueFromData(dataObj, value, localization, mapping)}
      </Typography>
    </React.Fragment>
  )
}
