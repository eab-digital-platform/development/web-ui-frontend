import React from 'react'
import { Typography } from '@material-ui/core';
import _ from 'lodash';

export default function EABLabel(props) {
  // const cxt = useContext(WebUIContext);
  // const { localization } = cxt;
  // const { css_class, align, valign, width, display, variant } = element;
  // const style = { textAlign: align, verticalAlign: valign, width, display };
  const { muiProps, variant, cmsProps = {}, style, dataObj } = props;
  const { value, visible } = cmsProps;
  
  let display = true;
  if (visible) {
    display = _.get(dataObj, visible.substr(1), false);
  }

  return (
    display && 
    <Typography variant={variant || "inherit"} {...muiProps} style={style}>
      {value}
    </Typography>
  )
}
