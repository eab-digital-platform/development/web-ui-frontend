import React, { useState, useContext } from 'react';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import IconBtn from '@material-ui/core/IconButton';
import Link from '@material-ui/core/Link';
import IconsUtils from 'componentsByWebUI/displayEngine/IconsUtils';
import WebUIContext from 'WebUIContext';
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';
import _ from 'lodash';
import util from "utils/util";
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStylesEABTooltip = makeStyles((theme) => ({
  arrow: {
    color: "#cccccc"
  },
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    border: '1px solid #dadde9',
  },
}));

function EABTooltip(props) {
  const classes = useStylesEABTooltip();
  const { value = "", arrow = false, placement = "top-start", dataObj } = props;
  const title = value.indexOf("#") >= 0 ? _.get(dataObj, value.substr(1), "") : value;
  return <Tooltip
    classes={classes}
    arrow={arrow}
    placement={placement}
    title={
      <React.Fragment>
        <Typography color="inherit">{title}</Typography>
      </React.Fragment>
    }
  >
    {props.children}
  </Tooltip>;
}


function EABButton(props) {
  console.log("====>这里是创建iconbutton, props: ", props);
  const cxt = useContext(WebUIContext);
  const { cmsComponentType, muiProps = {}, dataObj, cmsProps = {}, style, actionFunctions } = props;
  const { actionType, actionUrl, iconName, fullScreen, displayText, visible, disabled, params, requestType, fullWidth = false, maxWidth = false, callbackFunction, startIcon, endIcon, preHandle, radioCardSelectedName, tooltips } = cmsProps;
  // 下面dialogOpened和toggleDialog是为了点击这个button弹出一个dialog使用的
  const [dialogOpened, setDialogOpened] = useState(false);
  const openDialog = () => {
    setDialogOpened(true);
  };

  const closeDialog = () => {
    setDialogOpened(false);
  };

  // 定义打开dialog前执行的api返回的数据
  const [preHandleData, setPreHandleData] = useState(null);
  const previousHandleFunction = async (preHandle) => {
    const { api, type, params } = preHandle;
    const request = util.resolveParams(dataObj, api, params);
    return await util.fetchData(request.dataUrl, request.params, type);
  }

  // 定义这个button事件, 如果actionType="dialog", 就是打开dialog, 否则就是调用useContext里面定义的方法
  // 目前定义在useContext里面的方法只有打开/关闭leftmenu
  let btnHandleClick;
  let dialogProps;
  if (actionType === "dialog") {
    btnHandleClick = () => {
      // 这里判断是否有preHandle配置, 如果有, 则先执行preHandle配置的api, 然后将返回的data传递给打开的dialog
      if (preHandle) {
        // console.log("======这里是进入到执行打开dialog前置api执行过程中========");
        previousHandleFunction(preHandle).then(result => {
          // console.log("======这里是dialog前置api执行返回的data========", result);
          setPreHandleData(result);
          openDialog();
        });
      } else {
        openDialog();
      };
    }

    // 下面这里构建dialog需要的props, 把关闭dialog的方法传进去
    dialogProps = { cmsProps: { actionUrl, fullScreen, fullWidth, maxWidth }, dialogOpened, closeDialog };
  } else if (actionType === "api") {
    if (actionUrl.indexOf("createEapp") !== -1) {
      //这个hardcode处理application选择了quote之后新建一个新的application number
      console.log("====看一下createEapp这里的api数据==>>>>", radioCardSelectedName ? { ...dataObj, radioCardSelectedObj: actionFunctions.radioCardSelectedObj } : dataObj);
      const request = util.resolveParams(radioCardSelectedName ? { ...dataObj, radioCardSelectedObj: actionFunctions.radioCardSelectedObj } : dataObj, actionUrl, params);
      console.log("====其他数据==>>>>", actionUrl, params, request);

      btnHandleClick = () => {
        util.fetchData(request.dataUrl, request.params, requestType).then(result => {
          callbackFunction.forEach(funcName => {
            const handleCallbackFunction = _.get(actionFunctions, funcName, () => { console.log("==这个function不能在actionFunctions找到, 请检查代码=="); });
            handleCallbackFunction(cmsProps, result)
          });
        });
      }
    } else {
      console.log("====看一下这里的api数据==>>>>", radioCardSelectedName ? { ...dataObj, radioCardSelectedObj: actionFunctions.radioCardSelectedObj } : dataObj);
      const request = util.resolveParams(radioCardSelectedName ? { ...dataObj, radioCardSelectedObj: actionFunctions.radioCardSelectedObj } : dataObj, actionUrl, params);

      btnHandleClick = () => {
        util.fetchData(request.dataUrl, request.params, requestType).then(result => {
          console.log("这里是api调用返回的result: ", result);
          if (result.ok === 1) {
            // console.log("这里是调用api成功之后, 这里应该是调用callback function");
            // console.log("但是我不知道callback function哪里定义");
            // console.log("有时候需要关闭dialog, 并且刷新父级的页面; 有时候需要弹出框, 点确定在关闭; 有时候是关闭一个popper");
            callbackFunction.forEach(funcName => {
              const handleCallbackFunction = _.get(actionFunctions, funcName, () => { console.log("==这个function不能在actionFunctions找到, 请检查代码=="); });
              handleCallbackFunction()
            });
          }
        });

      }
    }

  } else if (actionType === "dialog.redirect") {
    const { dialogName } = props;
    if (dialogName) {
      const funcs = { ...cxt.contextFunction, ...actionFunctions };
      btnHandleClick = _.get(funcs, `${dialogName}.handleDialogRedirect`, () => { console.log("==这个handleDialogRedirect不能在actionFunctions找到, 请检查代码=="); });
    } else {
      btnHandleClick = () => { console.log("==这里运行了, 代表这个button是redirect的, 但是没有找到dialogName=="); };
    }
  } else {
    // 这里将contextFunction和actionFunctions合并, 因为这个button有可能是要关闭dialog, 有可能是打开leftmenu
    const funcs = { ...cxt.contextFunction, ...actionFunctions };
    btnHandleClick = _.get(funcs, actionUrl, () => { console.log("==这个function不能在actionFunctions找到, 请检查代码=="); });
  }

  const generateButtonUI = () => {
    muiProps.disabled = disabled && _.get(dataObj, disabled.substr(1), false);
    switch (cmsComponentType) {
      case "ICON_BUTTON":
        return (
          <IconBtn {...muiProps} style={style} onClick={() => btnHandleClick(cmsProps, dataObj)}>
            {IconsUtils[iconName]}
          </IconBtn>
        );
      case "BUTTON":
        return (
          tooltips ?
            <EABTooltip {...tooltips} dataObj={dataObj}>
              <Button {...muiProps} style={style} onClick={() => btnHandleClick(cmsProps, dataObj)}
                startIcon={IconsUtils[startIcon]}
                endIcon={IconsUtils[endIcon]}
              >
                {displayText}
              </Button>
            </EABTooltip>
            :
            <Button {...muiProps} style={style} onClick={() => btnHandleClick(cmsProps, dataObj)}
              startIcon={IconsUtils[startIcon]}
              endIcon={IconsUtils[endIcon]}
            >
              {displayText}
            </Button>
        );
      case "FLOATING_BUTTON":
        return (
          <Fab {...muiProps} style={{ ...style, zIndex: 10000 }} onClick={() => btnHandleClick(cmsProps, dataObj)}>
            {IconsUtils[iconName]}
          </Fab>
        );
      case "TEXT_LINK":
        const { items } = props;
        return (
          tooltips ?
            <EABTooltip {...tooltips} dataObj={dataObj}>
              <Link onClick={() => btnHandleClick(cmsProps)} {...muiProps} style={{ "cursor": "pointer" }}>
                {
                  items && items.map((item, i) => {
                    return generateWebUI(item.cmsComponentType, { ...item, dataObj })
                  })
                }
              </Link>
            </EABTooltip>
            :
            <Link onClick={() => btnHandleClick(cmsProps)} {...muiProps} style={{ "cursor": "pointer" }}>
              {
                items && items.map((item, i) => {
                  return generateWebUI(item.cmsComponentType, { ...item, dataObj })
                })
              }
            </Link>
        );
      default:
        return null;
    }
  }

  return (
    <React.Fragment>
      {(!visible || _.get(dataObj, visible.substr(1), false)) && generateButtonUI()}
      {/* 如果这里的actionType='dialog', 这里创建dialog组件对象 */}
      {dialogOpened && generateWebUI("EABDialog", { ...dialogProps, dataObj: { ...dataObj, ...preHandleData }, actionFunctions })}
    </React.Fragment>
  );
}

export default EABButton;
