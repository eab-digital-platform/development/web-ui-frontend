import React from 'react';
import Popper from '@material-ui/core/Popper';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import IconsUtils from 'componentsByWebUI/displayEngine/IconsUtils';
import { generateWebUI } from 'componentsByWebUI/displayEngine/DisplayEngineProvider';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import _ from 'lodash';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  popper: {
    zIndex: 1301,//因为dialog的z-index是1300, 这个应该在dialog上层, 暂时设定为1301
    '&[x-placement*="bottom"] $arrow': {
      top: 0,
      left: 0,
      marginTop: "-0.9em",
      width: "3em",
      height: "1em",
      "&::before": {
        borderWidth: "0 1em 1em 1em",
        borderColor: `transparent transparent ${
          theme.palette.background.paper
          } transparent`
      }
    },
    '&[x-placement*="top"] $arrow': {
      bottom: 0,
      left: 0,
      marginBottom: "-0.9em",
      width: "3em",
      height: "1em",
      "&::before": {
        borderWidth: "1em 1em 0 1em",
        borderColor: `${
          theme.palette.background.paper
          } transparent transparent transparent`
      }
    },
    '&[x-placement*="right"] $arrow': {
      left: 0,
      marginLeft: "-0.9em",
      height: "3em",
      width: "1em",
      "&::before": {
        borderWidth: "1em 1em 1em 0",
        borderColor: `transparent ${
          theme.palette.background.paper
          } transparent transparent`
      }
    },
    '&[x-placement*="left"] $arrow': {
      right: 0,
      marginRight: "-0.9em",
      height: "3em",
      width: "1em",
      "&::before": {
        borderWidth: "1em 0 1em 1em",
        borderColor: `transparent transparent transparent ${
          theme.palette.background.paper
          }`
      }
    }
  },
  arrow: {
    position: "absolute",
    fontSize: 7,
    width: "3em",
    height: "3em",
    "&::before": {
      content: '""',
      margin: "auto",
      display: "block",
      width: 0,
      height: 0,
      borderStyle: "solid"
    }
  }
}));

export default function EABPopper(props) {
  console.log("========这里生成EABPopper, 打印props======", props);
  const classes = useStyles();
  const { muiProps, dataObj, cmsProps = {}, items, style, actionFunctions = {} } = props;
  const { dataUrl, iconName, visible, size, arrow, notAutoHideBackdrop } = cmsProps;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [arrowRef, setArrowRef] = React.useState(null);
  const handleClick = event => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  //这里给actionFunctions添加一个关闭popper的事件
  actionFunctions.handleClosePopper = () => {
    setAnchorEl(null);
  };

  let result = dataUrl ? dataObj[dataUrl] : dataObj;

  const modifiers = { flip: { enabled: true }, preventOverflow: { enabled: true, boundariesElement: 'scrollParent' } };
  if (arrow) {
    modifiers.arrow = {
      enabled: true,
      element: arrowRef
    };
  }

  return (
    (!visible || _.get(dataObj, visible.substr(1), false)) &&
    <ClickAwayListener onClickAway={() => {
      !notAutoHideBackdrop && setAnchorEl(null);
    }}>
      <div>
        <IconButton onClick={handleClick} size={size}>
          {IconsUtils[iconName]}
        </IconButton>
        <Popper
          modifiers={modifiers}
          open={Boolean(anchorEl)}
          anchorEl={anchorEl}
          {...muiProps}
          className={classes.popper}
          style={style} 
        >
          {arrow ? <span className={classes.arrow} ref={setArrowRef} /> : null}
          <Paper>
            {items.map((item) => {
              return generateWebUI(item.cmsComponentType, { ...item, dataObj: result, actionFunctions })
            })}
          </Paper>
        </Popper>
      </div>
    </ClickAwayListener>
  );
}
