import React, { forwardRef, useImperativeHandle } from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: 9999,
    color: '#fff',
  },
}));

const Loading = (props, ref) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  // 定义暴露给父组件使用的方法
  useImperativeHandle(ref, () => ({
    showBackdrop: () => {
      console.log("======打开loading======");
      setOpen(true);
    },
    hideBackdrop: () => {
      console.log("======关闭loading======");
      setOpen(false);
    }
  }));
  return (
    <Backdrop className={classes.backdrop} open={open}>
      <CircularProgress color="default" />
    </Backdrop>
  )
};

export default forwardRef(Loading);
