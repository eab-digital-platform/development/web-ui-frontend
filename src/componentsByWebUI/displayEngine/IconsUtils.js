import React from 'react';

import Folder from '@material-ui/icons/Folder';
import People from '@material-ui/icons/People';
import Star from '@material-ui/icons/Star';
import Schedule from '@material-ui/icons/Schedule';
import OfflinePin from '@material-ui/icons/OfflinePin';
import Publish from '@material-ui/icons/Publish';
import Backup from '@material-ui/icons/Backup';
import Delete from '@material-ui/icons/Delete';
import Settings from '@material-ui/icons/Settings';
import Menu from '@material-ui/icons/Menu';
import Search from '@material-ui/icons/Search';
import Close from '@material-ui/icons/Close';
import Add from '@material-ui/icons/Add';
import Create from '@material-ui/icons/Create';
import ArrowBack from '@material-ui/icons/ArrowBack';
import CheckCircle from '@material-ui/icons/CheckCircle';
import Cancel from '@material-ui/icons/Cancel';
import Visibility from '@material-ui/icons/Visibility';
import CreateNewFolderOutlined from '@material-ui/icons/CreateNewFolderOutlined';
import DescriptionOutlined from '@material-ui/icons/DescriptionOutlined';
import ArrowForwardIos from '@material-ui/icons/ArrowForwardIos';
import Description from '@material-ui/icons/Description';
import MoreVert from '@material-ui/icons/MoreVert';
import Done from '@material-ui/icons/Done';
import Loyalty from '@material-ui/icons/Loyalty';
import FormatListBulleted from '@material-ui/icons/FormatListBulleted';
import NoteAdd from '@material-ui/icons/NoteAdd';
import CloudUpload from '@material-ui/icons/CloudUpload';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Save from '@material-ui/icons/Save';

const icons = {
  Folder: <Folder />,
  People: <People />,
  Star: <Star />,
  Schedule: <Schedule />,
  OfflinePin: <OfflinePin />,
  Publish: <Publish />,
  Backup: <Backup />,
  Delete: <Delete />,
  Settings: <Settings />,
  Menu: <Menu />,
  Search: <Search />,
  Close: <Close />,
  Add: <Add />,
  Create: <Create />,
  ArrowBack: <ArrowBack />, // 这是返回箭头
  CheckCircle: <CheckCircle />,
  Cancel: <Cancel />,
  Visibility: <Visibility />,
  CreateNewFolderOutlined: <CreateNewFolderOutlined />,//这个是FNA "create new" icon
  DescriptionOutlined: <DescriptionOutlined />, //这个是FNA "use existing" icon
  ArrowForwardIos: <ArrowForwardIos />, //这个是proposal那个search button
  Description: <Description />,
  MoreVert: <MoreVert />, //这是三个点
  Done: <Done />,
  Loyalty: <Loyalty />, //quote状态图标
  FormatListBulleted: <FormatListBulleted />, //EAPP那边的FNA按钮icon
  NoteAdd: <NoteAdd />,
  CloudUpload: <CloudUpload />,
  ExpandMore: <ExpandMore />,
  Save: <Save />
};
export default icons;