import React from 'react';
import MainControl from 'componentsByWebUI/components/MainControl/MainControl';
import Header from 'componentsByWebUI/components/Header/Header';
import GridRow from 'componentsByWebUI/components/GridRow/GridRow';
import GridColumn from 'componentsByWebUI/components/GridColumn/GridColumn';
import EABButton from 'componentsByWebUI/components/EABButton/EABButton';
import EABLabel from 'componentsByWebUI/components/EABLabel/EABLabel';
import LeftMenu from 'componentsByWebUI/components/LeftMenu/LeftMenu';
import Blank from 'componentsByWebUI/components/Blank/Blank';
import EABAvatar from 'componentsByWebUI/components/EABAvatar/EABAvatar';
import EABText from 'componentsByWebUI/components/EABText/EABText';
import ButtonLink from 'componentsByWebUI/components/ButtonLink/ButtonLink';
import Carousell from 'componentsByWebUI/components/Carousell/Carousell';
import EABImage from 'componentsByWebUI/components/EABImage/EABImage';
import EABDialog from 'componentsByWebUI/components/EABDialog/EABDialog';
import CafeForm from 'componentsByWebUI/components/CafeForm/CafeForm';
import Search from 'componentsByWebUI/components/Search/Search';
import Input from 'componentsByWebUI/components/Input/Input';
import SearchResult from 'componentsByWebUI/components/SearchResult/SearchResult';
import Card from 'componentsByWebUI/components/Card/Card';
import Tabs from 'componentsByWebUI/components/Tabs/Tabs';
import LineSeparator from 'componentsByWebUI/components/LineSeparator/LineSeparator';
import EABPopper from 'componentsByWebUI/components/EABPopper/EABPopper';
import EABCheckBox from 'componentsByWebUI/components/EABCheckBox/EABCheckBox';
import RadioCard from 'componentsByWebUI/components/RadioCard/RadioCard';
import CheckBoxGroup from 'componentsByWebUI/components/CheckBoxGroup/CheckBoxGroup';
import Esign from 'componentsByWebUI/components/Esign/Esign';

import webAjax from 'utils/request'
import PDFPreview from 'componentsByWebUI/components/PDFPreview/PDFPreview';
import EABStatus from 'componentsByWebUI/components/EABStatus/EABStatus';
import UploadExpansionPanel from 'componentsByWebUI/components/UploadExpansionPanel/UploadExpansionPanel';
import EABExpansionPanel from 'componentsByWebUI/components/EABExpansionPanel/EABExpansionPanel';
import UploadFile from 'componentsByWebUI/components/UploadFile/UploadFile';
import IFrame from 'componentsByWebUI/components/IFrame/IFrame';

const generateMainControl = ({ ...props }) => {
  console.log("这里执行generateMainControl", props);
  return (<MainControl {...props} />);
}

const generateHeader = ({ ...props }) => {
  return (<Header {...props} />);
}

const generateGridRow = ({ ...props }) => {
  return (<GridRow {...props} />);
}

const generateGridColumn = ({ ...props }) => {
  return (<GridColumn {...props} />);
}

const generateButton = ({ ...props }) => {
  return (<EABButton {...props} />);
}

const generateLabel = ({ ...props }) => {
  return (<EABLabel {...props} />);
}

const generateLeftMenu = ({ ...props }) => {
  return (<LeftMenu {...props} />);
}

const generateBlank = ({ ...props }) => {
  return (<Blank {...props} />);
}

const generateSearch = ({ ...props }) => {
  return (<Search {...props} />);
}

const generateImageAvatar = ({ ...props }) => {
  return (<EABAvatar {...props} />)
}

const generateText = ({ ...props }) => {
  return (<EABText {...props} />)
}

const generateButtonLink = ({ ...props }) => {
  return (<ButtonLink {...props} />)
}

const generateCarsousell = ({ ...props }) => {
  return (<Carousell {...props} />)
}

const generateImage = ({ ...props }) => {
  return (<EABImage {...props} />)
}

const generateEABDialog = ({ ...props }) => {
  return (<EABDialog {...props} />)
}

const generateCafeForm = ({ ...props }) => {
  return (<CafeForm {...props} />)
}

const generateInput = ({ ...props }) => {
  return (<Input {...props} />)
}

const generateSearchResult = ({ ...props }) => {
  return (<SearchResult {...props} />)
}

const generateCard = ({ ...props }) => {
  return (<Card {...props} />)
}

const generateTabs = ({ ...props }) => {
  return (<Tabs {...props} />)
}

const generateLineSeparator = ({ ...props }) => {
  return (<LineSeparator {...props} />)
}

const generatePopper = ({ ...props }) => {
  return (<EABPopper {...props} />)
}

const generateCheckBox = ({ ...props }) => {
  return (<EABCheckBox {...props} />)
}

const generateCheckBoxGroup = ({ ...props }) => {
  return (<CheckBoxGroup {...props} />)
}

const generateRadioCard = ({ ...props }) => {
  return (<RadioCard {...props} />)
}

const generatePDFPreview = ({ ...props }) => {
  return (<PDFPreview {...props} />)
}

const generateStatus = ({ ...props }) => {
  return (<EABStatus {...props} />)
}

const generateEsign = ({ ...props }) => {
  return (<Esign {...props} />)
}

const generateUploadExpansionPlan = ({ ...props }) => {
  return (<UploadExpansionPanel {...props} />)
}

const generateExpansionPlan = ({ ...props }) => {
  return (<EABExpansionPanel {...props} />)
}

const generateUpload = ({ ...props }) => {
  return (<UploadFile {...props} />)
}

const generateIFrame = ({ ...props }) => {
  return (<IFrame {...props} />)
}

//这里将所有的创建方法和type做一个映射
const generateFunctions = new Map([
  ["MAIN_CONTROL", generateMainControl],
  ["HEADER", generateHeader],
  ["GRID_ROW", generateGridRow],
  ["GRID_COLUMN", generateGridColumn],
  ["ICON_BUTTON", generateButton],
  ["BUTTON", generateButton],
  ["FLOATING_BUTTON", generateButton],
  ["LABEL", generateLabel],
  ["LEFT_MENU", generateLeftMenu],
  ["BLANK", generateBlank],
  ["IMAGE_AVATAR", generateImageAvatar],
  ["TEXT", generateText],
  ["BUTTON_LINK", generateButtonLink],
  ["TEXT_LINK", generateButton],
  ["CARSOUSELL", generateCarsousell],
  ["IMAGE", generateImage],
  ["EABDialog", generateEABDialog],
  ["CAFE_FORM", generateCafeForm],
  ["SEARCH", generateSearch],
  ["INPUT", generateInput],
  ["SEARCH_RESULT", generateSearchResult],
  ["CARD", generateCard],
  ["TABS", generateTabs],
  ["LINE_SEPARATOR", generateLineSeparator],
  ["POPPER", generatePopper],
  ["CHECKBOX", generateCheckBox],
  ["PDF_PREVIEW", generatePDFPreview],
  ["CHECKBOX_GROUP", generateCheckBoxGroup],
  ["RADIO_CARD", generateRadioCard],
  ["STATUS", generateStatus],
  ["ESIGN", generateEsign],
  ["UPLOAD_EXPANSION_PANEL", generateUploadExpansionPlan],
  ["EXPANSION_PANEL", generateExpansionPlan],
  ["UPLOAD_DOC", generateUpload],
  ["IFRAME", generateIFrame]
]);

const generateWebUI = (key, props) => {
  // console.log("调用generateWebUI:", key, props);
  let generateFunction = generateFunctions.get(key);
  if (generateFunction) {
    return generateFunction(props);
  }
}

const generateWebUIData = (dataUrl, cxt, dataObj, params) => {
  if (dataUrl) {
    if (dataObj && dataObj[dataUrl]) {
      return dataObj[dataUrl];
    } else if (cxt[dataUrl]) {
      return cxt[dataUrl];
    } else {
      // console.log("这里generateWebUIData调用的url > ", dataUrl);
      const data = webAjax.get(dataUrl, params)
        .then(res => {
          // console.log("这里generateWebUIData返回的response > ", res);
          if (res.success && res.result) {
            // console.log("==这里是根据dataUrl查询到的数据:", res.result[0]);
            return res.result[0];
          } else {
            return { msg: "数据查询不是success!" }
          }
        });
      return data;
    }
  } else {
    return dataObj;
  }
}
export { generateWebUI, generateWebUIData }