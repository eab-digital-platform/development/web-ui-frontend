import React, { useState, useEffect, useRef } from 'react';
import util from "utils/util";
// import { createWebUIPage, createWebUIComponent } from './DisplayEngineUtils'
import { generateWebUI } from './DisplayEngineProvider'
import configConstants from "config/configConstants";

function DisplayEngine(props) {
  // console.log("===这里调用display engine传递过来的organizationId, projectId分别是 :=>", props);
  const { params = {}, data, isPreview = false } = props;
  const { organizationId, projectId } = params;
  // const [displayEngineData] = useState(props.data)
  // const [displayEngineData, setDisplayEngineData] = useState(null);
  const [displayConfig, setDisplayConfig] = useState(null);

  // 定义拿到这个project的firstPageId
  const fetchFirstPageConfig = useRef(async () => {
    // http://cmsadminapi-dp.eab.openshift/project/5e572b0450b21a0032f5c444
    if (configConstants.BACKEND_URL_FOR_PAGE.indexOf("getJsonByDocId") !== -1) {
      const projectInfo = await util.fetchPageJsonData("first_page");
      // console.log("这里拿到firstPage对象:", projectInfo);
      const displayConfig = await util.fetchPageJsonData(projectInfo.firstPageId);
      // console.log("这里拿到firstPage里面配置的json对象:", displayConfig);
      setDisplayConfig(displayConfig)
    } else {
      const projectInfo = await util.fetchProjectJsonData(organizationId, projectId);
      // console.log("这里拿到firstPage对象:", projectInfo);
      const displayConfig = await util.fetchPageJsonData(projectInfo.settings.firstPageId);
      // console.log("这里拿到firstPage里面配置的json对象:", displayConfig);
      setDisplayConfig(displayConfig)
    }
  });


  useEffect(() => {
    if (data) {
      setDisplayConfig(data);
    }
  }, [data]);

  useEffect(() => {
    if (!isPreview){
      fetchFirstPageConfig.current();
    }
  }, []);

  const displayEngine = () => {
    // const { type, pageOrComponent, items } = displayConfig;
    // return (pageOrComponent === "page" ? createWebUIPage({ type, items }) : createWebUIComponent({ type, items }));
    console.log("这里是displayEngine: ", displayConfig);
    return generateWebUI(displayConfig.cmsComponentType, displayConfig);
  }

  return (
    <React.Fragment>
      {displayConfig && displayEngine()}
    </React.Fragment>
  )
}

export default DisplayEngine

