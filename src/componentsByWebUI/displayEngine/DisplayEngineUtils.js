import React from 'react';
import Grid from '@material-ui/core/Grid';
// import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';

import MainControl from '../pages/MainControl/MainControl'
import Header from '../pages/Header/Header'
import IconsUtils from './IconsUtils';
import IconButton from '../pages/IconButton/IconButton';
import LeftMenu from '../pages/LeftMenu/LeftMenu';
import Carousell from '../pages/Carousell/Carousell';
import SearchBar from '../pages/SearchBar/SearchBar'
import Card from 'componentsByWebUI/pages/Card/Card';
import SearchResult from 'componentsByWebUI/pages/SearchResult/SearchResult';
import ClientSummary from 'componentsByWebUI/pages/ClientSummary/ClientSummary';
import CafeForm from 'componentsByWebUI/pages/CafeForm/CafeForm';
import Tabs from 'componentsByWebUI/pages/Tabs/Tabs';
import Text from 'componentsByWebUI/pages/Text/Text';
import ResultSet from 'componentsByWebUI/pages/ResultSet/ResultSet'

function createWebUIPage(pageConfig) {
  // console.log("这里打印这个displayConfig是多少? ==> ", pageConfig);
  // console.log("这里打印这个type是多少? ==> ", pageConfig.type);
  // console.log("这里打印这个items是多少? ==> ", pageConfig.items);

  switch (pageConfig.type) {
    case "MAINCONTROL":
      return createMainControl(pageConfig.items);
    case "HEADER":
      return createHeader(pageConfig);
    case "LEFTMENU":
      return createLeftMenu(pageConfig.items, pageConfig.width);
    case "CARSOUSELL":
      return createCarsousell(pageConfig.items, pageConfig.data_url);
    case "SEARCHBAR":
      return createSearchBar(pageConfig);
    case "MAINCONTROL2":
      return createMainControl2(pageConfig);
    case "RESULTSET" :
      return createResultSet(pageConfig);
    default:
      return (<div />);
  }
}

function createWebUIComponent(element, i, functions, data) {
  const { type } = element;
  // console.log("这里打印这个type是多少? ==> ", type);
  switch (type) {
    case "LABEL":
      return createLabel(element, i);
    case "ICON_BUTTON":
      return createIconButton(element, i, functions, data);
    case "IMAGE_AVATARS":
      return createImageAvatars(element, i, data);
    case "TEXT":
      return createText(element, i, data);
    case "BUTTONLINK":
      return createButtonLink(element, i);
    case "BLANK":
      return createBlank(element, i, data);
    case "IMAGE":
      return createImage(element, i);
    case "GRID":
      return createGrid(element, i, functions, data);
    case "IFRAME":
      return createIframe(element, i);
    case "TEXTFIELD":
      return createTextField(element, i, functions, data);
    case "CARD":
      return createCard(element, i, functions, data);
    case "CAFEFORM":
      return createCafeForm(element, i, functions, data);
    case "SEARCHRESULT":
      return createSearchResult(element, i, functions, data);
    case "TABS":
      return createTabs(element, i, functions, data);
    case "LINE_SEPARATOR":
      return createLineSeparator(element, i, functions, data);
    default:
      return (<div key={i} />);
  }
}

const createBlank = (element, i, data) => {
  return (
    <div key={i} style={{ backgroundImage: "url(" + element.background + ")", backgroundSize: "contain", backgroundRepeat: "no-repeat" }}>
      {element.items.map((item, inx) => {
        return createWebUIComponent(item, inx, null, data);
      })}
    </div>
  );
}

const createSearchBar = ({ ...props }) => {
  return (
    <SearchBar {...props} />
  );
}

const createMainControl2 = (props) => {
  const headerJson = getConfig(props.items, "header");
  const contentJson = getConfig(props.items, "content");
  return (
    <ClientSummary headerJson={headerJson} contentJson={contentJson} {...props} />
  );
}

const createCard = (element, i, functions, data) => {
  const props = { element, i, functions, data };
  return (
    <Card key={i} {...props} />
  );
}

const createSearchResult = (element, i, functions, data) => {
  const props = { element, i, functions, data };
  return (
    <SearchResult key={i} {...props} />
  );
}

const createImage = (element, i) => {
  return (
    <img key={i} src={element.image_path} style={{ width: `${element.width}`, height: `${element.height}` }} alt={""} />
  );
}

const createLabel = (element, i) => {
  const { css_class, align, valign, width, display, variant } = element;
  const style = { textAlign: align, verticalAlign: valign, width, display };
  return (
    <Typography key={i} variant={variant || "inherit"} className={css_class} style={Object.assign(style, element.style)}>
      {element.name["en"]}
    </Typography>
  );
}

const createImageAvatars = (element, i, data) => {
  return (
    <Avatar key={i} src={data ? data.avatars : element.image_path} style={element.style} />
  );
}

const createButtonLink = (element, i) => {
  return (
    // <Avatar key={i} src={element.image_path} style={{ width: `${element.width}`, height: `${element.height}` }} />
    <ListItem button key={i} style={element.style} onClick={() => {
      console.log("==这里跳转到===>", element.action_URL);
    }}>
      {element.iconName ? <ListItemIcon>{IconsUtils[element.iconName]}</ListItemIcon> : null}
      {/* {createIconButton(element, i)} */}
      <ListItemText primary={element.text} />
    </ListItem>
  );
}

const createText = (element, i, data) => {
  console.log("==这里是createTEXT方法传进来的data===>", data);
  return (
    <Text key={i} element={element} data={data} />
  );
}

const createIconButton = (element, i, functions, data) => {
  // console.log("=============这里打印出functions===================");
  // console.log(functions);
  return (
    <IconButton element={element} key={i} functions={functions} data={data} />
  );
}

function getConfig(items, type) {
  return items.filter(item => item.id === type)[0]
}

const createMainControl = (items) => {
  const headerJson = getConfig(items, "header");
  const leftMenuJson = getConfig(items, "leftmenu");
  const contentJson = getConfig(items, "content");
  return (<MainControl headerJson={headerJson} leftMenuJson={leftMenuJson} contentJson={contentJson} />);
}

const createCarsousell = (items, data_url) => {
  console.log("=====>>>这里是创建Carsousell==", items);
  return (
    <Carousell items={items} data_url={data_url} />
  );
}

const createHeader = (pageConfig) => {
  console.log("=====>>>这里是创建header==", pageConfig.items);
  return (<Header key={pageConfig.i} {...pageConfig} />);
}

const createLeftMenu = (items, width) => {
  console.log("=====>>>这里是创建leftMenu==", items);
  const headerJson = getConfig(items, "header");
  const contentJson = getConfig(items, "content");
  const footerJson = getConfig(items, "footer");

  return (
    <LeftMenu
      width={width}
      headerJson={headerJson}
      contentJson={contentJson}
      footerJson={footerJson}
    />
  );
}

const createGrid = (element, i, functions, data) => {
  const { backgroundColor, position, width, height, margin, padding, style } = element;
  let positionElem = {};
  if (position) {
    const arr = position.split(",");
    if (position.length >= 2) {
      positionElem.position = "absolute";
      positionElem.top = arr[0];
      positionElem.right = arr[1];
    }
  }
  positionElem = Object.assign({ backgroundColor, width, height, margin, padding, flexGrow: 1 }, positionElem, style);
  return (
    <Grid key={i} container direction={element.direction} justify={element.justify} alignItems={element.alignItems} spacing={element.spacing} style={positionElem}>
      {
        element.items.map((item, inx) => {
          const { align, valign } = item;
          return (
            <Grid key={inx} item xs={item.span} style={Object.assign({ textAlign: align, alignItems: align, verticalAlign: valign }, item.style)}>
              {
                item.elements.map((element, index) => {
                  if (element.pageOrComponent === "page") {
                    element.i = i;
                    element.data = data;
                    return createWebUIPage(element);
                  } else {
                    return createWebUIComponent(element, index, functions, data);
                  }
                })
              }
            </Grid>
          );
        })
      }
    </Grid>
  );
}

const createIframe = (element, i) => {
  return (
    <iframe key={i} title={element.title} style={element.style} src={element.src} />
  );
}

const createTextField = (element, i, functions) => {
  const onChange = event => {
    if (functions[element.action_URL]) {
      functions[element.action_URL](event.target.value);
    }
  }
  return <TextField key={i} fullWidth={element.fullWidth} onChange={onChange} />
}

const createCafeForm = (element, i, functions, data) => {
  console.log("======这里看一看能否获得这个正确的formid, responseid", element);
  return <CafeForm key={i} {...element} functions={functions} data={data} />
}

const createTabs = (element, i, functions, data) => {
  const props = { element, functions, data };
  return <Tabs key={i} {...props} />;
}

const createLineSeparator = (element, i) => {
  const { css_class, style, orientation } = element;
  const props = { className: css_class, style, orientation };
  return <Divider key={i} {...props} />;
}

const createResultSet = (pageConfig) => {
  return <ResultSet key={pageConfig.i} {...pageConfig} />;
}

export { createWebUIPage, createWebUIComponent };