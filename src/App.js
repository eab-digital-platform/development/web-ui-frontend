import React, { useState, useEffect } from "react";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from "@material-ui/styles";
import 'theme/default.css';
// 加载全局样式, 这里加载的是cafe里面的
import customTheme from './theme/theme';
// 加载整个路由
import Routes from './routes';
// 加载context
import { WebUIProvider } from './WebUIContext';

const hist = createBrowserHistory();

const useStyles = makeStyles({
  root: {
    display: 'flex',
  }
});

export default function App() {

  const [displayJson, setDisplayJson] = useState(null);
  useEffect(() => {
    const receiveMessageFromIndex = (event) => {
      if (event !== undefined) {
        // console.log( '我是react,我接受到了来自iframe的模型ID：', event.data );
        setDisplayJson(event.data);
      }
    }

    //监听message事件
    window.addEventListener("message", receiveMessageFromIndex, false);

  }, [])

  const classes = useStyles();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <WebUIProvider>
        <ThemeProvider theme={customTheme()}>
          <Router history={hist}>
            <Routes displayJson={displayJson} />
          </Router>
        </ThemeProvider>
      </WebUIProvider>
    </div>
  );
}
