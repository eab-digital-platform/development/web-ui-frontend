/* eslint-disable */

import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import external from "rollup-plugin-peer-deps-external";
import postcss from "rollup-plugin-postcss";
import resolve from "rollup-plugin-node-resolve";
import url from "rollup-plugin-url";
import replace from "rollup-plugin-replace";
import { terser } from "rollup-plugin-terser";
import obfuscator from "rollup-plugin-javascript-obfuscator";
import { eslint } from "rollup-plugin-eslint";
// import autoExternal from 'rollup-plugin-auto-external';
import svgr from "@svgr/rollup";

import pkg from "./package.json";

export default {
  input: "src/App.js",
  output: [
    {
      file: pkg.main,
      format: "cjs",
      sourcemap: true
    },
    {
      file: pkg.module,
      format: "es",
      sourcemap: true
    }
  ],
  plugins: [
    // autoExternal(),
    external(),
    postcss({
      // 如果设置成true, 那么你编写的css属性名字, 在编译的时候都会增加一个module name
      modules: false
    }),
    url(),
    svgr(),
    babel({
      exclude: "node_modules/**",
      runtimeHelpers: true
      // plugins: [ '@babel/external-helpers' ]
    }),
    replace({
      exclude: "node_modules/**",
      ENV: "production"
    }),
    resolve({
      customResolveOptions: {
        moduleDirectory: "src/"
      }
    }),
    commonjs({
      include: "node_modules/**",
      namedExports: {
        "react-is": ["ForwardRef"]
      }
    }),
    terser()
    // obfuscator(),
  ]
};
